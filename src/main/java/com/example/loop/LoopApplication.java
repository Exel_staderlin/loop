package com.example.loop;

import com.example.loop.model.ArtistData;
import com.example.loop.model.DataSet;
import com.example.loop.model.SongData;
import com.example.loop.model.UserData;
import com.example.loop.repository.ArtistRepository;
import com.example.loop.repository.GenreRepository;
import com.example.loop.repository.SongRepository;
import com.example.loop.repository.UserRepository;
import com.univocity.parsers.common.processor.RowListProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@SpringBootApplication
@EnableJpaAuditing
public class LoopApplication {

    String pic = "https://png.pngtree.com/svg/20170602/b7c3ca6e9e.png";

    public static void main(String[] args) {
        SpringApplication.run(LoopApplication.class, args);
    }

    @Bean
    CommandLineRunner init(ArtistRepository artistRepository, SongRepository songRepository, UserRepository userRepository, GenreRepository genreRepository) {
        return args -> {
//            final int[] time = {0};
//            Timer timer = new Timer();
//            TimerTask task = new TimerTask() {
//                private int i = 0;
//                public void run() {
//                    time[0] = i++; /*difference time*/
//                }
//            };
//            timer.scheduleAtFixedRate(task, 0, 1000); //1000ms = 1sec
//
//            ArrayList<List<String>> datasetV1 = readCsvToList("R:/all_filtered_log.csv");
////            ArrayList<List<String>> userRecord = readCsvToList("R:/user.csv");
////            ArrayList<List<String>> artistRecord = readCsvToList("R:/artist.csv");
////            ArrayList<List<String>> songRecord = readCsvToList("R:/song.csv");
////            List<DataSet> data = recordMixArtistSong(artistRecord,songRecord);
////            List<DataSet> data = recordMixUserLog(datasetV1, songRecord);
////            List<DataSet> dataset = convertRecordToDataset(datasetV1);
////            Set<DataSet> uniqueName = new HashSet<>(artist);
////            System.out.println("uniqueName size :" + uniqueName.size());
////            List<SongData> songData = songRepository.findAll();
//            List<DataSet> dataset = new ArrayList<>();
//            for(int i=0; i<datasetV1.size(); i++) {
//                DataSet data = new DataSet();
//                data.user = datasetV1.get(i).get(0);
////                data.song = datasetV1.get(i).get(2);
////                data.createDate = datasetV1.get(i).get(3);
//                dataset.add(data);
//            }
//            Set<DataSet> set = new HashSet<>(dataset);
//            List<DataSet> uniqueName = new ArrayList<>(set);
//            writeTextArtist(uniqueName);
//            System.out.println("time for loop :" + time[0]);
//            writeArtistToDB(uniqueName, artistRepository);
//            writeSongToDB(uniqueName, artistRepository, songRepository);
        };
    }

    private static List<String> convertRecordArtistString(ArrayList<List<String>> records) {
        List<String> listData = new ArrayList<>();
        for (int i = 0; i < records.size(); i++) {
            String artist = records.get(i).get(3); // Artist
            listData.add(artist);
        }
        return listData;
    }

    private static List<DataSet> convertRecordToDataset(ArrayList<List<String>> records) {
        //        List<String> listData = new ArrayList<>();
        List<DataSet> listTestData = new ArrayList<>();
        for (int i = 0; i < records.size(); i++) {
//                String user = records.get(i).get(0); // User
//                String time = records.get(i).get(1); // Time
            String artist = records.get(i).get(0); // Artist
            String song = records.get(i).get(1); // Song
            DataSet testData = new DataSet();
            testData.artist = artist;
            testData.song = song;
            listTestData.add(testData);
        }
        return listTestData;
    }

    private static List<DataSet> recordMixArtistSong(ArrayList<List<String>> artistRecord, ArrayList<List<String>> songRecord) {
        List<DataSet> listTestData = new ArrayList<>();
        System.out.println("loading...");

        for (int i = 0; i < songRecord.size(); i++) {
            String artist = "null"; // Artist
            String song = songRecord.get(i).get(1);
            for (int j = 0; j < artistRecord.size(); j++) {
                if (songRecord.get(i).get(0).equals(artistRecord.get(j).get(2))) { // jika title sama maka
                    artist = artistRecord.get(j).get(0); // ambil idnya
                    break;
                }
            }
            System.out.println("artist  :" + artist + " - " + "Song :" + song);
            DataSet testData = new DataSet();
            testData.artist = artist;
            testData.song = song;
            listTestData.add(testData);
        }
        System.out.println("finish");

        return listTestData;
    }

    private static List<DataSet> recordMixUserLog(ArrayList<List<String>> logRecord, ArrayList<List<String>> songRecord) {
        List<DataSet> listTestData = new ArrayList<>();
        System.out.println("loading...");

        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("log_training.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter printWriter = new PrintWriter(fileWriter);

        for (int i = 0; i < logRecord.size(); i++) {
            String idSong = "null";
//            String id = logRecord.get(i).get(0);
            String user = logRecord.get(i).get(0);
            String song = logRecord.get(i).get(1);
            String timespamp = logRecord.get(i).get(2);

            for (int j = 0; j < songRecord.size(); j++) {
//                System.out.println(songRecord.get(j).get(0)+ ","+songRecord.get(j).get(1));

                if (song.equals(songRecord.get(j).get(1))) { // jika title sama maka
                    idSong = songRecord.get(j).get(0); // ambil idnya
                    break;
                }
            }
            DataSet testData = new DataSet();
//            testData.id = i+1;
            testData.user = user;
            testData.song = idSong;
            testData.createDate =timespamp;
            System.out.println(i+1+ ","+testData.user+ "," + testData.song+ "," + testData.createDate);
            printWriter.println(i+1+ ","+testData.user+ "," + testData.song+ "," + testData.createDate);

            if(i == 200000) {
                break;
            }
//            listTestData.add(testData);

        }
        System.out.println("finish");
        printWriter.close();
        return listTestData;
    }


    private static ArrayList<List<String>> readCsvToList(String csvFileLocation) {
        ArrayList<List<String>> records = new ArrayList<>();
        CsvParser parser = new CsvParser(settingsCsvParser());
        parser.beginParsing(new File(csvFileLocation));
        try {
            String[] row;
            int loop = 1;
            int threshold = 5000000;
            while ((row = parser.parseNext()) != null) {
                records.add(Arrays.asList(row));
                if (loop >= threshold) {
                    System.out.println("load record :" + loop);
                }
                loop++;
            }
            System.out.println("record size :" + records.size());
            return records;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    private static CsvParserSettings settingsCsvParser() {
        CsvParserSettings settings = new CsvParserSettings(); // you'll find many options here, check the tutorial.
        RowListProcessor rowProcessor = new RowListProcessor();
        settings.setNullValue("<NULL>");
        settings.setEmptyValue("<EMPTY>");
        settings.setIgnoreLeadingWhitespaces(false);
        settings.setIgnoreTrailingWhitespaces(false);
        settings.setNumberOfRecordsToRead(10000000);
        settings.setSkipEmptyLines(false);
        settings.setMaxCharsPerColumn(10000000);
        settings.setInputBufferSize(1000);
        settings.setReadInputOnSeparateThread(false);
        settings.setLineSeparatorDetectionEnabled(true);
        settings.setRowProcessor(rowProcessor);
        settings.setHeaderExtractionEnabled(true);
        return settings;
    }

    private void writeSongToDB(Set<DataSet> uniqueTestData, ArtistRepository artistRepository, SongRepository songRepository) {
        int loop = 0;
        List<DataSet> sortedUniqueName = new ArrayList<>(uniqueTestData);
        Collections.sort(sortedUniqueName, Collections.reverseOrder());
        for (DataSet testData : sortedUniqueName) {
            if (testData.artist.length() > 3 && testData.song.length() > 3) {
                ArtistData artistData = artistRepository.findByName(testData.artist);
                if (artistData != null) {
                    SongData songData = getSongData(artistData.id, testData.song);
                    songRepository.save(songData);
                }
            }
            loop++;
        }
    }

    private void writeArtistToDB(Set<DataSet> uniqueName, ArtistRepository artistRepository) {
        int loop = 0;
        for (DataSet data : uniqueName) {
            if (data.artist.length() > 3) {
                ArtistData artistData = getArtistData(data.artist);
                artistRepository.save(artistData);
            }
//            if(loop == 1000) {
//                break;
//            }
            loop++;
        }
    }

    private UserData getUserData(String name) {
        UserData userData = new UserData();
        userData.name = name;
        userData.email = name + "@gmail.com";
        userData.password = "$2a$10$Z/3MaeXBzpCBruDuY5R9h.vdOvD7nwkEh1O0pWolL/LyFaomgZ00O";
        userData.image = "https://www.logolynx.com/images/logolynx/b4/b4ef8b89b08d503b37f526bca624c19a.jpeg";
        userData.role = 0;
        return userData;
    }

    private ArtistData getArtistData(String name) {
        ArtistData artistData = new ArtistData();
        artistData.name = name;
        artistData.image = "https://www.logolynx.com/images/logolynx/b4/b4ef8b89b08d503b37f526bca624c19a.jpeg";
        return artistData;
    }

    private SongData getSongData(int idArtist, String name) {
        SongData songData = new SongData();
        songData.id_artist = idArtist;
        songData.title = name;
        songData.linkUrl = "http://dl3.wapkizfile.info/ddl/60386b599691fd2b67db3a2e2f087101/matikiri+wapkiz+com/Shawn%20Mendes%20Camila%20Cabello%20-%20Senorita%20(%20cover%20by%20J.Fla%20)-(matikiri.wapkiz.com).mp3";
        songData.playcount = 0;
        return songData;
    }

    private void writeTextUser(List<DataSet> uniqueName) {
//        List<DataSet> sortedUniqueName = new ArrayList<>(uniqueName);
        Collections.sort(uniqueName);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("user.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter printWriter = new PrintWriter(fileWriter);
        int id = 5;
        for (DataSet testData : uniqueName) {
            String email = testData.user+"@gmail.com";
            String image = "https://www.logolynx.com/images/logolynx/b4/b4ef8b89b08d503b37f526bca624c19a.jpeg";
            String role = "0";
            String lastLoginDate = "2019-05-04T12:43:14Z";
            String name = testData.user;
            String password = "$2a$10$T43uOOwHjk7ovcGUo0yzk.1gEXIpPGQeJSYwgiSjUOOGXVzmNlsX6";
            String registeredDate = "2019-05-04T12:43:14Z";
            String status = "0";
            printWriter.println(id+ "," + email+ "," + image+ "," + role+ "," + lastLoginDate+ "," + name+ "," + password + "," + registeredDate + "," + status );
            id++;
        }

        printWriter.close();
    }


    private void writeTextString(Set<String> uniqueName) {

        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("song_artist.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter printWriter = new PrintWriter(fileWriter);
        int id = 7;
        for (String artist : uniqueName) {
            if (artist.length() > 3) {
                printWriter.println(id + "," + "https://www.logolynx.com/images/logolynx/b4/b4ef8b89b08d503b37f526bca624c19a.jpeg" + " ," + artist);
                id++;
            }
        }
        printWriter.close();
    }

}


