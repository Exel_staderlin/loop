package com.example.loop.pengujian;

import com.example.loop.model.DataSet;
import com.example.loop.model.FscoreModel;
import com.univocity.parsers.common.processor.RowListProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class HitungFScore {
    static int[] jmlFold = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    static String rekomendasiFoldPath = "";
    static String fscoreFileName = "";
    static int limit = 45;
    static List<Double> rata2PrecisionSemuaFold = new ArrayList<>();
    static List<Double> rata2RecallSemuaFold = new ArrayList<>();
    static List<Double> rata2F1ScoreSemuaFold = new ArrayList<>();

    public static void main(String[] args) {
        for (Integer fold : jmlFold) {
            rekomendasiFoldPath = "C:/dataset/rekomendasi_fold-genre-limit/rekomendasi_fold" + fold + "-genre-limit" + limit + ".csv";
            fscoreFileName = "C://dataset//f1score-limit-" + limit + "//fscore_fold" + fold + "-limit" + limit + ".csv";
            ArrayList<List<String>> rekomendasi = readCsvToList(rekomendasiFoldPath);
            ArrayList<List<String>> userLiked = readCsvToList("C:/dataset/user_liked_genre(2).csv");
            List<FscoreModel> data = recordMixRecomendasi(rekomendasi, userLiked);
            writeFscore(data);
            // testJmlGenreBenarDiDataset(rekomendasi,userLiked);
        }
        writeSemuaFoldRata2(rata2PrecisionSemuaFold, rata2RecallSemuaFold, rata2F1ScoreSemuaFold);
    }

    private static List<FscoreModel> recordMixRecomendasi(ArrayList<List<String>> rekomendasi, ArrayList<List<String>> userLiked) {
        List<FscoreModel> listFScore = new ArrayList<>();
        System.out.println("loading...");
        String currentUser = rekomendasi.get(0).get(0);
        double genreYgBenar = 0;
        double jmlLaguUser = 0;
        int jmlGenreBenarDiDataset = 0;
        for (int i = 0; i < rekomendasi.size(); i++) {
            String mUser = rekomendasi.get(i).get(0);
            String mGenre = rekomendasi.get(i).get(2);
            if (!currentUser.equals(mUser)) {
                for (List<String> data : userLiked) {
                    if (currentUser.equals(data.get(0))) {
                        jmlGenreBenarDiDataset += Integer.valueOf(data.get(3));
                    }
                }
//                System.out.println("user " + currentUser + ": Jml Genre = " + jmlLaguUser + ",Genre yg benar = " + genreYgBenar
//                        + ", jmlGenreBenarDiDataset = " + jmlGenreBenarDiDataset);
                FscoreModel hitungFscore = hitungFScore(currentUser, genreYgBenar, jmlLaguUser, jmlGenreBenarDiDataset);
                listFScore.add(hitungFscore);
                genreYgBenar = 0;
                jmlLaguUser = 0;
                jmlGenreBenarDiDataset = 0;
                currentUser = mUser;
            } else if (i + 1 == rekomendasi.size()) {
                jmlLaguUser++;
                System.out.println("user " + currentUser + ": Jml Genre = " + jmlLaguUser + ",Genre yg benar = " + genreYgBenar
                        + ", jmlGenreBenarDiDataset = " + jmlGenreBenarDiDataset);
            }
            jmlLaguUser++;
            for (int j = 0; j < userLiked.size(); j++) {
                String user = userLiked.get(j).get(0);
                String genre = userLiked.get(j).get(1);
                if (mUser.equals(user)) {
                    if (mGenre.equals(genre)) {
                        genreYgBenar++;     // mencari genre yang benar di user_liked_genre
                    }
                }

            }
        }
        System.out.println("finish");
        return listFScore;
    }

    private static void testJmlGenreBenarDiDataset(ArrayList<List<String>> rekomendasi, ArrayList<List<String>> userLiked) {
        List<FscoreModel> listFScore = new ArrayList<>();
        System.out.println("loading...");
        String currentUser = rekomendasi.get(0).get(0);
        double genreYgBenar = 0;
        double jmlLaguUser = 0;
        int jmlGenreBenarDiDataset = 0;
        for (int i = 0; i < rekomendasi.size(); i++) {
            String mUser = rekomendasi.get(i).get(0);
            String mGenre = rekomendasi.get(i).get(2);
            if (!currentUser.equals(mUser)) {
                for (List<String> data : userLiked) {
                    if (currentUser.equals(data.get(0))) {
                        jmlGenreBenarDiDataset += Integer.valueOf(data.get(3));
                    }
                }
                System.out.println("user " + currentUser + ": Jml Genre = " + jmlLaguUser + ",Genre yg benar = " + genreYgBenar
                        + ", jmlGenreBenarDiDataset = " + jmlGenreBenarDiDataset);
                FscoreModel hitungFscore = hitungFScore(currentUser, genreYgBenar, jmlLaguUser, jmlGenreBenarDiDataset);
                listFScore.add(hitungFscore);
                genreYgBenar = 0;
                jmlLaguUser = 0;
                jmlGenreBenarDiDataset = 0;
                currentUser = mUser;
            } else if (i + 1 == rekomendasi.size()) {
                jmlLaguUser++;
                System.out.println("user " + currentUser + ": Jml Genre = " + jmlLaguUser + ",Genre yg benar = " + genreYgBenar
                        + ", jmlGenreBenarDiDataset = " + jmlGenreBenarDiDataset);
            }
            jmlLaguUser++;
            for (int j = 0; j < userLiked.size(); j++) {
                String user = userLiked.get(j).get(0);
                String genre = userLiked.get(j).get(1);
                if (mUser.equals(user)) {
                    if (mGenre.equals(genre)) {
                        genreYgBenar++;     // mencari genre yang benar di user_liked_genre
                    }
                }

            }
        }
        System.out.println("finish");
    }

    private static FscoreModel hitungFScore(String user, double genreYgBenar, double jmlLaguUser, double jmlGenreBenarDiDataset) {
        double precision = genreYgBenar / jmlLaguUser;
        double recall = genreYgBenar / jmlGenreBenarDiDataset;
        double f1Score; // rumus f1score = 2 * ( (precision * recall) / (precision + recall) )
        if (recall != 0 && precision != 0) {
            double a = ld(precision) * ld(recall);
            double b = ld(precision) + ld(recall);
            f1Score = 2 * (a / b);
        } else
            f1Score = 0;

        FscoreModel fscoreModel = new FscoreModel();
        fscoreModel.user = user;
        fscoreModel.precision = ld(precision);
        fscoreModel.recall = ld(recall);
        fscoreModel.f1Score = ld(f1Score);
        System.out.println("user :" + user + " precision = " + ld(precision) + " recall = " + ld(recall) + " F1Score = " + ld(f1Score));
        return fscoreModel;
    }

    private static void writeFscore(List<FscoreModel> fscoreModels) {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(fscoreFileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter printWriter = new PrintWriter(fileWriter);
        printWriter.println("user,precision,recall,f1score");
        double totalPrecision = 0;
        double totalRecall = 0;
        double totalF1Score = 0;
        for (FscoreModel fscoreModel : fscoreModels) {
            totalPrecision += fscoreModel.precision;
            totalRecall += fscoreModel.recall;
            totalF1Score += fscoreModel.f1Score;
            printWriter.println(fscoreModel.user + "," + fscoreModel.precision + "," + fscoreModel.recall + "," + fscoreModel.f1Score);
        }
        double rata2Precision = totalPrecision / fscoreModels.size();
        double rata2Recall = totalRecall / fscoreModels.size();
        double rata2F1Score = totalF1Score / fscoreModels.size();
        rata2PrecisionSemuaFold.add(rata2Precision);
        rata2RecallSemuaFold.add(rata2Recall);
        rata2F1ScoreSemuaFold.add(rata2F1Score);
        System.out.println("rata2 Precision = " + ld(rata2Precision));
        System.out.println("rata2 Recall= " + ld(rata2Recall));
        System.out.println("rata2 f1score= " + ld(rata2F1Score));
        printWriter.println("rata2 Precision = " + ld(rata2Precision));
        printWriter.println("rata2 Recall = " + ld(rata2Recall));
        printWriter.println("rata2 F1 Score = " + ld(rata2F1Score));
        printWriter.close();
    }

    private static void writeSemuaFoldRata2(List<Double> rata2PrecisionSemuaFold, List<Double> rata2RecallSemuaFold, List<Double> rata2F1ScoreSemuaFold) {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("C://dataset//f1score-limit-" + limit + "//Rata2-semua-fold-limit" + limit + ".csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter printWriter = new PrintWriter(fileWriter);
        printWriter.println("rata2Precision,rata2Recall,rata2F1Score");
        double totalPrecision = 0;
        double totalRecall = 0;
        double totalF1Score = 0;
        for (Double precision : rata2PrecisionSemuaFold) {
            totalPrecision += precision;
        }
        for (Double recall : rata2RecallSemuaFold) {
            totalRecall += recall;
        }
        for (Double f1Score : rata2F1ScoreSemuaFold) {
            totalF1Score += f1Score;
        }
        double rata2Precision = totalPrecision / rata2PrecisionSemuaFold.size();
        double rata2Recall = totalRecall / rata2RecallSemuaFold.size();
        double rata2F1Score = totalF1Score / rata2F1ScoreSemuaFold.size();
        System.out.println("rata2 Precision= " + ld(rata2Precision));
        System.out.println("rata2 Recall = " + ld(rata2Recall));
        System.out.println("rata2 f1score= " + ld(rata2F1Score));
        printWriter.println(ld(rata2Precision) + "," + ld(rata2Recall) + "," + ld(rata2F1Score));
        printWriter.close();
    }


    private static ArrayList<List<String>> readCsvToList(String csvFileLocation) {
        ArrayList<List<String>> records = new ArrayList<>();
        CsvParser parser = new CsvParser(settingsCsvParser());
        parser.beginParsing(new File(csvFileLocation));
        try {
            String[] row;
            int loop = 1;
            int threshold = 5000000;
            while ((row = parser.parseNext()) != null) {
                records.add(Arrays.asList(row));
                if (loop >= threshold) {
                    System.out.println("load record :" + loop);
                }
                loop++;
            }
            System.out.println("record size :" + records.size());
            return records;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    private static CsvParserSettings settingsCsvParser() {
        CsvParserSettings settings = new CsvParserSettings(); // you'll find many options here, check the tutorial.
        RowListProcessor rowProcessor = new RowListProcessor();
        settings.setNullValue("<NULL>");
        settings.setEmptyValue("<EMPTY>");
        settings.setIgnoreLeadingWhitespaces(false);
        settings.setIgnoreTrailingWhitespaces(false);
        settings.setNumberOfRecordsToRead(10000000);
        settings.setSkipEmptyLines(false);
        settings.setMaxCharsPerColumn(10000000);
        settings.setInputBufferSize(1000);
        settings.setReadInputOnSeparateThread(false);
        settings.setLineSeparatorDetectionEnabled(true);
        settings.setRowProcessor(rowProcessor);
        settings.setHeaderExtractionEnabled(true);
        return settings;
    }

    private static double ld(double value) { //LD itu limit digit , persingkat aja
        return Math.round(value * Math.pow(10, 4)) / Math.pow(10, 4);
    }

}
