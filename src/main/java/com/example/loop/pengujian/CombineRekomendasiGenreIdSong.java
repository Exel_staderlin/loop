package com.example.loop.pengujian;

import com.example.loop.model.SongData;
import com.example.loop.repository.SongRepository;
import com.univocity.parsers.common.processor.RowListProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CombineRekomendasiGenreIdSong {

    @Autowired
    private static SongRepository songRepository;

    static int[] jmlFold = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    static String rekomendasiPath = "";
    static String rekomendasiGenrePath = "";
    static String outputFileName = "";

    public static void main(String[] args) {
        runGetSongData();
    }

    private static void runGetSongData() {
        for (Integer fold : jmlFold) {
            rekomendasiPath = "C:/dataset/rekomendasi_fold" + fold + ".csv";
            rekomendasiGenrePath = "C:/dataset/rekomendasi_fold-genre-limit/rekomendasi_fold" + fold + "-genre.csv";
            outputFileName = "C://dataset//rekomendasi_fold" + fold + "-genre-song.csv";
            ArrayList<List<String>> rekomendasi = readCsvToList(rekomendasiPath);
            ArrayList<List<String>> rekomendasiGenre = readCsvToList(rekomendasiGenrePath);
            writeRekomendasi(rekomendasi, rekomendasiGenre);
        }
    }

    private static void writeRekomendasi(ArrayList<List<String>> rekomendasi, ArrayList<List<String>> rekomendasiGenre) {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(outputFileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter printWriter = new PrintWriter(fileWriter);
        printWriter.println("id_user,rating,id_song,playcount,id_genre");
        for (int i = 0; i < rekomendasi.size(); i++) {
            System.out.println("run :" + i + " to " + rekomendasi.size());
            printWriter.println(rekomendasi.get(i).get(0) + "," +
                    rekomendasi.get(i).get(1) + "," +
                    rekomendasi.get(i).get(2) + "," +
                    rekomendasi.get(i).get(3) + "," +
                    rekomendasiGenre.get(i).get(2));
        }
        printWriter.close();
    }

    private static ArrayList<List<String>> readCsvToList(String csvFileLocation) {
        ArrayList<List<String>> records = new ArrayList<>();
        CsvParser parser = new CsvParser(settingsCsvParser());
        parser.beginParsing(new File(csvFileLocation));
        try {
            String[] row;
            int loop = 1;
            int threshold = 5000000;
            while ((row = parser.parseNext()) != null) {
                records.add(Arrays.asList(row));
                if (loop >= threshold) {
                    System.out.println("load record :" + loop);
                }
                loop++;
            }
            System.out.println("record size :" + records.size());
            return records;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    private static CsvParserSettings settingsCsvParser() {
        CsvParserSettings settings = new CsvParserSettings(); // you'll find many options here, check the tutorial.
        RowListProcessor rowProcessor = new RowListProcessor();
        settings.setNullValue("<NULL>");
        settings.setEmptyValue("<EMPTY>");
        settings.setIgnoreLeadingWhitespaces(false);
        settings.setIgnoreTrailingWhitespaces(false);
        settings.setNumberOfRecordsToRead(10000000);
        settings.setSkipEmptyLines(false);
        settings.setMaxCharsPerColumn(10000000);
        settings.setInputBufferSize(1000);
        settings.setReadInputOnSeparateThread(false);
        settings.setLineSeparatorDetectionEnabled(true);
        settings.setRowProcessor(rowProcessor);
        settings.setHeaderExtractionEnabled(true);
        return settings;
    }

}
