package com.example.loop.pengujian;

import com.example.loop.model.DataSet;
import com.example.loop.model.RekomendasiFoldData;
import com.univocity.parsers.common.processor.RowListProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.stream.IntStream;

public class CropListRekomendasiGenreFoldCsv {

    static int[] jmlFold = {1,2,3,4,5,6,7,8,9,10};
    static String rekomendasiGenrePath = "";
    static String rekomendasiGenreNameLimit = "";
    static int limit = 46;

    public static void main(String[] args) {
        for(Integer fold : jmlFold) {
            rekomendasiGenrePath = "C:/dataset/rekomendasi_fold-genre-limit/rekomendasi_fold"+fold+"-genre.csv";
            rekomendasiGenreNameLimit = "C://dataset//rekomendasi_fold-genre-limit//rekomendasi_fold"+fold+"-genre-limit"+limit+".csv";
            ArrayList<List<String>> userLikedGenre = readCsvToList("C:/dataset/user_liked_genre.csv");
            ArrayList<List<String>> datasetV1 = readCsvToList(rekomendasiGenrePath);
            ArrayList<List<DataSet>> data = recordRecommendationGenres(datasetV1);
            HashMap<String, int[]> likedUser = recordHashMapUserLikedGenre(userLikedGenre);
            writeRekomendasi(data, likedUser);
        }


    }

    private static ArrayList<List<DataSet>> recordRecommendationGenres(ArrayList<List<String>> logRecord) {
        ArrayList<List<DataSet>> listDataPerUser = new ArrayList<>();
        List<DataSet> listTestData = new ArrayList<>();
        System.out.println("loading...");
        String currentUser = logRecord.get(0).get(0);
        for (int i = 0; i < logRecord.size(); i++) {
            String user = logRecord.get(i).get(0);
            String rating = logRecord.get(i).get(1);
            String idGenre = logRecord.get(i).get(2);
            String playcount = logRecord.get(i).get(3);
            System.out.println(i + " to " + logRecord.size() + " = " + idGenre);

            if (!currentUser.equals(user) || i + 1 == logRecord.size()) {
                listDataPerUser.add(listTestData);
                listTestData = new ArrayList<>();
                currentUser = user;
            }

            DataSet testData = new DataSet();
            testData.user = user;
            testData.rating = rating;
            testData.idGenre = idGenre;
            testData.playcount = playcount;
            listTestData.add(testData);
        }
        System.out.println("finish");
        return listDataPerUser;
    }

    private static HashMap<String, int[]> recordHashMapUserLikedGenre(ArrayList<List<String>> logRecord) { // hashmap user suka genre apa aja contoh "6" : {19,12}
        HashMap<String, int[]> hashMap = new HashMap<>();
        ArrayList<Integer> listLikedGenre = new ArrayList<>();
        String currentUser = logRecord.get(0).get(0);
        for (int i = 0; i < logRecord.size(); i++) {
            String user = logRecord.get(i).get(0);
            String likedGenre = logRecord.get(i).get(1);
            if (!currentUser.equals(user) || i + 1 == logRecord.size()) {
                int[] likedGenreArray = listLikedGenre.stream().mapToInt(a -> a).toArray();
                hashMap.put(currentUser, likedGenreArray);
                listLikedGenre = new ArrayList<>();
                currentUser = user;
            }
            listLikedGenre.add(Integer.valueOf(likedGenre));
        }
        return hashMap;
    }

    private static void writeRekomendasi(ArrayList<List<DataSet>> listDataPerUser, HashMap<String, int[]> likedGenre) {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(rekomendasiGenreNameLimit);
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter printWriter = new PrintWriter(fileWriter);
        printWriter.println("user, rating, genre, playcount");

        for (List<DataSet> listData : listDataPerUser) {

            List<RekomendasiFoldData> listRekomendasiFoldData = new ArrayList<>();
            for (int i = 0; i < listData.size(); i++) {
                if (!listData.get(i).idGenre.equals("null")) {
                    RekomendasiFoldData rekomendasiFoldData = new RekomendasiFoldData();
                    rekomendasiFoldData.user = listData.get(i).user;
                    rekomendasiFoldData.rating = Double.valueOf(listData.get(i).rating);
                    rekomendasiFoldData.idGenre = Integer.parseInt(listData.get(i).idGenre);
                    rekomendasiFoldData.playcount = listData.get(i).playcount;
                    boolean contains = IntStream.of(likedGenre.get(rekomendasiFoldData.user)).anyMatch(x -> x == rekomendasiFoldData.idGenre); // jika id user terkandung di dalam data
                    if (contains) { // mengandung genre yg di sukai
                        rekomendasiFoldData.priority = 1; // genre prioritas
                    } else {
                        rekomendasiFoldData.priority = 0; // genre non prioritas
                    }
                    listRekomendasiFoldData.add(rekomendasiFoldData);
                }
            }
            Collections.sort(listRekomendasiFoldData, Collections.reverseOrder()); // sort berdasarkan rating dan prioritaskan genre yg disukai
            int printed = 0;
            for(RekomendasiFoldData data : listRekomendasiFoldData) {
                printWriter.println(data.user + "," + data.rating + "," + data.idGenre + "," + data.playcount);
                printed++;
                if (printed == limit) break; // jika sudah 30 yg di print berhenti
            }
        }
        printWriter.close();
    }

    private static ArrayList<List<String>> readCsvToList(String csvFileLocation) {
        ArrayList<List<String>> records = new ArrayList<>();
        CsvParser parser = new CsvParser(settingsCsvParser());
        parser.beginParsing(new File(csvFileLocation));
        try {
            String[] row;
            int loop = 1;
            int threshold = 5000000;
            while ((row = parser.parseNext()) != null) {
                records.add(Arrays.asList(row));
                if (loop >= threshold) {
                    System.out.println("load record :" + loop);
                }
                loop++;
            }
            System.out.println("record size :" + records.size());
            return records;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    private static CsvParserSettings settingsCsvParser() {
        CsvParserSettings settings = new CsvParserSettings(); // you'll find many options here, check the tutorial.
        RowListProcessor rowProcessor = new RowListProcessor();
        settings.setNullValue("<NULL>");
        settings.setEmptyValue("<EMPTY>");
        settings.setIgnoreLeadingWhitespaces(false);
        settings.setIgnoreTrailingWhitespaces(false);
        settings.setNumberOfRecordsToRead(10000000);
        settings.setSkipEmptyLines(false);
        settings.setMaxCharsPerColumn(10000000);
        settings.setInputBufferSize(1000);
        settings.setReadInputOnSeparateThread(false);
        settings.setLineSeparatorDetectionEnabled(true);
        settings.setRowProcessor(rowProcessor);
        settings.setHeaderExtractionEnabled(true);
        return settings;
    }

    private static Timer runTimer() {
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            private int i = 0;

            public void run() {
                System.out.println("time running :" + i++); /*difference time*/
            }
        };
        timer.scheduleAtFixedRate(task, 0, 1000); //1000ms = 1sec
        return timer;
    }
}
