package com.example.loop.pengujian;

import com.example.loop.model.DataSet;
import com.example.loop.model.SongData;
import com.example.loop.repository.SongRepository;
import com.univocity.parsers.common.processor.RowListProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class writeRekomendasiFoldCsv {


    private static String rekomendasiFoldPath = "C:/dataset/rekomendasi_fold1.csv";
    private static String rekomendasiGenreName = "rekomendasi_fold1-genre.csv";


    public static void main(String[] args) {
        Timer timer = runTimer();
        ArrayList<List<String>> datasetV1 = readCsvToList(rekomendasiFoldPath);
        ArrayList<List<String>> songRecord = readCsvToList("C:/dataset/song_id_idgenre_idartist_doang.csv");
        List<DataSet> data = recordMixSongLog(datasetV1, songRecord);
        writeRekomendasi(data);
        timer.cancel();
    }

    private static List<DataSet> recordMixSongLog(ArrayList<List<String>> logRecord, ArrayList<List<String>> songRecord) {
        List<DataSet> listTestData = new ArrayList<>();
        System.out.println("loading...");
        for (int i = 0; i < logRecord.size(); i++) {
            String user = logRecord.get(i).get(0);
            String rating = logRecord.get(i).get(1);
            String idSong = logRecord.get(i).get(2);
            String playcount = logRecord.get(i).get(3);
            String idGenre = "null";
//            String song = logRecord.get(i).get(1);
            String timespamp = logRecord.get(i).get(3);
            for (int j = 0; j < songRecord.size(); j++) {
                if (idSong.equals(songRecord.get(j).get(0))) { // jika song sama maka
                    idGenre = songRecord.get(j).get(2); // ambil genre
                    break;
                }
            }
            System.out.println(i + " to " + logRecord.size() + " = " + idGenre);
            DataSet testData = new DataSet();
            testData.user = user;
            testData.idSong = idSong;
            testData.rating = rating;
            testData.idGenre = idGenre;
            testData.playcount = playcount;
            listTestData.add(testData);
        }
        System.out.println("finish");
        return listTestData;
    }

    private static void writeRekomendasi(List<DataSet> uniqueName) {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(rekomendasiGenreName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter printWriter = new PrintWriter(fileWriter);
        printWriter.println("user, rating, genre, playcount");
        for (DataSet testData : uniqueName) {
            String user = testData.user;
            String rating = testData.rating;
            String idGenre = testData.idGenre;
            String playcount = testData.playcount;
            printWriter.println(user + "," + rating + "," + idGenre + "," + playcount);
        }
        printWriter.close();
    }

    private static ArrayList<List<String>> readCsvToList(String csvFileLocation) {
        ArrayList<List<String>> records = new ArrayList<>();
        CsvParser parser = new CsvParser(settingsCsvParser());
        parser.beginParsing(new File(csvFileLocation));
        try {
            String[] row;
            int loop = 1;
            int threshold = 5000000;
            while ((row = parser.parseNext()) != null) {
                records.add(Arrays.asList(row));
                if (loop >= threshold) {
                    System.out.println("load record :" + loop);
                }
                loop++;
            }
            System.out.println("record size :" + records.size());
            return records;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    private static CsvParserSettings settingsCsvParser() {
        CsvParserSettings settings = new CsvParserSettings(); // you'll find many options here, check the tutorial.
        RowListProcessor rowProcessor = new RowListProcessor();
        settings.setNullValue("<NULL>");
        settings.setEmptyValue("<EMPTY>");
        settings.setIgnoreLeadingWhitespaces(false);
        settings.setIgnoreTrailingWhitespaces(false);
        settings.setNumberOfRecordsToRead(10000000);
        settings.setSkipEmptyLines(false);
        settings.setMaxCharsPerColumn(10000000);
        settings.setInputBufferSize(1000);
        settings.setReadInputOnSeparateThread(false);
        settings.setLineSeparatorDetectionEnabled(true);
        settings.setRowProcessor(rowProcessor);
        settings.setHeaderExtractionEnabled(true);
        return settings;
    }

    private static Timer runTimer() {
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            private int i = 0;

            public void run() {
                System.out.println("time running :" + i++); /*difference time*/
            }
        };
        timer.scheduleAtFixedRate(task, 0, 1000); //1000ms = 1sec
        return timer;
    }
}
