package com.example.loop.pengujian;

import com.example.loop.model.PrediksiLaguDataV2;
import com.example.loop.model.ItemPlaycountData;
import com.example.loop.model.SessionData;
import com.example.loop.model.SessionLogData;
import com.example.loop.model.TemporalDiversityDataV2;
import com.example.loop.utilities.EmAlgoritmaV2;
import com.example.loop.utilities.Utility;
import com.univocity.parsers.common.processor.RowListProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class Pengujian2 {

    static int[] fold1 = {6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97};
    static int[] fold2 = {98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189};
    static int[] fold3 = {190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278, 279, 280, 281};
    static int[] fold4 = {282, 283, 284, 285, 286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 298, 299, 300, 301, 302, 303, 304, 305, 306, 307, 308, 309, 310, 311, 312, 313, 314, 315, 316, 317, 318, 319, 320, 321, 322, 323, 324, 325, 326, 327, 328, 329, 330, 331, 332, 333, 334, 335, 336, 337, 338, 339, 340, 341, 342, 343, 344, 345, 346, 347, 348, 349, 350, 351, 352, 353, 354, 355, 356, 357, 358, 359, 360, 361, 362, 363, 364, 365, 366, 367, 368, 369, 370, 371, 372, 373};
    static int[] fold5 = {374, 375, 376, 377, 378, 379, 380, 381, 382, 383, 384, 385, 386, 387, 388, 389, 390, 391, 392, 393, 394, 395, 396, 397, 398, 399, 400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 418, 419, 420, 421, 422, 423, 424, 425, 426, 427, 428, 429, 430, 431, 432, 433, 434, 435, 436, 437, 438, 439, 440, 441, 442, 443, 444, 445, 446, 447, 448, 449, 450, 451, 452, 453, 454, 455, 456, 457, 458, 459, 460, 461, 462, 463, 464, 465};
    static int[] fold6 = {466, 467, 468, 469, 470, 471, 472, 473, 474, 475, 476, 477, 478, 479, 480, 481, 482, 483, 484, 485, 486, 487, 488, 489, 490, 491, 492, 493, 494, 495, 496, 497, 498, 499, 500, 501, 502, 503, 504, 505, 506, 507, 508, 509, 510, 511, 512, 513, 514, 515, 516, 517, 518, 519, 520, 521, 522, 523, 524, 525, 526, 527, 528, 529, 530, 531, 532, 533, 534, 535, 536, 537, 538, 539, 540, 541, 542, 543, 544, 545, 546, 547, 548, 549, 550, 551, 552, 553, 554, 555, 556, 557};
    static int[] fold7 = {558, 559, 560, 561, 562, 563, 564, 565, 566, 567, 568, 569, 570, 571, 572, 573, 574, 575, 576, 577, 578, 579, 580, 581, 582, 583, 584, 585, 586, 587, 588, 589, 590, 591, 592, 593, 594, 595, 596, 597, 598, 599, 600, 601, 602, 603, 604, 605, 606, 607, 608, 609, 610, 611, 612, 613, 614, 615, 616, 617, 618, 619, 620, 621, 622, 623, 624, 625, 626, 627, 628, 629, 630, 631, 632, 633, 634, 635, 636, 637, 638, 639, 640, 641, 642, 643, 644, 645, 646, 647, 648, 649};
    static int[] fold8 = {650, 651, 652, 653, 654, 655, 656, 657, 658, 659, 660, 661, 662, 663, 664, 665, 666, 667, 668, 669, 670, 671, 672, 673, 674, 675, 676, 677, 678, 679, 680, 681, 682, 683, 684, 685, 686, 687, 688, 689, 690, 691, 692, 693, 694, 695, 696, 697, 698, 699, 700, 701, 702, 703, 704, 705, 706, 707, 708, 709, 710, 711, 712, 713, 714, 715, 716, 717, 718, 719, 720, 721, 722, 723, 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 734, 735, 736, 737, 738, 739, 740, 741};
    static int[] fold9 = {742, 743, 744, 745, 746, 747, 748, 749, 750, 751, 752, 753, 754, 755, 756, 757, 758, 759, 760, 761, 762, 763, 764, 765, 766, 767, 768, 769, 770, 771, 772, 773, 774, 775, 776, 777, 778, 779, 780, 781, 782, 783, 784, 785, 786, 787, 788, 789, 790, 791, 792, 793, 794, 795, 796, 797, 798, 799, 800, 801, 802, 803, 804, 805, 806, 807, 808, 809, 810, 811, 812, 813, 814, 815, 816, 817, 818, 819, 820, 821, 822, 823, 824, 825, 826, 827, 828, 829, 830, 831, 832, 833};
    static int[] fold10 = {834, 835, 836, 837, 838, 839, 840, 841, 842, 843, 844, 845, 846, 847, 848, 849, 850, 851, 852, 853, 854, 855, 856, 857, 858, 859, 860, 861, 862, 863, 864, 865, 866, 867, 868, 869, 870, 871, 872, 873, 874, 875, 876, 877, 878, 879, 880, 881, 882, 883, 884, 885, 886, 887, 888, 889, 890, 891, 892, 893, 894, 895, 896, 897, 898, 899, 900, 901, 902, 903, 904, 905, 906, 907, 908, 909, 910, 911, 912, 913, 914, 915, 916, 917, 918, 919, 920, 921, 922, 923, 924, 925, 926, 927, 928, 929, 930};

    public static void main(String[] args) {
        List<List<PrediksiLaguDataV2>> listRecomendationAlluser = new ArrayList<>();
        ArrayList<List<String>> datasetV1 = readCsvToList("C:/dataset/data_asli.csv");
        List<SessionLogData> semuaLog = recordAllLog(datasetV1);
        Collections.sort(semuaLog);

        for (Integer userId : fold2) {
            System.out.println("user ke :" + userId);
            try {
//                Integer userId = 10; // user yang akan di hitung rekomendasinya.
                Integer threshold = 30; // Threshold menentukan jeda dalam generate suatu session. dihitung dalam satuan menit
                Integer topK = 30; // Top K menentukan jumlah session similar yang akan di gunakan untuk rekomendasi
                Double epsilon = 0.001; // epsilon yang menentukan semua param convergence atau tidak.
                Integer limitList = 500; // menentukan jumlah lagu yang akan di tampilkan utk di rekomendasi
                Integer digitLimit = 4; // digit limit membatasi berapa angka di belakang koma dari sebuah nilai// .

                List<SessionLogData> userLog = recordUserLog(datasetV1, userId);

                if (userLog.size() == 0) {
                    System.out.println("Hasil = " + "User belum ada log");
                }
                List<SessionData> semuaSession = generateSession(semuaLog, threshold);
                List<SessionData> userSession = generateSession(userLog, threshold);
                System.out.println("semuaSession " + "=" + semuaSession.size());
                System.out.println("userSession " + "=" + userSession.size());

                if (userSession.size() == 0 || semuaSession.size() == 0) {
                    System.out.println("Hasil = " + new ArrayList<>());
                }
                List<PrediksiLaguDataV2> prediksiLagu = runAlgorithm(semuaSession, userSession, digitLimit, topK, epsilon);
                List<PrediksiLaguDataV2> limitHasilPrediksi = limitHasilPrediksi(prediksiLagu, limitList);
                System.out.println("Hasil = " + limitHasilPrediksi);
                listRecomendationAlluser.add(limitHasilPrediksi);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Hasil = " + e.toString());
            }

        }
        writeRecommendation(listRecomendationAlluser);
    }

    private static List<PrediksiLaguDataV2> runAlgorithm(
            List<SessionData> semuaSession,
            List<SessionData> userSession,
            Integer digitLimit,
            Integer topK,
            Double epsilon
    ) {
        SessionData activeUserSession = userSession.get(userSession.size() - 1);
        List<TemporalDiversityDataV2> temporalDiversityData = makeTemporalDiversity(semuaSession);
        TemporalDiversityDataV2 temporalActiveSession = makeTemporalDiversityActive(activeUserSession);
        temporalDiversityData.add(temporalActiveSession); // Menambahkan active session taruh di ke paling terakhir
        return EmAlgoritmaV2.build(digitLimit, topK, epsilon, temporalDiversityData);
    }

    private static List<SessionData> generateSession(List<SessionLogData> sessionLog, Integer threshold) {
        List<SessionData> allSession = new ArrayList<>();
        List<SessionLogData> listLog = new ArrayList<>();
        SessionData sessionData = new SessionData();
        Date lastDate = sessionLog.get(0).created_date;
        Integer recentUser = sessionLog.get(0).id_user;
        int idSession = 1;
        int run = 1;
        for (SessionLogData log : sessionLog) {
            Date currentDate = log.created_date;
            Integer idUser = log.id_user;
            if (!recentUser.equals(idUser)) {
                sessionData.sessionLogData = listLog; // add list Log
                sessionData.id = idSession;
                sessionData.id_user = recentUser;
                sessionData.created_date = lastDate;
                allSession.add(sessionData); //Add Session
                sessionData = new SessionData(); //Create New Session
                listLog = new ArrayList<>();
                idSession++;
            } else {
                if (lastDate.before(currentDate)) {
                    long dif = getDateDiffMinutes(lastDate, currentDate);
                    if (dif >= threshold) {    // cek threshold untuk generate session
                        sessionData.sessionLogData = listLog; // add list Log
                        sessionData.id = idSession;
                        sessionData.id_user = recentUser;
                        sessionData.created_date = lastDate;
                        allSession.add(sessionData); //Add Session
                        sessionData = new SessionData(); //Create New Session
                        listLog = new ArrayList<>();
                        idSession++;
                    }
                }
            }

            listLog.add(log);    // add log ke list
            lastDate = currentDate;
            recentUser = idUser;

            if (run == sessionLog.size()) {
                sessionData.sessionLogData = listLog; // add list Log
                sessionData.id = idSession;
                sessionData.id_user = recentUser;
                sessionData.created_date = lastDate;
                allSession.add(sessionData); //Add Session
                sessionData = new SessionData(); //Create New Session
                listLog = new ArrayList<>();
                idSession++;
            }
            run++;
        }
        Collections.sort(allSession);
        return allSession;
    }

    private static long getDateDiffMinutes(Date lastDate, Date currentDate) {
        long diffTime = currentDate.getTime() - lastDate.getTime();
        long diffMinutes = diffTime / (60 * 1000);
        return TimeUnit.MINUTES.convert(diffMinutes, TimeUnit.MINUTES);
    }

    private static List<TemporalDiversityDataV2> makeTemporalDiversity(List<SessionData> allSession) {
        Calendar cal = Calendar.getInstance();
        List<TemporalDiversityDataV2> temporalList = new ArrayList<>();

        for (int i = 0; i < allSession.size(); i++) {
            List<SessionLogData> listSessionLog = allSession.get(i).sessionLogData;
            List<Integer> listLogIdSong = new ArrayList<>();
            for (SessionLogData log : listSessionLog) {
                listLogIdSong.add(log.id_song);
            }
            HashMap<Integer, Integer> occurrenceId = Utility.countOccurrenceNumber(listLogIdSong);
            if (listLogIdSong.size() >= 2) { // session yg isi lagunya kurang dari 2 gk akan di ikut di hitung
                TemporalDiversityDataV2 temporalDiversityData = new TemporalDiversityDataV2();
                Date date = allSession.get(i).created_date;
                cal.setTime(date);
                Set<Integer> uniqueLogSong = new HashSet<>(listLogIdSong);
                temporalDiversityData.id = allSession.get(i).id;
                temporalDiversityData.hour = cal.get(Calendar.HOUR_OF_DAY);
                temporalDiversityData.weekday = cal.get(Calendar.WEEK_OF_MONTH);
                temporalDiversityData.dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
                temporalDiversityData.month = cal.get(Calendar.MONTH) + 1;
                temporalDiversityData.diversity = (float) limitDigit((float) uniqueLogSong.size() / (float) listLogIdSong.size());

                List<ItemPlaycountData> listSongDataV2 = new ArrayList<>();
                for (Integer idSong : uniqueLogSong) {
                    ItemPlaycountData songData = new ItemPlaycountData();
                    songData.id = idSong;
                    songData.playcount = occurrenceId.get(idSong);
                    listSongDataV2.add(songData);
                }
                temporalDiversityData.songData = listSongDataV2;
                temporalList.add(temporalDiversityData);
            }


        }
        return temporalList;
    }

    private static TemporalDiversityDataV2 makeTemporalDiversityActive(SessionData allSession) {
        Calendar cal = Calendar.getInstance();
        List<SessionLogData> listSessionLog = allSession.sessionLogData;
        List<Integer> listLogIdSong = new ArrayList<>();
        for (SessionLogData log : listSessionLog) {
            listLogIdSong.add(log.id_song);
        }
        HashMap<Integer, Integer> occurrenceId = Utility.countOccurrenceNumber(listLogIdSong);
        TemporalDiversityDataV2 temporalDiversityData = new TemporalDiversityDataV2();
        Date date = allSession.created_date;
        cal.setTime(date);
        Set<Integer> uniqueLogSong = new HashSet<>(listLogIdSong);

        temporalDiversityData.id = allSession.id;
        temporalDiversityData.hour = cal.get(Calendar.HOUR_OF_DAY);
        temporalDiversityData.weekday = cal.get(Calendar.WEEK_OF_MONTH);
        temporalDiversityData.dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
        temporalDiversityData.month = cal.get(Calendar.MONTH) + 1;
        temporalDiversityData.diversity = (float) limitDigit((float) uniqueLogSong.size() / (float) listLogIdSong.size());

        List<ItemPlaycountData> listSongDataV2 = new ArrayList<>();
        for (Integer idSong : uniqueLogSong) {
            ItemPlaycountData songData = new ItemPlaycountData();
            songData.id = idSong;
            songData.playcount = occurrenceId.get(idSong);
            listSongDataV2.add(songData);
        }
        temporalDiversityData.songData = listSongDataV2;
        return temporalDiversityData;
    }

    private static List<SessionLogData> recordUserLog(ArrayList<List<String>> datasetV1, Integer idUser) {
        List<SessionLogData> dataset = new ArrayList<>();
        for (int i = 0; i < datasetV1.size(); i++) {
            SessionLogData data = new SessionLogData();
            if (Integer.parseInt(datasetV1.get(i).get(1)) == idUser) {
                data.id = Integer.parseInt(datasetV1.get(i).get(0));
                data.id_user = Integer.parseInt(datasetV1.get(i).get(1));
                data.id_song = Integer.valueOf(datasetV1.get(i).get(2));
                data.created_date = Utility.formatZDate(datasetV1.get(i).get(3));
                dataset.add(data);
            }
        }
        return dataset;
    }

    private static List<SessionLogData> recordAllLog(ArrayList<List<String>> datasetV1) {
        List<SessionLogData> dataset = new ArrayList<>();
        for (int i = 0; i < datasetV1.size(); i++) {
            int id = Integer.parseInt(datasetV1.get(i).get(0));
            int idUser = Integer.parseInt(datasetV1.get(i).get(1));
            int idSong = Integer.valueOf(datasetV1.get(i).get(2));
            Date createdDate = Utility.formatZDate(datasetV1.get(i).get(3));

            boolean contains = IntStream.of(fold2).anyMatch(x -> x == idUser); // jika id user terkandung di dalam data fold k maka session tidak akan di masukan
            if(!contains) {
                SessionLogData data = new SessionLogData();
                data.id = id;
                data.id_user = idUser;
                data.id_song = idSong;
                data.created_date = createdDate;
                dataset.add(data);
            }
        }
        return dataset;
    }

    private static ArrayList<List<String>> readCsvToList(String csvFileLocation) {
        ArrayList<List<String>> records = new ArrayList<>();
        CsvParser parser = new CsvParser(settingsCsvParser());
        parser.beginParsing(new File(csvFileLocation));
        try {
            String[] row;
            int loop = 1;
            int threshold = 5000000;
            while ((row = parser.parseNext()) != null) {
                records.add(Arrays.asList(row));
                if (loop >= threshold) {
                    System.out.println("load record :" + loop);
                }
                loop++;
            }
            System.out.println("record size :" + records.size());
            return records;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    private static CsvParserSettings settingsCsvParser() {
        CsvParserSettings settings = new CsvParserSettings(); // you'll find many options here, check the tutorial.
        RowListProcessor rowProcessor = new RowListProcessor();
        settings.setNullValue("<NULL>");
        settings.setEmptyValue("<EMPTY>");
        settings.setIgnoreLeadingWhitespaces(false);
        settings.setIgnoreTrailingWhitespaces(false);
        settings.setNumberOfRecordsToRead(10000000);
        settings.setSkipEmptyLines(false);
        settings.setMaxCharsPerColumn(10000000);
        settings.setInputBufferSize(1000);
        settings.setReadInputOnSeparateThread(false);
        settings.setLineSeparatorDetectionEnabled(true);
        settings.setRowProcessor(rowProcessor);
        settings.setHeaderExtractionEnabled(true);
        return settings;
    }

    private static double limitDigit(double value) {
        return Math.round(value * Math.pow(10, 3)) / Math.pow(10, 3);
    }

    private static List<PrediksiLaguDataV2> limitHasilPrediksi(List<PrediksiLaguDataV2> prediksiLagu, int limitAngka) {
        List<PrediksiLaguDataV2> limitPrediksi = new ArrayList<>();
        for (int i = 0; i < prediksiLagu.size(); i++) {
            if (i == limitAngka) {
                break;
            }
            PrediksiLaguDataV2 rate = new PrediksiLaguDataV2();
            rate.ratingLagu = prediksiLagu.get(i).ratingLagu;
            rate.songData = prediksiLagu.get(i).songData;
            limitPrediksi.add(rate);
        }
        return limitPrediksi;
    }

    private static void writeText(List<SessionLogData> s) {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("log_sorted.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert fileWriter != null;
        PrintWriter printWriter = new PrintWriter(fileWriter);
        for (SessionLogData testData : s) {
            printWriter.println(testData.id_user + "," + testData.created_date);
        }
        printWriter.close();
    }

    private static void writeRecommendation(List<List<PrediksiLaguDataV2>> prediksiLaguDataV2) {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("rekomendasi_fold10.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert fileWriter != null;
        PrintWriter printWriter = new PrintWriter(fileWriter);
        int pos = 0;
        printWriter.println("id_user,rating,id_song,playcount");
        for (List<PrediksiLaguDataV2> listPrediksi : prediksiLaguDataV2) {
            for (PrediksiLaguDataV2 testData : listPrediksi) {
                printWriter.println(fold2[pos] + "," + testData);
            }
            pos++;
        }
        printWriter.close();
    }
}
