package com.example.loop.model;

public class KlDivergence implements Comparable {

    public Integer position;

    public Double divergence;


//    @Override
//    public double compareTo(KlDivergence o) {
//        return(divergence - o.divergence);     // Gunakan ini untuk sort bedasarkan id, angka, atau integer karena lebih tepat
//    }
     @Override
     public int compareTo(Object o) {
         return this.divergence.compareTo(((KlDivergence) o).divergence);    // INI UNTUK SORT BERDASARKAN DIVERGENCE
     }
}
