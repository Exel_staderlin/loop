package com.example.loop.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.hibernate.annotations.CreationTimestamp;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "playlist", schema = "SYS")
public class PlaylistData {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @JsonProperty("id")
    public int id;

    @JsonProperty("id_user")
    public int id_user;

    @JsonProperty("title")
    public String title;

    @JsonProperty("image")
    public String image;

    @CreationTimestamp
    @JsonFormat(pattern="dd-MM-yyyy hh:mm")
    @Column(name = "created_date")
    @JsonProperty("created_date")
    public Date created_date;
}