package com.example.loop.model;

/**
 * Created by Exel staderlin on 10/22/2019.
 */
public class PrediksiLaguDataV2 implements Comparable{

    public Double ratingLagu;

    public ItemPlaycountData songData;

//    public SongData songData;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((songData == null) ? 0 : songData.hashCode());
        return result;                                                                                 // INI GUNANYA UNTUK SET UNIQUE VALUES BERDASARKAN songData
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;                                          // INI GUNANYA UNTUK SET UNIQUE VALUES BERDASARKAN songData
        PrediksiLaguDataV2 other = (PrediksiLaguDataV2) obj;
        if (songData == null) {
            if (other.songData != null)
                return false;
        } else if (!songData.equals(other.songData))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return + ratingLagu + "," + songData;
    }

    @Override
    public int compareTo(Object o) {
        return this.ratingLagu.compareTo(((PrediksiLaguDataV2) o).ratingLagu);    // INI UNTUK SORT BERDASARKAN ratingLagu
    }

}
