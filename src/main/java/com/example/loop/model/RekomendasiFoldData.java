package com.example.loop.model;

public class RekomendasiFoldData implements Comparable<RekomendasiFoldData> {

    public String user;
    public Double rating;
    public Integer idGenre;
    public Integer idSong;
    public String playcount;
    public Integer priority;


    @Override
    public int compareTo(RekomendasiFoldData o) {
        int i = rating.compareTo(o.rating);// INI UNTUK SORT BERDASARKAN ratingLagu
        if (i != 0) return i;

        return Integer.compare(priority, o.priority);

    }
}
