package com.example.loop.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.lang.Nullable;

import lombok.Getter;
import lombok.Setter;

@XmlRootElement
@Getter
@Setter
@Entity
@Table(name = "search")
public class SearchLogData {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @JsonProperty("id")
    private int id;

    @JsonProperty("id_user")
    public int id_user;

    @Nullable
    @JsonProperty("id_playlist")
    public Integer id_playlist;

    @Nullable
    @JsonProperty("id_song")
    @Column(name = "id_song")
    public Integer id_song;

    @Nullable
    @JsonProperty("id_artist")
    @Column(name = "id_artist")
    public Integer id_artist;
 
    @OneToOne
    @JoinColumn(name="id_playlist", insertable = false, updatable = false)
    @RestResource(path = "playlist", rel="playlist")    
    @JsonProperty("playlist")
    private PlaylistData playlist;

    @OneToOne
    @JoinColumn(name="id_song", insertable = false, updatable = false)
    @RestResource(path = "song", rel="song")    
    @JsonProperty("songs")
    private SongData songs;
    
    @OneToOne
    @JoinColumn(name="id_artist", insertable = false, updatable = false)
    @RestResource(path = "artist", rel="artist")    
    @JsonProperty("artist")
    private ArtistData artist;
}
