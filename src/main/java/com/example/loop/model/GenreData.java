package com.example.loop.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "genre")
@ApiModel(value = "GenreData", description = "genre")
public class GenreData{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @JsonProperty("id")
    @ApiModelProperty(notes = "The database generated aritst ID")
    public int id;

    @JsonProperty("name")
    public String name;

    @Override
    public String toString() {
        return name + "," + id;
    }

}