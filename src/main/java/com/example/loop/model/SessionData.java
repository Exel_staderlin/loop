package com.example.loop.model;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SessionData implements Comparable<SessionData>{

    public int id;

    public int id_user;

    @JsonFormat(pattern="dd-MM-yyyy hh:mm:ss")
    public Date created_date;

    public List<SessionLogData> sessionLogData;

    @Override
    public int compareTo(SessionData o) {
        return this.created_date.compareTo(o.created_date);    // INI UNTUK SORT BERDASARKAN date
    }

}