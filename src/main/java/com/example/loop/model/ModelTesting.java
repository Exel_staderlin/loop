package com.example.loop.model;

public class ModelTesting implements Comparable<ModelTesting>{

    public String id;
    public String id_user;
    public String liked_genre;
    public Integer playcount;
    public Integer jml_genre_song;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((playcount == null) ? 0 : playcount.hashCode());
        return result;                                                                                 // INI GUNANYA UNTUK SET UNIQUE VALUES BERDASARKAN LAGU
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;                                          // INI GUNANYA UNTUK SET UNIQUE VALUES BERDASARKAN LAGU
        ModelTesting other = (ModelTesting) obj;
        if (playcount == null) {
            if (other.playcount != null)
                return false;
        } else if (!playcount.equals(other.playcount))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "DataSet{" +
                "song='" + playcount + '\'' +
                ", artist=" + playcount +
                '}';
    }

    @Override
    public int compareTo(ModelTesting o) {
        return(Integer.valueOf(playcount) - Integer.valueOf(o.playcount));     // Gunakan ini untuk sort bedasarkan id, angka, atau integer karena lebih tepat
    }
}
