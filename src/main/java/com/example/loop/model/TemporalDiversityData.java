package com.example.loop.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@XmlRootElement
@Getter
@Setter
public class TemporalDiversityData {

    public int id;

    public int hour;

    public int weekday;

    public int dayOfMonth;

    public int month;

    public float diversity;

    public List<SongData> songData;


//    @Override
//    public int hashCode() {
//        return new HashCodeBuilder()
//                .append(id)
//                .append(name)
//                .append(salary)
//                .toHashCode();
//    }
//
//    @Override
//    public boolean equals(Object obj) {
//        if (obj instanceof Employee) {
//            final Employee employee = (Employee) obj;
//
//            return new EqualsBuilder()
//                    .append(id, employee.id)
//                    .append(id, employee.name)
//                    .append(id, employee.salary)
//                    .isEquals();
//        } else {
//            return false;
//        }
//    }
//
}