package com.example.loop.model;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@XmlRootElement
@Getter
@Setter
public class SessionLogCountData {

    public int id;
//
//    public int id_session;
//
//    public int id_session_user;

    public int id_user;

    public int id_song;

    public String created_date;

    public int playcount;

    public SongData songs;
}