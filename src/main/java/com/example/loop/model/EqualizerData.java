package com.example.loop.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "equalizer")
@ApiModel(value = "EqualizerData", description = "equalizer")
public class EqualizerData{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @JsonProperty("id")
    @ApiModelProperty(notes = "The database generated equalizer ID")
    public int id;

    @JsonProperty("id_user")
    public int id_user;

    @JsonProperty("frequency")
    public String frequency;

    @Override
    public String toString() {
        return id_user + "= " + frequency;
    }

}