package com.example.loop.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.models.auth.In;

/**
 * Created by Exel staderlin on 9/17/2019.
 */
public class PrediksiLaguData implements Comparable<PrediksiLaguData> {

    @JsonProperty("weight")
    public Double ratingLagu;

    public Integer playcount;

    @JsonProperty("songs")
    public SongData songData;

    public Integer priority;


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((songData == null) ? 0 : songData.hashCode());
        return result;                                                                                 // INI GUNANYA UNTUK SET UNIQUE VALUES BERDASARKAN songData
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;                                          // INI GUNANYA UNTUK SET UNIQUE VALUES BERDASARKAN songData
        PrediksiLaguData other = (PrediksiLaguData) obj;
        if (songData == null) {
            if (other.songData != null)
                return false;
        } else if (!songData.equals(other.songData))
            return false;
        return true;
    }


    @Override
    public int compareTo(PrediksiLaguData o) {
        int i = ratingLagu.compareTo(o.ratingLagu);// INI UNTUK SORT BERDASARKAN ratingLagu
        if (i != 0) return i;

        return Integer.compare(priority, o.priority);

    }

}
