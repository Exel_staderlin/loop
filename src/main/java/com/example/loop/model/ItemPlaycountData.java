package com.example.loop.model;

/**
 * Created by Exel staderlin on 10/22/2019.
 */
public class ItemPlaycountData implements Comparable{

    public Integer id;

    public Integer playcount;


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;                                                                                 // INI GUNANYA UNTUK SET UNIQUE VALUES BERDASARKAN id LAGU
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;                                          // INI GUNANYA UNTUK SET UNIQUE VALUES BERDASARKAN id LAGU
        ItemPlaycountData other = (ItemPlaycountData) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return + id + "," + playcount;
    }

    @Override
    public int compareTo(Object o) {
        return this.playcount.compareTo(((ItemPlaycountData) o).playcount);    // INI UNTUK SORT BERDASARKAN playcount lagu
    }

}
