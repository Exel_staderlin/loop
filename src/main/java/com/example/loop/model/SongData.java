package com.example.loop.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@XmlRootElement
@Getter
@Setter
@Entity
@Table(name = "song")
public class SongData implements Comparable{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @JsonProperty("id")
    public Integer id;

    @JsonProperty(access = Access.WRITE_ONLY)
    public int id_artist;

    @JsonProperty("title")
    public String title;

    @JsonProperty("link_url")
    public String linkUrl;

    @JsonProperty("playcount")
    public int playcount;

    @OneToOne
    @JoinColumn(name = "id_artist",insertable=false, updatable=false)
    @RestResource(path = "artist", rel="artist")
    @JsonProperty("artist")
    public ArtistData artistData;

    @JsonProperty("status")
    public int status;

    @JsonProperty("duration")
    public String duration;

    @JsonProperty("id_genre")
    public int id_genre;

    @Override
    public String toString() {
        return title;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;                                                                                 // INI GUNANYA UNTUK SET UNIQUE VALUES BERDASARKAN id LAGU
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;                                          // INI GUNANYA UNTUK SET UNIQUE VALUES BERDASARKAN id LAGU
        SongData other = (SongData) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }


    @Override
    public int compareTo(Object o) {
        return this.id.compareTo(((SongData) o).id);    // INI UNTUK SORT BERDASARKAN ratingLagu
    }

}