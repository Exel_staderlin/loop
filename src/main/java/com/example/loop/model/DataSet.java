 package com.example.loop.model;

 public class DataSet implements Comparable<DataSet>{

     public String id;
     public String user;
     public String song;
     public String idSong;
     public String createDate;
     public String artist;
     public String idArtist;
     public String idGenre;
     public String rating;
     public String playcount;

     @Override
     public int hashCode() {
         final int prime = 31;
         int result = 1;
         result = prime * result + ((idGenre == null) ? 0 : idGenre.hashCode());
         return result;                                                                                 // INI GUNANYA UNTUK SET UNIQUE VALUES BERDASARKAN LAGU
     }

     @Override
     public boolean equals(Object obj) {
         if (this == obj)
             return true;
         if (obj == null)
             return false;
         if (getClass() != obj.getClass())
             return false;                                          // INI GUNANYA UNTUK SET UNIQUE VALUES BERDASARKAN LAGU
         DataSet other = (DataSet) obj;
         if (idGenre == null) {
             if (other.idGenre != null)
                 return false;
         } else if (!idGenre.equals(other.idGenre))
             return false;
         return true;
     }

     @Override
     public String toString() {
         return "DataSet{" +
                 "song='" + song +  '\'' +
                 ", artist=" + artist +
                 '}';
     }

//     @Override
//     public int compareTo(DataSet o) {
//         return this.user.compareTo(o.user);    // INI UNTUK SORT BERDASARKAN ARTIST
//     }
     @Override
     public int compareTo(DataSet o) {
         return(Integer.valueOf(user) - Integer.valueOf(o.user));     // Gunakan ini untuk sort bedasarkan id, angka, atau integer karena lebih tepat
     }
 }
