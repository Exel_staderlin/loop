package com.example.loop.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import org.springframework.data.rest.core.annotation.RestResource;

import lombok.Getter;
import lombok.Setter;

@XmlRootElement
@Getter
@Setter
@Entity
@Table(name = "playlist_song")
public class PlaylistSongData implements Serializable{


  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  @JsonProperty("id")
  private int id;

  @JsonProperty("id_playlist")
  public int id_playlist;
  
  @JsonProperty(access = Access.WRITE_ONLY)
  @Column(name = "id_song")
  public int id_song;

  // @OneToMany(targetEntity=SongData.class, mappedBy="id",cascade=CascadeType.ALL, fetch = FetchType.LAZY)    
  // @JoinColumn(name="id",referencedColumnName = "id_song", reinsertable = false, updatable = false)    

  @OneToOne
  @JoinColumn(name="id_song", insertable = false, updatable = false)    
  @RestResource(path = "song", rel="song")    
  @JsonProperty("songs")
  private SongData songs;

  // private SongData songs = new SongData();   // Ini penyebab nilai null selalu di input


}