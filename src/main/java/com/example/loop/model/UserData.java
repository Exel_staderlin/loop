package com.example.loop.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@XmlRootElement
@Getter
@Setter
@Entity
@Table(name = "user")
public class UserData {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @JsonProperty("id")
    public int id;

    @JsonProperty("name")
    public String name;

    @JsonProperty(access = Access.WRITE_ONLY)
    public String password;

    @JsonProperty("email")
    public String email;

    @CreationTimestamp
    @JsonFormat(pattern="dd-MM-yyyy hh:mm")
    @JsonProperty("registered_date")
    @Column(name = "registered_date")
    public Date registeredDate;

    @JsonProperty("image")
    public String image;
    
    @UpdateTimestamp
    @JsonFormat(pattern="dd-MM-yyyy hh:mm:ss")
    @JsonProperty("last_login_date")
    @Column(name = "last_login_date")
    public Date lastLoginDate;

    @JsonProperty("role")
    public int role;

    @JsonProperty("status")
    public int status;

    @PreUpdate
    protected void onUpdate() {
        lastLoginDate = new Date();
    }
}
