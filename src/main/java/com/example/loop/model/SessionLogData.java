package com.example.loop.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@XmlRootElement
@Getter
@Setter
@Entity
@Table(name = "session_log")
public class SessionLogData implements Comparable<SessionLogData>{

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @JsonProperty("id")
    public int id;

    @JsonProperty("id_user")
    public int id_user;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Column(name = "id_song")
    public Integer id_song;

    @CreationTimestamp
    @JsonFormat(pattern="dd-MM-yyyy hh:mm:ss")
    @Column(name = "created_date")
    @JsonProperty("created_date")   
    public Date created_date;

    @OneToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name="id_song", insertable = false, updatable = false)
    @RestResource(path = "song", rel="song")
    @JsonProperty("songs")
    public SongData songs;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id_song == null) ? 0 : id_song.hashCode());
        return result;                                                                                 // INI GUNANYA UNTUK SET UNIQUE VALUES BERDASARKAN LAGU
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;                                          // INI GUNANYA UNTUK SET UNIQUE VALUES BERDASARKAN LAGU
        SessionLogData other = (SessionLogData) obj;
        if (id_song == null) {
            if (other.id_song != null)
                return false;
        } else if (!id_song.equals(other.id_song))
            return false;
        return true;
    }

    @Override
    public int compareTo(SessionLogData o) {
//        return this.id_user.compareTo(o.id_user);    // INI UNTUK SORT BERDASARKAN ARTIST

        return (id_user - o.id_user);     // Gunakan ini untuk sort bedasarkan id, angka, atau integer karena lebih tepat
    }
}
