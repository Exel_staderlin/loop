package com.example.loop.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@XmlRootElement
@Getter
@Setter
@Entity
@Table(name = "recommendation")
public class RecommendationData {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @JsonProperty("id")
    private int id;

    @JsonProperty("id_user")
    public int id_user;

    @JsonProperty("id_song")
    @Column(name = "id_song")
    public Integer id_song;

    @JsonProperty("weight")
    public String weight;

    @OneToOne
    @JoinColumn(name="id_song", insertable = false, updatable = false)
    @RestResource(path = "song", rel="song")    
    @JsonProperty("songs")
    public SongData songs;

}
