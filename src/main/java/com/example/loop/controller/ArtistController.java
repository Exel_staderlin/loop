package com.example.loop.controller;

import com.example.loop.model.ArtistData;
import com.example.loop.model.RecommendationData;
import com.example.loop.repository.ArtistRepository;
import com.example.loop.repository.RecommendationRepository;
import com.example.loop.repository.SongRepository;
import com.example.loop.utilities.BaseDataRx;
import com.example.loop.utilities.BaseRx;
import com.example.loop.utilities.Utility;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api")
public class ArtistController {

    @Autowired
    private ArtistRepository artistRepository;

    @Autowired
    private SongRepository songRepository;

    @Autowired
    private RecommendationRepository recommendationRepository; //utk bot search image


    @PostMapping(path = "/add/artist")
    public ResponseEntity<?> addArtist(@RequestParam HashMap<String, String> map) {
        try {
            artistRepository.save(getArtistData(map));
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(new BaseRx().bodyCallback(true, "Artist telah di tambahkan"));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }

    @PostMapping(path = "/edit/artist/{id}")
    public ResponseEntity<?> editArtist(@PathVariable(value = "id") int id, @RequestParam HashMap<String, String> param) {
        try {
            ArtistData artistData = artistRepository.findById(id);
            ArtistData response = artistRepository.save(updateArtist(artistData, param));
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(new BaseDataRx<ArtistData>().bodyDataCallback(true, response, "Artist telah di update"));                         //EDIT PLAYLIST IMAGE
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseDataRx<ArtistData>().bodyCallback(false, e.toString()));
        }
    }

    @DeleteMapping("/artist/delete/{id}")
    public ResponseEntity<?> deleteArtist(@PathVariable(value = "id") Integer id) {
        try {
            artistRepository.deleteById(id);
            songRepository.deleteByIdArtist(id);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(new BaseRx().bodyCallback(true, "Artist telah di hapus"));                         //DELETE ARTIST
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }


    @GetMapping(path = "/artist")
    public ResponseEntity<?> allArtist() {
        try {
            List<ArtistData> response = artistRepository.findAlls();
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(new BaseDataRx<ArtistData>().bodyListCallback(true, response, "true"));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }

    @GetMapping(path = "/artist/bot_search_image/{id_user}")
    public ResponseEntity<?> searchImageArtistBot(@PathVariable(value = "id_user") Integer id) {
        try {
            List<RecommendationData> listData = recommendationRepository.findByIdUser(id);
            for(RecommendationData recommendationData : listData) {
                ArtistData artist = recommendationData.songs.artistData;
                if(artist.image == null || artist.image.equals("") || artist.image.equals("https://www.logolynx.com/images/logolynx/b4/b4ef8b89b08d503b37f526bca624c19a.jpeg")) {
                    String key="AIzaSyCuONXnwnIKSu7-UlsATXeBovxwGmaU9fw";
                    String qry = Utility.escapeSql(artist.name + " " + "Album Artist Song");
                    URL url = new URL(
                            "https://www.googleapis.com/customsearch/v1?key="+key+ "&cx=013036536707430787589:_pqjad5hr1a&q="+ qry + "&as_filetype=png");
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setRequestProperty("Accept", "application/json");
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            (conn.getInputStream())));
    
                    String output;
                    System.out.println("Output from Server .... \n");
    
                    List<String> listImage = new ArrayList<>();
                    while ((output = br.readLine()) != null) {
                        if(output.contains("\"og:image\": \"")){              
                            try {
                                String link=output.substring(25, output.indexOf("\","));
                                if(link.contains("https") && link.length() < 255)
                                    listImage.add(link);
                            }catch (Exception e) {
                            }  
                        
                        }     
                    }
                    conn.disconnect();
                    String lastImageSearched = "";
                    if(listImage.size() != 0) 
                        lastImageSearched = listImage.get(listImage.size() - 1);
                    else
                        lastImageSearched = artist.image;

                    System.out.println("id : " + artist.id);
                    System.out.println("artist : " + artist.name);
                    System.out.println("new image : " +lastImageSearched);
                    artist.image = lastImageSearched;
                    ArtistData response = artistRepository.save(artist); //update image of artist
                }
            }
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(new BaseRx().bodyCallback(true, "all searched completed"));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }

    @GetMapping(path = "/artist/bot_search_image/artist/{id_artist}")
    public ResponseEntity<?> searchImageByArtist(@PathVariable(value = "id_artist") Integer id) {
        try {
            ArtistData artist = artistRepository.findById(id);
                if(artist.image == null || artist.image.equals("") || artist.image.equals("https://www.logolynx.com/images/logolynx/b4/b4ef8b89b08d503b37f526bca624c19a.jpeg")) {
                    String key="AIzaSyCuONXnwnIKSu7-UlsATXeBovxwGmaU9fw";
                    String qry = Utility.escapeSql(artist.name + " " + "Album Artist Song");
                    URL url = new URL(
                            "https://www.googleapis.com/customsearch/v1?key="+key+ "&cx=013036536707430787589:_pqjad5hr1a&q="+ qry + "&as_filetype=png");
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setRequestProperty("Accept", "application/json");
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            (conn.getInputStream())));
    
                    String output;
                    System.out.println("Output from Server .... \n");
    
                    List<String> listImage = new ArrayList<>();
                    while ((output = br.readLine()) != null) {
                        if(output.contains("\"og:image\": \"")){              
                            try {
                                String link=output.substring(25, output.indexOf("\","));
                                if(link.contains("https") && link.contains("jpg") ||
                                   link.contains("https") && link.contains("jpeg") ||
                                   link.contains("https") && link.contains("png") &&
                                   link.length() < 255)
                                    listImage.add(link);
                            }catch (Exception e) {
                            }  
                        
                        }     
                    }
                    conn.disconnect();
                    String lastImageSearched = "";
                    if(listImage.size() != 0) 
                        lastImageSearched = listImage.get(listImage.size() - 1);
                    else
                        lastImageSearched = artist.image;

                    System.out.println("id : " + artist.id);
                    System.out.println("artist : " + artist.name);
                    System.out.println("new image : " +lastImageSearched);
                    artist.image = lastImageSearched;
                    ArtistData response = artistRepository.save(artist); //update image of artist
                }
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(new BaseRx().bodyCallback(true, "all searched completed"));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }

    @PostMapping(path = "/artist/{action}/{id}")
    public ResponseEntity<?> blockArtist(@PathVariable(value = "action") String action, @PathVariable(value = "id") int id) {
        try {
            ArtistData artistData = artistRepository.findById(id);
            int idArtist = artistData.id;
            if(action.equals("block")) {
                artistData.status = -1;
                songRepository.blockByIdArtist(-1, idArtist);
            }
            else {
                artistData.status = 0;
                songRepository.blockByIdArtist(0, idArtist);
            }
            ArtistData response = artistRepository.save(artistData);
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(new BaseDataRx<ArtistData>().bodyDataCallback(true, response, "Artist Telah di " + action));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseDataRx<ArtistData>().bodyDataCallback(false, null, e.toString()));
        }
    }


    private ArtistData updateArtist(ArtistData artistData, HashMap<String, String> param) {
        if (param.get("name") != null) {
            artistData.name = param.get("name");
        }
        if (param.get("image") != null) {
            artistData.image = param.get("image");
        }
        return artistRepository.save(artistData);
    }

    private ArtistData getArtistData(HashMap<String, String> map) {
        ArtistData data = new ArtistData();
        data.name = map.get("name");
        data.image = map.get("image");
        return data;
    }

}