package com.example.loop.controller;

import com.example.loop.model.SongData;
import com.example.loop.repository.SongRepository;
import com.example.loop.utilities.BaseDataRx;
import com.example.loop.utilities.BaseRx;
import com.example.loop.utilities.Utility;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api")
public class SongController {

    @Autowired
    private SongRepository songRepository;


    @GetMapping(path = "/play/song/{filename}",produces = {MediaType.APPLICATION_OCTET_STREAM_VALUE })
    public ResponseEntity<byte[]>  playAudio(HttpServletResponse response,@PathVariable(value = "filename") String filename) {

        String file = "D:\\Music\\" + filename+".mp3";
        try {
            Path path = Paths.get(file);
            response.setContentType("audio/mp3");
//            response.setHeader("Content-Disposition", "attachment; filename=\"" + "song.mp3" + "\"");  // line ini yg membuat download terus
            response.setContentLength((int) Files.size(path));
            Files.copy(path, response.getOutputStream());
            response.flushBuffer();
        } catch (Exception ignored) {
        }
        return null;
    }



    @PostMapping(path = "/add/song")
    public ResponseEntity<?> addSong(@RequestParam HashMap<String, String> map) {
        try {
            songRepository.save(getSongData(map));
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(new BaseRx().bodyCallback(true, "Song telah di tambahkan"));
        }
        catch(Exception e) {
            e.printStackTrace();
            return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new BaseRx().bodyCallback(false, e.toString()));
            }
    }

    @PostMapping(path = "/song/{action}/{id}")
    public ResponseEntity<?> blockSong(@PathVariable(value = "action") String status, @PathVariable(value = "id") int id) {
        try {
            SongData songData = songRepository.findById(id);
            if(status.equals("block")) {
                songData.status = -1;
            }
            else {
                songData.status = 0;
            }
            SongData respone = songRepository.save(songData);
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(new BaseDataRx<SongData>().bodyDataCallback(true, respone, "Song Telah block"));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseDataRx<SongData>().bodyDataCallback(false, null, e.toString()));
        }
    }

    @PostMapping(path = "/edit/song/{id}")
    public ResponseEntity<?> editSong(@PathVariable(value = "id") int id,@RequestParam HashMap<String,String> param) {
        try {
            SongData playlistData =  songRepository.findById(id);
            SongData response = songRepository.save(updateSong(playlistData, param));
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(new BaseDataRx<SongData>().bodyDataCallback(true, response,"Song telah di update"));  //EDIT SONG
        }
        catch(Exception e) {
            e.printStackTrace();
            return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new BaseDataRx<SongData>().bodyCallback(false, e.toString()));
            }
    }

    @DeleteMapping("/song/delete/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable(value = "id") Integer id) {
        try {
            songRepository.deleteById(id);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(new BaseRx().bodyCallback(true, "Song telah di hapus"));     //DELETE SONG
        }
        catch(Exception e) {
            e.printStackTrace();
            return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }


	@GetMapping(path = "/allsong")
    public ResponseEntity<?> allSong() {
        try {
            List<SongData> data = songRepository.findTopDatas();
            // writeText(data);
            if(data.size() != 0) {
            //     return ResponseEntity
            //             .status(HttpStatus.OK)
            //             .body(new BaseRx().bodyCallback(true, "Operation finish"));

               return ResponseEntity
               .status(HttpStatus.OK)
               .body(new BaseDataRx<SongData>().bodyListCallback(true, data, "true"));
            }
            else {
                return ResponseEntity
                .status(HttpStatus.OK)                 
                .body(new BaseDataRx<SongData>().bodyListCallback(false, data, "No Data Available"));
            } 
        }
        catch(Exception e) {
            e.printStackTrace();
            return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }

    private static void writeText(List<SongData> songDataList) {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("song_db.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter printWriter = new PrintWriter(fileWriter);

        for (SongData data : songDataList) {
            printWriter.println(data.id + "," + data.id_artist + "," + data.linkUrl + "," + data.title + "," + data.playcount + "," + data.status+ "," + data.duration+ "," + data.id_genre);
        }
        printWriter.close();
    }


    @GetMapping(path = "/reset-duration")
    public ResponseEntity<?> resetDurationFromLink() {
        try {
            List<SongData> data = songRepository.findAll();
            List<SongData> dataNew = new ArrayList<>();
            for(SongData songData : data) {
                songData.duration = Utility.getDurationMp3(songData.linkUrl);
                dataNew.add(songData);
                songRepository.save(songData);
            }
            if(dataNew.size() != 0) {
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .body(new BaseDataRx<SongData>().bodyListCallback(true, dataNew, "true"));
            }
            else {
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .body(new BaseDataRx<SongData>().bodyListCallback(false, data, "No Data Available"));
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }

    @GetMapping(path = "/song/{id}")
    public ResponseEntity<?> getSongById( @PathVariable("id") int id) {
        try {
            SongData data = songRepository.findById(id);
            if(data != null) {
                return ResponseEntity
                .status(HttpStatus.OK)                 
                .body(new BaseDataRx<SongData>().bodyDataCallback(true, data, "true"));
            }
            else {
                return ResponseEntity
                .status(HttpStatus.OK)                 
                .body(new BaseDataRx<SongData>().bodyDataCallback(false, data, "No Data Available"));
            } 
        }
        catch(Exception e) {
            e.printStackTrace();
            return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }


    @CrossOrigin(origins = "http://localhost:8080")
    @GetMapping(path = "/song/artist/{id}")
    public ResponseEntity<?> songByArtist( @PathVariable("id") int id) {
        try {
            List<SongData> data = songRepository.findByIdArtist(id);
            if(data.size() != 0) {
                return ResponseEntity
                .status(HttpStatus.OK)                 
                .body(new BaseDataRx<SongData>().bodyListCallback(true, data, "true"));
            }
            else {
                return ResponseEntity
                .status(HttpStatus.OK)                 
                .body(new BaseDataRx<SongData>().bodyListCallback(false, data, "No Data Available"));
            } 
        }
        catch(Exception e) {
            e.printStackTrace();
            return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }

    @CrossOrigin(origins = "http://localhost:8080")
    @GetMapping(path = "/song/genre/{id}")
    public ResponseEntity<?> songByGenre( @PathVariable("id") int id) {
        try {
            List<SongData> data = songRepository.findByIdGenre(id);
            if(data.size() != 0) {
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .body(new BaseDataRx<SongData>().bodyListCallback(true, data, "true"));
            }
            else {
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .body(new BaseDataRx<SongData>().bodyListCallback(false, data, "No Data Available"));
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }


    private SongData updateSong(SongData songData, HashMap<String,String> param) {
        if(param.get("id_artist") != null) {
            songData.id_artist = Integer.parseInt(param.get("id_artist"));
        }
        if(param.get("title") != null) {
            songData.title = param.get("title");
        }
        if(param.get("link_url") != null) {
            songData.linkUrl = param.get("link_url");
            songData.duration = Utility.getDurationMp3(songData.linkUrl);
        }
        return songRepository.save(songData);
    }

    private SongData getSongData(HashMap<String, String> map){
        SongData data = new SongData();
        data.title = map.get("title");
        data.linkUrl = map.get("link_url");
        data.id_artist = Integer.parseInt(map.get("id_artist"));
        data.playcount = 0;
        data.duration = Utility.getDurationMp3(data.linkUrl);
        return data;
    }

}