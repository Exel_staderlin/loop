package com.example.loop.controller;

import com.example.loop.model.ArtistData;
import com.example.loop.model.GenreData;
import com.example.loop.repository.ArtistRepository;
import com.example.loop.repository.GenreRepository;
import com.example.loop.repository.SongRepository;
import com.example.loop.utilities.BaseDataRx;
import com.example.loop.utilities.BaseRx;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Random;

/**
 * Created by Exel staderlin on 10/16/2019.
 */

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api")
public class GenreController {

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private SongRepository songRepository;

    @Autowired
    private ArtistRepository artistRepository;

    @GetMapping(path = "/allgenre")
    public ResponseEntity<?> getAllGenre() {
        try {
            List<GenreData> data = genreRepository.findAll();
            if(data.size() != 0) {
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .body(new BaseDataRx<GenreData>().bodyListCallback(true, data, "true"));
            }
            else {
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .body(new BaseDataRx<GenreData>().bodyListCallback(false, data, "No Data Available"));
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }

    @GetMapping(path = "/random/genre")
    public ResponseEntity<?> randomGenreOnSong() {
        try {
            List<ArtistData> artist = artistRepository.findAll();
            if(artist.size() != 0) {
                for(ArtistData artistData : artist) {
                    Random randomGenerator = new Random();
                    int randomInt = randomGenerator.nextInt(22) + 1;
                    songRepository.randomGenreOnSong(randomInt, artistData.id);
                }
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .body(new BaseRx().bodyCallback(true, "Success"));
            }
            else {
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .body(new BaseDataRx<GenreData>().bodyListCallback(false, null, "No Data Available"));
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }


}
