package com.example.loop.controller;


import com.example.loop.model.GenreData;
import com.example.loop.model.SessionLogCountData;
import com.example.loop.model.SessionLogData;
import com.example.loop.model.SongData;
import com.example.loop.repository.GenreRepository;
import com.example.loop.repository.SessionLogRepository;
import com.example.loop.repository.SongRepository;
import com.example.loop.utilities.BaseDataRx;
import com.example.loop.utilities.BaseRx;
import com.univocity.parsers.common.processor.RowListProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api")
public class SessionLogController {

    static int[] jmlFold = {1,2,3,4,5,6,7,8,9,10};
    static String rekomendasiGenrePath = "";
    static String rekomendasiGenreNameLimit = "";
    static int limit = 40;

    @Autowired
    private SessionLogRepository sessionLogRepository;

    @Autowired
    private SongRepository songRepository;

    @Autowired
    private GenreRepository genreRepository;

    @PostMapping(path = "/add/log")
    public ResponseEntity<?> addSessionLog(@RequestParam HashMap<String, String> map) {
        try {
            int idUser = Integer.parseInt(map.get("id_user"));
            int idSong = Integer.parseInt(map.get("id_song"));
            sessionLogRepository.save(getLogData(idUser, idSong)); //ini tambah log di db
            updateSongPlayCount(idSong);//ini update song playcount
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(new BaseRx().bodyCallback(true, "Log added , user = " + idUser));                    //ADD LOG
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }

    @GetMapping(path = "/log")
    public ResponseEntity<?> allLog() {
        try {
            List<SessionLogData> response = sessionLogRepository.findAlls();
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(new BaseDataRx<SessionLogData>().bodyListCallback(true, response, "true"));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }

    @GetMapping(path = "/log/session/{id_session_user}")
    public ResponseEntity<?> getLogBySession(@PathVariable("id_session_user") int id_session) {
        try {
            List<SessionLogData> response = sessionLogRepository.findByIdSessionUser(id_session);
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(new BaseDataRx<SessionLogData>().bodyListCallback(true, response, "true"));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }

    @GetMapping(path = "/log/{id_user}")
    public ResponseEntity<?> getRecentlyPlayByUser(@PathVariable("id_user") int id) {
        try {
            List<SessionLogData> response = sessionLogRepository.findLatestLog(id);
            if(response.size() == 0) {
                return ResponseEntity.status(HttpStatus.CREATED)
                        .body(new BaseDataRx<SessionLogData>().bodyListCallback(false, response, "User Belum Ada Log"));
            }
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(new BaseDataRx<SessionLogData>().bodyListCallback(true, response, "true"));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }

    @GetMapping(path = "/log/count")
    public ResponseEntity<?> getPopularSong() {
        try {
            List<Object> response = sessionLogRepository.hitungByIdSong();
            List<SessionLogCountData> list = new ArrayList<>();
            for (int i = 0; i < response.size(); i++) {
                SessionLogCountData data = new SessionLogCountData();
                Object[] row = (Object[]) response.get(i);
                System.out.println("session_log_data " + i + Arrays.toString(row));
                data.id = Integer.parseInt(row[0].toString());
//                data.id_session = Integer.parseInt(row[1].toString());
//                data.id_session_user = Integer.parseInt(row[2].toString());
                data.id_user = Integer.parseInt(row[1].toString());
                data.id_song = Integer.parseInt(row[2].toString());
                data.created_date = row[3].toString();
                data.playcount = Integer.parseInt(row[4].toString());
                data.songs = songRepository.findById(Integer.parseInt(row[2].toString()));
                list.add(data);
            }
            System.out.println(response.size());
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(new BaseDataRx<SessionLogCountData>().bodyListCallback(true, list, "true"));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }

    @GetMapping(path = "/get_rec_song")
    public ResponseEntity<?> getRecSong() {
        try {
            runGetSongData();
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(new BaseRx().bodyCallback(true, "done"));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }

    private void updateSongPlayCount(int idSong) {
        SongData data = songRepository.findById(idSong);
        int playcount = data.playcount;
        data.playcount = playcount + 1;
        songRepository.save(data);
    }

    private SessionLogData getLogData(int idUser, int idSong) {
        SessionLogData data = new SessionLogData();
        data.id_user = idUser;
        data.id_song = idSong;
        return data;
    }

    private void runGetSongData() {
        for(Integer fold : jmlFold) {
            rekomendasiGenrePath = "C:/dataset/rekomendasi_fold-limit/rekomendasi_fold"+fold+"-limit"+limit+".csv";
            rekomendasiGenreNameLimit = "C://dataset//rekomendasi_fold-limit//rekomendasi_fold"+fold+"-limit"+limit+"-title.csv";
            ArrayList<List<String>> datasetV1 = readCsvToList(rekomendasiGenrePath);
            System.out.println("loading.");
            writeRekomendasi(datasetV1);
        }
    }

    private void writeRekomendasi(ArrayList<List<String>> recommendationRecord) {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(rekomendasiGenreNameLimit);
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter printWriter = new PrintWriter(fileWriter);
        printWriter.println("id_user, rating, id_song, playcount, title_song, title_genre, id_genre");
        System.out.print(".");
        for (List<String> data : recommendationRecord) {
            SongData songData = songRepository.findById(Integer.parseInt(data.get(2))); //find song by id
            GenreData genreData;
            if(songData != null)
                genreData = genreRepository.findById(songData.id_genre); //find genre by id
            else
                genreData = new GenreData();

            printWriter.println(data.get(0) + "," + data.get(1) + "," + data.get(2) + "," + data.get(3) + "," + songData + "," + genreData);
        }
        printWriter.close();
    }

    private ArrayList<List<String>> readCsvToList(String csvFileLocation) {
        ArrayList<List<String>> records = new ArrayList<>();
        CsvParser parser = new CsvParser(settingsCsvParser());
        parser.beginParsing(new File(csvFileLocation));
        try {
            String[] row;
            int loop = 1;
            int threshold = 5000000;
            while ((row = parser.parseNext()) != null) {
                records.add(Arrays.asList(row));
                if (loop >= threshold) {
                    System.out.println("load record :" + loop);
                }
                loop++;
            }
            System.out.println("record size :" + records.size());
            return records;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    private CsvParserSettings settingsCsvParser() {
        CsvParserSettings settings = new CsvParserSettings(); // you'll find many options here, check the tutorial.
        RowListProcessor rowProcessor = new RowListProcessor();
        settings.setNullValue("<NULL>");
        settings.setEmptyValue("<EMPTY>");
        settings.setIgnoreLeadingWhitespaces(false);
        settings.setIgnoreTrailingWhitespaces(false);
        settings.setNumberOfRecordsToRead(10000000);
        settings.setSkipEmptyLines(false);
        settings.setMaxCharsPerColumn(10000000);
        settings.setInputBufferSize(1000);
        settings.setReadInputOnSeparateThread(false);
        settings.setLineSeparatorDetectionEnabled(true);
        settings.setRowProcessor(rowProcessor);
        settings.setHeaderExtractionEnabled(true);
        return settings;
    }
}