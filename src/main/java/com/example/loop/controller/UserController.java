package com.example.loop.controller;

import com.example.loop.model.PlaylistData;
import com.example.loop.model.UserData;
import com.example.loop.repository.PlaylistRepository;
import com.example.loop.repository.UserRepository;
import com.example.loop.utilities.BaseDataRx;
import com.example.loop.utilities.BaseRx;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private PlaylistRepository playlistRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @PostMapping(path = "/login")
    public ResponseEntity<?> login(@RequestParam HashMap<String, String> map) {
        try {
            UserData user = userRepository.findByEmail(map.get("email"));
            if (user == null) {
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .body(new BaseDataRx<UserData>().bodyDataCallback(false, null, "Email belum terdaftar"));
            }
            if (user.status == -1) {
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .body(new BaseDataRx<UserData>().bodyDataCallback(false, null, "User telah di block, mohon maaf silakan kontak admin kita"));
            }
            if (passwordEncoder.matches(map.get("password"), user.password)) {
                userLoginUpdateTime(user); //update user login
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .body(new BaseDataRx<UserData>().bodyDataCallback(true, user, "Login berhasil"));
            } else {
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .body(new BaseDataRx<UserData>().bodyDataCallback(false, null, "Password salah"));
            }

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseDataRx<UserData>().bodyDataCallback(false, null, "Login gagal, tolong periksa password dan email anda sekali lagi"));
        }
    }


    @PostMapping(path = "/register")
    public ResponseEntity<?> register(@RequestParam HashMap<String, String> map) {
        try {
            UserData user = userRepository.findByEmail(map.get("email"));
            if (user != null) {
                return ResponseEntity
                        .status(HttpStatus.CREATED)
                        .body(new BaseDataRx<UserData>().bodyDataCallback(false, null, "Email sudah terdaftar, silahkan coba email lain"));
            } else {
                UserData response = saveUserData(getUserData(map));
                initLikedPlaylist(response.id);
                return ResponseEntity
                        .status(HttpStatus.CREATED)
                        .body(new BaseDataRx<UserData>().bodyDataCallback(true, response, "Account sudah berhasil di daftarkan"));
            }

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseDataRx<UserData>().bodyDataCallback(false, null, e.toString()));
        }
    }

    @PostMapping(path = "/profile/update/{id}")
    public ResponseEntity<?> updateProfile(@PathVariable(value = "id") int id, @RequestParam HashMap<String, String> map) {
        try {
            UserData userData = userRepository.findById(id);
            UserData userByEmail = userRepository.findByEmail(map.get("email"));
            if (userByEmail != null) {
                if (!userData.email.equals(userByEmail.email)) {
                    return ResponseEntity
                            .status(HttpStatus.CREATED)
                            .body(new BaseDataRx<UserData>().bodyDataCallback(false, null, "Email sudah terdaftar, silahkan coba email lain"));
                }
            }
            if (map.get("password") != null && !map.get("password").equals("")) {
                if (!passwordEncoder.matches(map.get("password"), userData.password)) {
                    return ResponseEntity
                            .status(HttpStatus.CREATED)
                            .body(new BaseRx().bodyCallback(true, "Password Lama Salah"));
                }
                if (map.get("new_password") != null && !map.get("new_password").equals("")) {
                    if (passwordEncoder.matches(map.get("new_password"), userData.password)) {
                        return ResponseEntity
                                .status(HttpStatus.CREATED)
                                .body(new BaseRx().bodyCallback(true, "Password Baru Tidak Boleh Sama"));
                    }
                } else {
                    return ResponseEntity
                            .status(HttpStatus.CREATED)
                            .body(new BaseRx().bodyCallback(true, "New password harus di isi"));
                }
            }
            UserData response = updateUserData(userData, map);
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(new BaseDataRx<UserData>().bodyDataCallback(true, response, "Account Telah di update"));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseDataRx<UserData>().bodyDataCallback(false, null, e.toString()));
        }
    }

    @PostMapping(path = "/change_password/{id}")
    public ResponseEntity<?> changePassword(@PathVariable(value = "id") int id, @RequestParam HashMap<String, String> map) {
        try {
            UserData userData = userRepository.findById(id);
            String password = map.get("password");
            String newPassword = map.get("new_password");

            if (password != null && !password.equals("")) {
                if (!passwordEncoder.matches(password, userData.password)) {
                    return ResponseEntity
                            .status(HttpStatus.OK)
                            .body(new BaseRx().bodyCallback(true, "Password Lama Salah"));
                }
                if (newPassword != null && !newPassword.equals("")) {
                    if (passwordEncoder.matches(newPassword, userData.password)) {
                        return ResponseEntity
                                .status(HttpStatus.OK)
                                .body(new BaseRx().bodyCallback(true, "Password Baru Tidak Boleh Sama"));
                    }
                } else {
                    return ResponseEntity
                            .status(HttpStatus.OK)
                            .body(new BaseRx().bodyCallback(true, "New password harus di isi"));
                }
                userData.password = passwordEncoder.encode(newPassword);
            }
            UserData response = userRepository.save(userData);
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(new BaseDataRx<UserData>().bodyDataCallback(true, response, "Password Telah di update"));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseDataRx<UserData>().bodyDataCallback(false, null, e.toString()));
        }
    }


    @GetMapping(path = "/alluser")
    public ResponseEntity<?> alluser() {
        try {
            List<UserData> response = userRepository.findAll();
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(new BaseDataRx<UserData>().bodyListCallback(true, response, "true"));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseDataRx<UserData>().bodyListCallback(false, null, e.toString()));
        }
    }

    @DeleteMapping("/user/delete/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable(value = "id") Integer id) {
        try {
            userRepository.deleteById(id);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(new BaseRx().bodyCallback(true, "User telah di hapus"));                         //DELETE USER
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }

    @PostMapping(path = "/user/{action}/{id}")
    public ResponseEntity<?> blockUser(@PathVariable(value = "action") String status, @PathVariable(value = "id") int id) {
        try {
            UserData userData = userRepository.findById(id);
            if (status.equals("block")) {
                userData.status = -1;
            } else {
                userData.status = 0;
            }
            UserData respone = userRepository.save(userData);
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(new BaseDataRx<UserData>().bodyDataCallback(true, respone, "Account Telah block"));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseDataRx<UserData>().bodyDataCallback(false, null, e.toString()));
        }
    }


    private UserData getUserData(HashMap<String, String> map) {
        UserData userData = new UserData();
        userData.name = map.get("name");
        userData.email = map.get("email");
        userData.password = map.get("password");

        if (map.get("image") != null)
            userData.image = map.get("image");
        else
            userData.image = "https://www.logolynx.com/images/logolynx/b4/b4ef8b89b08d503b37f526bca624c19a.jpeg";

        if (map.get("role") != null)
            userData.role = Integer.parseInt(map.get("role"));
        else
            userData.role = 0;

        return userData;
    }

    private UserData saveUserData(UserData userData) {
        UserData user;
        user = userData;
        user.password = passwordEncoder.encode(userData.password);
        return userRepository.save(user);
    }

    private UserData updateUserData(UserData userData, HashMap<String, String> map) {
        String name = map.get("name");
        String email = map.get("email");
        String image = map.get("image");
        String password = map.get("password");
        String newPassword = map.get("new_password");
        String role = map.get("role");
        String status = map.get("status");

        if (name != null && !name.equals("")) {
            userData.name = map.get("name");
        }
        if (email != null && !email.equals("")) {
            userData.email = map.get("email");
        }
        if (image != null && !image.equals("")) {
            userData.image = image;
        }
        if (role != null) {
            userData.role = Integer.valueOf(role);
        }
        if (status != null) {
            userData.status = Integer.valueOf(status);
        }
        if (password != null && !password.equals("")) {
            if (passwordEncoder.matches(password, userData.password)) {
                userData.password = passwordEncoder.encode(newPassword);
            }
        }
        return userRepository.save(userData);
    }


    private void initLikedPlaylist(int id) {
        PlaylistData data = new PlaylistData();
        data.id_user = id;
        data.title = "Liked Songs";
        data.image = "https://cmkt-image-prd.global.ssl.fastly.net/0.1.0/ps/2396424/580/386/m1/fpnw/wm0/creative-12-.png?1489325831&s=0f9f4e19bc38c347d967a537bc28dd48";
        playlistRepository.save(data);
    }

    private UserData userLoginUpdateTime(UserData userData) {
        UserData user = new UserData();
        user = userData;
        Date date = new Date();
        user.lastLoginDate = date;
        return userRepository.save(user);
    }

}