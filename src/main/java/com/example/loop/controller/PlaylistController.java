package com.example.loop.controller;

import com.example.loop.model.PlaylistData;
import com.example.loop.model.PlaylistSongData;
import com.example.loop.repository.PlaylistRepository;
import com.example.loop.repository.PlaylistSongRepository;
import com.example.loop.utilities.BaseDataRx;
import com.example.loop.utilities.BaseRx;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api")
public class PlaylistController {

    @Autowired
    private PlaylistRepository playlistRepository;

    @Autowired
    private PlaylistSongRepository playlistSongRepository;

    @PostMapping(path = "/add/playlist/{id_user}")
    public ResponseEntity<?> addPlaylist(@PathVariable(value = "id_user") String id_user, @RequestParam HashMap<String, String> map) {
        try {
            PlaylistData playlistData = playlistRepository.findPlaylistTitle(Integer.parseInt(id_user), map.get("title"));
            if (playlistData != null) {
                return ResponseEntity
                        .status(HttpStatus.CREATED)
                        .body(new BaseRx().bodyCallback(true, "Nama playlist sudah ada"));
            } 
            if (map.get("title").equals("Liked Songs")) {
                return ResponseEntity
                        .status(HttpStatus.CREATED)
                        .body(new BaseRx().bodyCallback(true, "Nama playlist tidak dapat di gunakan"));
                        
            } 
            if (map.get("title").equals("")) {
                return ResponseEntity
                        .status(HttpStatus.CREATED)
                        .body(new BaseRx().bodyCallback(true, "Nama playlist tidak boleh kosong "));
            }
            else {
                playlistRepository.save(getPlaylistData(id_user, map));
                return ResponseEntity
                        .status(HttpStatus.CREATED)
                        .body(new BaseRx().bodyCallback(true, "Playlist telah di tambahkan"));           //ADD PLAYLIST
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }

    @PostMapping(path = "/edit/playlist/{id}")
    public ResponseEntity<?> editPlaylistImage(@PathVariable(value = "id") int id, @RequestParam String image) {
        try {
            PlaylistData playlistData = playlistRepository.findById(id);
            if(!playlistData.title.equals("Liked Songs")) {
                PlaylistData response = playlistRepository.save(updatePlaylistImage(playlistData, image));
                return ResponseEntity
                        .status(HttpStatus.CREATED)
                        .body(new BaseDataRx<PlaylistData>().bodyDataCallback(true, response, "Playlist telah di update"));                         //EDIT PLAYLIST IMAGE
            } else {
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .body(new BaseRx().bodyCallback(true, "Liked Song Tidak Dapat Di Edit"));
            }

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseDataRx<PlaylistData>().bodyCallback(false, e.toString()));
        }
    }


    @DeleteMapping("/playlist/delete/{id}")
    public ResponseEntity<?> deletePlaylist(@PathVariable(value = "id") Integer id) {
        try {
            PlaylistData playlistData = playlistRepository.findById(id);
            if(!playlistData.title.equals("Liked Songs")) {
                playlistRepository.deleteById(id);
                playlistSongRepository.deleteAllSongByIdPlaylist(id);
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .body(new BaseRx().bodyCallback(true, playlistData.title));                         //DELETE PLAYLIST
            }
            else {
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .body(new BaseRx().bodyCallback(true, "Liked Song Tidak Dapat Di Hapus"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }

    @DeleteMapping("/playlist/{id_playlist}/delete/song/{id_song}")
    public ResponseEntity<?> deleteSongInPlaylist(
            @PathVariable(value = "id_playlist") Integer id_playlist,
            @PathVariable(value = "id_song") Integer id_song
    ) {
        try {
            playlistSongRepository.deleteSongById(id_playlist, id_song);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(new BaseRx().bodyCallback(true, "Lagu telah di hapus dari playlist"));                         //DELETE SONG IN PLAYLIST
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseRx().bodyCallback(false, e.toString()));
        }

    }


    @PostMapping(path = "/add/song/playlist")
    public ResponseEntity<?> addSongPlaylist(@RequestParam HashMap<String, String> map) {
        try {
            PlaylistSongData playlistSongData = playlistSongRepository.findSongPlaylist(
                    Integer.valueOf(map.get("id_playlist")),
                    Integer.valueOf(map.get("id_song"))
            );
            if (playlistSongData == null) {
                playlistSongRepository.save(getPlaylistSongData(map));
                return ResponseEntity
                        .status(HttpStatus.CREATED)
                        .body(new BaseRx().bodyCallback(true, "Song telah di tambahkan ke playlist"));      //ADD PLAYLIST SONG
            } else {
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .body(new BaseRx().bodyCallback(true, "Song sudah tersedia di dalam playlist"));
            }

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }

    @PostMapping(path = "/add/song/{id_user}/liked/{id_song}")
    public ResponseEntity<?> addSongLiked(@PathVariable(value = "id_user") Integer idUser, @PathVariable(value = "id_song") Integer idSong) {
        try {
            PlaylistData playlistData = playlistRepository.findLikedPlaylist(idUser);
            if (playlistData != null) {
                return saveLikedSongToPlaylist(playlistData.id, idSong);
            } else {
                PlaylistData data = initLikedPlaylist(idUser);
                return saveLikedSongToPlaylist(data.id, idSong);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }

    @DeleteMapping("/delete/song/{id_user}/liked/{id_song}")
    public ResponseEntity<?> unlikeSong(
            @PathVariable(value = "id_user") Integer idUser,
            @PathVariable(value = "id_song") Integer idSong
    ) {
        try {
            PlaylistData playlistData = playlistRepository.findLikedPlaylist(idUser);
            playlistSongRepository.deleteSongById(playlistData.id, idSong);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(new BaseRx().bodyCallback(true, "Lagu telah di unlike"));                         //DELETE SONG IN PLAYLIST
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseRx().bodyCallback(false, e.toString()));
        }

    }

    @GetMapping(path = "/playlist/{id}/song")
    public ResponseEntity<?> songByPlaylist(@PathVariable("id") int id) {
        try {
            List<PlaylistSongData> data = playlistSongRepository.findByIdPlaylist(id);
            if (data.size() != 0) {
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .body(new BaseDataRx<PlaylistSongData>().bodyListCallback(true, data, "true"));                       //GET SONG BY PLAYLIST
            } else {
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .body(new BaseDataRx<PlaylistSongData>().bodyListCallback(false, data, "Add your song"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }


    @GetMapping(path = "/playlist")
    public ResponseEntity<?> getPlaylist(@RequestParam(required = false) Integer userId) {
        try {
            List<PlaylistData> data = playlistRepository.findByIdUser(userId);
            if (data.size() != 0) {
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .body(new BaseDataRx<PlaylistData>().bodyListCallback(true, data, "true"));                       //GET PLAYLIST
            } else {
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .body(new BaseDataRx<PlaylistData>().bodyListCallback(false, data, "No Data Available"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }

    private PlaylistData getPlaylistData(String id_user, HashMap<String, String> map) {
        PlaylistData data = new PlaylistData();
        data.id_user = Integer.parseInt(id_user);
        data.title = map.get("title");
        if (map.get("image") == null) {
            data.image = "https://cdn.dribbble.com/users/29051/screenshots/2515769/icon.png";
        } else {
            data.image = map.get("image");
        }
        return data;
    }

    private PlaylistSongData getPlaylistSongData(HashMap<String, String> map) {
        PlaylistSongData data = new PlaylistSongData();
        data.id_playlist = Integer.parseInt(map.get("id_playlist"));
        data.id_song = Integer.parseInt(map.get("id_song"));
        return data;
    }

    private PlaylistData updatePlaylistImage(PlaylistData playlistData, String image) {
        playlistData.image = image;
        return playlistRepository.save(playlistData);
    }

    private PlaylistData initLikedPlaylist(int id) {
        PlaylistData data = new PlaylistData();
        data.id_user = id;
        data.title = "Liked Songs";
        data.image = "https://cmkt-image-prd.global.ssl.fastly.net/0.1.0/ps/2396424/580/386/m1/fpnw/wm0/creative-12-.png?1489325831&s=0f9f4e19bc38c347d967a537bc28dd48";
        return playlistRepository.save(data);
    }

    private ResponseEntity<?> saveLikedSongToPlaylist(Integer idPlaylist, Integer idSong) {
        PlaylistSongData data = new PlaylistSongData();
        data.id_playlist = idPlaylist;
        data.id_song = idSong;
        PlaylistSongData playlistSongData = playlistSongRepository.findSongPlaylist(idPlaylist, idSong);
        if (playlistSongData == null) {
            playlistSongRepository.save(data);
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(new BaseRx().bodyCallback(true, "Song telah di liked"));      //ADD PLAYLIST SONG
        } else {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(new BaseRx().bodyCallback(true, "Song sudah tersedia di dalam liked playlist"));
        }
    }
}