package com.example.loop.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = "http://localhost:8080")
@Controller
public class DashboardController {

    @RequestMapping("/")
    public String index() {
        return "dashboard/index";
    }
    
    
    @GetMapping(path = "/login")
    public ModelAndView login() {
       ModelAndView modelAndView = new ModelAndView();
       modelAndView.setViewName("login");
       return modelAndView;
    }


}
