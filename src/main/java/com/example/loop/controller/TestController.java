package com.example.loop.controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



@RestController
@RequestMapping("/algorithm")
public class TestController {

    
	@GetMapping(path = "/em")
    public ResponseEntity<?> allSong() {
        try {
            // EM em = new EM();
            // double[][] array = {{8,11,9},{2,2,3}};
            // em.bulidCluster(array, 1);
            return ResponseEntity
                .status(HttpStatus.OK)                 
                .body("em.getWeight()");
        }
        catch(Exception e) {
            e.printStackTrace();
            return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(e.toString());
        }
    }

}
