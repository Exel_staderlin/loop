package com.example.loop.controller;

import com.example.loop.model.ArtistData;
import com.example.loop.model.EqualizerData;
import com.example.loop.model.GenreData;
import com.example.loop.repository.ArtistRepository;
import com.example.loop.repository.EqualizerRepository;
import com.example.loop.repository.GenreRepository;
import com.example.loop.repository.SongRepository;
import com.example.loop.utilities.BaseDataRx;
import com.example.loop.utilities.BaseRx;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Random;

/**
 * Created by Exel staderlin on 01/28/2020.
 */

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api")
public class EqualizerController {

    @Autowired
    private EqualizerRepository equalizerRepository;

    @PostMapping(path = "/add/equalizer/{id_user}")
    public ResponseEntity<?> addEqualizer(@PathVariable(value = "id_user") Integer id_user, @RequestParam(value = "frequency") String frequency) {
        try {
            EqualizerData equalizerData = equalizerRepository.findByIdUser(id_user);
            if (equalizerData != null) {
                equalizerRepository.save(updateEqualizerData(equalizerData, frequency));
                return ResponseEntity
                        .status(HttpStatus.CREATED)
                        .body(new BaseRx().bodyCallback(true, "Equalizer telah di update"));           //Update Equalizer
            } else {
                equalizerRepository.save(getEqualizerData(id_user, frequency));
                return ResponseEntity
                        .status(HttpStatus.CREATED)
                        .body(new BaseRx().bodyCallback(true, "Equalizer telah di tambahkan"));           //ADD Equalizer
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }

    @GetMapping(path = "/equalizer")
    public ResponseEntity<?> getEqualizer(@RequestParam(required = false) Integer userId) {
        try {
            EqualizerData equalizerData = equalizerRepository.findByIdUser(userId);
            if (equalizerData != null) {
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .body(new BaseDataRx<EqualizerData>().bodyDataCallback(true, equalizerData, "true"));                       //GET PLAYLIST
            } else {
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .body(new BaseDataRx<EqualizerData>().bodyDataCallback(false, equalizerData, "No Data Available"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }

    private EqualizerData getEqualizerData(Integer id_user, String frequency) {
        EqualizerData data = new EqualizerData();
        data.id_user = id_user;
        data.frequency = frequency;
        return data;
    }

    private EqualizerData updateEqualizerData(EqualizerData equalizerData, String frequency) {
        equalizerData.frequency = frequency;
        return equalizerData;
    }
}