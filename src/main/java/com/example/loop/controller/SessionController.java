//package com.example.loop.controller;
//
//import com.example.loop.model.PrediksiLaguData;
//import com.example.loop.model.SessionData;
//import com.example.loop.model.SessionLogData;
//import com.example.loop.model.SongData;
//import com.example.loop.model.TemporalDiversityData;
//import com.example.loop.repository.SessionLogRepository;
//import com.example.loop.utilities.BaseDataRx;
//import com.example.loop.utilities.BaseRx;
//import com.example.loop.utilities.EmAlgoritma;
//import com.univocity.parsers.common.processor.RowListProcessor;
//import com.univocity.parsers.csv.CsvParser;
//import com.univocity.parsers.csv.CsvParserSettings;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.CrossOrigin;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.io.File;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Calendar;
//import java.util.Collections;
//import java.util.Date;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Set;
//import java.util.concurrent.TimeUnit;
//
//@CrossOrigin(origins = "http://localhost:8080")
//@RestController
//@RequestMapping("/api")
//public class SessionController {
//
//   @Autowired
//   private SessionLogRepository sessionLogRepository;
//
//
//   @GetMapping(path = "/generate_session")
//   public ResponseEntity<?> testGenerateSession(@RequestParam(required = false) Integer userId) {
//       try {
//           List<SessionLogData> response = sessionLogRepository.findAlls();
//           List<SessionData> generateSession = generateSession(response, 30);
//           List<TemporalDiversityData> temporalDiversityData = makeTemporalDiversity(generateSession);
//           return ResponseEntity.status(HttpStatus.CREATED)
//                   .body(new BaseDataRx<TemporalDiversityData>().bodyListCallback(true, temporalDiversityData, "true"));
//
//       } catch (Exception e) {
//           e.printStackTrace();
//           return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
//                   .body(new BaseRx().bodyCallback(true, e.toString()));
//       }
//   }
//
//   @GetMapping(path = "/recomendation")
//   public ResponseEntity<?> calculateRecommendation(@RequestParam(required = false) Integer userId,
//                                                 @RequestParam(required = false) Integer threshold,
//                                                 @RequestParam(required = false) Integer topK,
//                                                 @RequestParam(required = false) Integer limitList,
//                                                 @RequestParam(required = false) Integer digitLimit ) {
//       try {
//           if (threshold == null) {
//               threshold = 30;
//           }
//           if (topK == null) {
//               topK = 100;
//           }
//           if (limitList == null) {
//               limitList = 10;
//           }
//           if (digitLimit == null) {
//               digitLimit = 4;
//           }
//
//           List<SessionLogData> userLog = sessionLogRepository.findByIdUser(userId);
//           if (userLog.size() == 0) {
//               return ResponseEntity.status(HttpStatus.OK)
//                       .body(new BaseRx().bodyCallback(false, "User belum ada log"));
//           }
//           List<SessionLogData> semuaLog = sessionLogRepository.findAlls();
//
//
//           List<SessionData> semuaSession = generateSession(semuaLog, threshold);
//           List<SessionData> userSession = generateSession(userLog, threshold);
//           System.out.println("semuaSession " + "=" + semuaSession.size());
//           System.out.println("userSession " + "=" + userSession.size());
//           List<TemporalDiversityData> temporalDiversityData = makeTemporalDiversity(semuaSession);
//           System.out.println("temporalDiversityData " + "=" + temporalDiversityData.size());
//           SessionData activeUserSession = userSession.get(userSession.size() - 1);
//           List<PrediksiLaguData> prediksiLagu = EmAlgoritma.build(digitLimit, topK, temporalDiversityData, filteredSessionData(activeUserSession));
//           return ResponseEntity.status(HttpStatus.OK)
//                   .body(new BaseDataRx<PrediksiLaguData>().bodyListCallback(true, limitHasilPrediksi(prediksiLagu, limitList), "true"));
//
//           // ----------------------------------------------TESTING UNTUK SCATTERPOT------------------------------------------------------------------------------
////            List<Integer> id = new ArrayList<>();
////            List<Integer>  session =new ArrayList<>();
////            for(int i =5; i<13; i++) {
////                List<SessionLogData> userLog = sessionLogRepository.findByIdUser(i);
////                if (userLog.size() == 0) {
////                    return ResponseEntity.status(HttpStatus.OK)
////                            .body(new BaseRx().bodyCallback(false, "User belum ada log"));
////                }
//////                List<SessionData> userSession = generateSession(userLog, threshold);
////                Set<SessionLogData> uniqueSessionLogData = new HashSet<>(userLog);
////                id.add(i);
////                session.add(uniqueSessionLogData.size());
////
////            }
////            for(int i =0; i<id.size(); i++) {
////                System.out.println(id.get(i) + "," + session.get(i));
////            }
////
////            return ResponseEntity.status(HttpStatus.CREATED)
////                    .body(new BaseRx().bodyCallback(true,"true"));
//
//       } catch (Exception e) {
//           e.printStackTrace();
//           return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
//                   .body(new BaseRx().bodyCallback(false, e.toString()));
//       }
//   }
//
//   private static List<SessionLogData> filteredSessionData(SessionData sessionData) {
//       List<SessionLogData> listSessionLogFiltered = new ArrayList<>();
//       List<SessionLogData> listSessionLog = sessionData.sessionLogData;
//       Set<SessionLogData> set = new HashSet<>(listSessionLog);
//       for (SessionLogData sessionLogData : set) {
////            SessionLogAlgoData datas = new SessionLogData();
////            SongAlgoData datas = new SongAlgoData();
//           SessionLogData datas = new SessionLogData();
//
//           datas.id = sessionLogData.id;
//           datas.id_user = sessionLogData.id_user;
//           datas.created_date = sessionLogData.created_date;
//           SongData songdata = sessionLogData.songs;
//           songdata.playcount = Collections.frequency(listSessionLog, sessionLogData);       // ini code untuk metode get dari database
//           datas.songs = songdata;
//
//           listSessionLogFiltered.add(datas);
//       }
//       return listSessionLogFiltered;
//   }
//
//   private List<PrediksiLaguData> limitHasilPrediksi(List<PrediksiLaguData> prediksiLagu, int limitAngka) {
//       List<PrediksiLaguData> limitPrediksi = new ArrayList<>();
//       for (int i = 0; i < prediksiLagu.size(); i++) {
//           if (i == limitAngka) {
//               break;
//           }
//           PrediksiLaguData rate = new PrediksiLaguData();
//           rate.ratingLagu = prediksiLagu.get(i).ratingLagu;
//           rate.songData = prediksiLagu.get(i).songData;
//           limitPrediksi.add(rate);
//       }
//       return limitPrediksi;
//   }
//
//   private List<SessionData> generateSession(List<SessionLogData> response, Integer threshold) {
//       List<SessionData> allSession = new ArrayList<>();
//       List<SessionLogData> listLog = new ArrayList<>();
//       SessionData sessionData = new SessionData();
//       Date lastDate = null;
//       int sessionIdUser = 1;
//       for (SessionLogData log : response) {
//           Date currentDate = log.created_date;
//           if (lastDate != null && lastDate.before(currentDate)) {
//               long dif = getDateDiffMinutes(lastDate, currentDate, TimeUnit.MINUTES);
////                System.out.println("difference time " + "=" + dif);
//               if (dif >= threshold) {
//                   sessionIdUser++;
//                   sessionData.sessionLogData = listLog;
//                   allSession.add(sessionData);
//
//                   sessionData = new SessionData(); //Create New Session
//                   listLog = new ArrayList<>();
//                   sessionData.id = sessionIdUser;
//                   sessionData.created_date = currentDate;
////                    System.out.println("Buat session baru ==" + sessionIdUser);//"Di Session Berikutnya";
//               }
//               listLog.add(log);
//               lastDate = currentDate;
//           } else {
//               sessionData.id = sessionIdUser;
//               sessionData.created_date = currentDate;
//               listLog.add(log);
//               lastDate = currentDate;
//           }
//       }
////        System.out.println("Total Session " + "=" + sessionIdUser);
//       return allSession;
//   }
//
//   private List<TemporalDiversityData> makeTemporalDiversity(List<SessionData> session) {
//       Calendar cal = Calendar.getInstance();
//       List<TemporalDiversityData> temporalList = new ArrayList<>();
//
//       for (int i = 0; i < session.size(); i++) {
//           List<SessionLogData> sessionLog = session.get(i).sessionLogData;
//           List<SongData> listSongData = new ArrayList<>();
//           if (sessionLog.size() >= 2) { // session yg isi lagunya kurang dari 2 gk akan di ikut di hitung
//               TemporalDiversityData temporalDiversityData = new TemporalDiversityData();
//               Date date = session.get(i).created_date;
//               cal.setTime(date);
//               Set<SessionLogData> uniqueLog = new HashSet<>(sessionLog);
//
//               temporalDiversityData.id = session.get(i).id;
//               temporalDiversityData.hour = cal.get(Calendar.HOUR_OF_DAY);
//               temporalDiversityData.weekday = cal.get(Calendar.WEEK_OF_MONTH);
//               temporalDiversityData.dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
//               temporalDiversityData.month = cal.get(Calendar.MONTH) + 1;
//               temporalDiversityData.diversity = (float) limitDigit((float) uniqueLog.size() / (float) sessionLog.size());
//
//               Set<SessionLogData> set = new HashSet<>(sessionLog);
//               for (SessionLogData sessionLogData : set) {
//                   SongData songData = sessionLogData.songs;
//                   if(songData != null) {
//                       songData.playcount = Collections.frequency(sessionLog, sessionLogData);
//                       listSongData.add(songData);
//                   }
//               }
//               temporalDiversityData.songData = listSongData;
//               temporalList.add(temporalDiversityData);
//           }
//       }
//       return temporalList;
//   }
//
//   private static double limitDigit(double value) {
//       return Math.round(value * Math.pow(10, 3)) / Math.pow(10, 3);
//   }
//
//   private static ArrayList<List<String>> readCsvToList(String csvFileLocation) {
//       ArrayList<List<String>> records = new ArrayList<>();
//       CsvParser parser = new CsvParser(settingsCsvParser());
//       parser.beginParsing(new File(csvFileLocation));
//       try {
//           String[] row;
//           int loop = 1;
//           int threshold = 5000000;
//           while ((row = parser.parseNext()) != null) {
//               records.add(Arrays.asList(row));
//               if (loop >= threshold) {
//                   System.out.println("load record :" + loop);
//               }
//               loop++;
//           }
//           System.out.println("record size :" + records.size());
//           return records;
//       } catch (Exception e) {
//           e.printStackTrace();
//           return new ArrayList<>();
//       }
//   }
//
//   private static CsvParserSettings settingsCsvParser() {
//       CsvParserSettings settings = new CsvParserSettings(); // you'll find many options here, check the tutorial.
//       RowListProcessor rowProcessor = new RowListProcessor();
//       settings.setNullValue("<NULL>");
//       settings.setEmptyValue("<EMPTY>");
//       settings.setIgnoreLeadingWhitespaces(false);
//       settings.setIgnoreTrailingWhitespaces(false);
//       settings.setNumberOfRecordsToRead(10000000);
//       settings.setSkipEmptyLines(false);
//       settings.setMaxCharsPerColumn(10000000);
//       settings.setInputBufferSize(1000);
//       settings.setReadInputOnSeparateThread(false);
//       settings.setLineSeparatorDetectionEnabled(true);
//       settings.setRowProcessor(rowProcessor);
//       settings.setHeaderExtractionEnabled(true);
//       return settings;
//   }
//
////    private SessionData getSessionData(int idUser, int idSessionUser) {
////        SessionData data = new SessionData();
////        data.id_user = idUser;
////        data.id_session_user = idSessionUser;
////        return data;
////    }
//
//   // private Date convertToDate(String strDate) {
//   //     DateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm");
//   //     try {
//   //         return format.parse(strDate);
//   //     } catch (ParseException e) {
//   //         e.printStackTrace();
//   //         return null;
//   //     }
//   // }
//
//   private long getDateDiffMinutes(Date lastDate, Date currentDate, TimeUnit timeUnit) {
//       long diffTime = currentDate.getTime() - lastDate.getTime();
//       long diffMinutes = diffTime / (60 * 1000);
//       return timeUnit.convert(diffMinutes, TimeUnit.MINUTES);
//   }
//
//}