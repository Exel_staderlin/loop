package com.example.loop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletRequest;

import com.example.loop.model.UserData;
import com.example.loop.repository.UserRepository;
import com.example.loop.utilities.BaseDataRx;
import com.example.loop.utilities.BaseRx;
import com.example.loop.utilities.Utility;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api")
public class EmailController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;



    @PostMapping(path = "/forget_password")
    public ResponseEntity<?> forgetPassword(@RequestParam(value = "email") String email, HttpServletRequest request) throws AddressException, MessagingException, UnknownHostException {
        UserData userData = userRepository.findByEmail(email); // find user by id
        if(userData != null) {
            String localAddr = InetAddress.getLocalHost().getHostAddress();
            String localPort = String.valueOf(request.getLocalPort());
            String token = Utility.encryptString(String.valueOf(userData.id)); //enkripsi id kita jadi token
            String tokenEncode = ""; //encode agar simbol dapat di baca as string
            try {
                tokenEncode = URLEncoder.encode(token, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            String linkForgetPass = "http://"+localAddr+":"+localPort+"/api/reset_password?token="+tokenEncode;
            sendmail("LOOP MUSIC. Confirmation for reset password\"","To complete the password reset process, please click here: " + linkForgetPass , email);
            return ResponseEntity.status(HttpStatus.OK).body(new BaseRx().bodyCallback(true, "Message has been sent!!"));
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(new BaseRx().bodyCallback(true, "Email anda belum terdaftar!!"));
        }
       
    }

    @GetMapping(path = "/reset_password")
    public ResponseEntity<?> confirmResetPassword(@RequestParam(value = "token") String token) throws AddressException, MessagingException {
        String id = Utility.decryptString(token); // dekripsi token tadi menjadi id
        UserData userData = userRepository.findById(Integer.parseInt(id)); // find user by id
        String newPassword  = Utility.randomPasswordString(); // generate new password random
        userData.password = passwordEncoder.encode(newPassword);
        sendmail("LOOP MUSIC. Reset Password Successful","Congratulation reset password is completed, your new password is "  + newPassword, userData.email);
        userRepository.save(userData); // save to db
        return ResponseEntity.status(HttpStatus.OK).body(new BaseRx().bodyCallback(true, "New password sent to your email!"));
    }

    @PostMapping(path = "/find_email")
    public ResponseEntity<?> findEmail(@RequestParam(value = "email") String email) {
        UserData userData = userRepository.findByEmail(email); // find user by email
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(new BaseDataRx<UserData>().bodyDataCallback(false, userData, "oke"));
    }

    private void sendmail(String title,String message, String email) throws AddressException, MessagingException {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("exelstaderlin@gmail.com", "difukxkndueffwdn"); // email pengirim
            }
        });
        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress("exelstaderlin@gmail.com", false));
        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email)); // email penerima
        msg.setSubject(title);
        msg.setContent(message, "text/html");
        msg.setSentDate(new Date());

        MimeBodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(message, "text/html");

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);
//        MimeBodyPart attachPart = new MimeBodyPart();
//
//        attachPart.attachFile("/var/tmp/image19.png");
//        multipart.addBodyPart(attachPart);
        msg.setContent(multipart);
        Transport.send(msg);
    }
    //as

}
