package com.example.loop.controller;

import java.util.HashMap;
import java.util.List;

import com.example.loop.model.ArtistData;
import com.example.loop.model.PlaylistData;
import com.example.loop.model.SearchLogData;
import com.example.loop.model.SongData;
import com.example.loop.model.UserData;
import com.example.loop.repository.ArtistRepository;
import com.example.loop.repository.PlaylistRepository;
import com.example.loop.repository.SearchRepository;
import com.example.loop.repository.SongRepository;
import com.example.loop.repository.UserRepository;
import com.example.loop.utilities.BaseDataRx;
import com.example.loop.utilities.BaseRx;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api")
public class SearchController {

    @Autowired
    private SearchRepository searchRepository;

    @Autowired
    private SongRepository songRepository;

    @Autowired
    private ArtistRepository artistRepository;

    @Autowired
    private PlaylistRepository playlistRepository;

    @Autowired
    private UserRepository userRepository;

    @CrossOrigin(origins = "http://localhost:8080")
    @PostMapping(path = "/add/search_log")
    public ResponseEntity<?> addSearchLog(@RequestParam HashMap<String, String> map) {
        try {
            SearchLogData data = new SearchLogData();
            data.id_user = Integer.parseInt(map.get("id_user"));
            if(map.get("id_song") != null) {
                data.id_song = Integer.parseInt(map.get("id_song"));
            }
            if(map.get("id_playlist") != null) {
                data.id_playlist = Integer.parseInt(map.get("id_playlist"));
            }
            if(map.get("id_artist") != null) {
                data.id_artist = Integer.parseInt(map.get("id_artist"));
            }
            searchRepository.save(data);
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(new BaseRx().bodyCallback(true, "Search log added"));                    //ADD LOG
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }

    
    @GetMapping(path = "/search_log/{id_user}")
    public ResponseEntity<?> getRecentlySearchByUser(@PathVariable("id_user") int id) {
        try {
            List<SearchLogData> response = searchRepository.findByIdUser(id);
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(new BaseDataRx<SearchLogData>().bodyListCallback(true, response, "true"));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }

    @GetMapping(path = "/user/search")
    public ResponseEntity<?> searchUser(@RequestParam(required = false) String name) {
        try {
            List<UserData> data = userRepository.findByNameIgnoreCaseContaining(name);
            if(data.size() != 0) {
                return ResponseEntity
                .status(HttpStatus.OK)                 
                .body(new BaseDataRx<UserData>().bodyListCallback(true, data, "true"));
            }
            else {
                return ResponseEntity
                .status(HttpStatus.OK)                 
                .body(new BaseDataRx<UserData>().bodyListCallback(false, data, "No Data Available"));
            } 
        }
        catch(Exception e) {
            e.printStackTrace();
            return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }

    @GetMapping(path = "/song/search")
    public ResponseEntity<?> searchSong(@RequestParam(required = false) String title) {
        try {
            List<SongData> data = songRepository.findByTitleIgnoreCaseContaining(title);
            if(data.size() != 0) {
                return ResponseEntity
                .status(HttpStatus.OK)                 
                .body(new BaseDataRx<SongData>().bodyListCallback(true, data, "true"));
            }
            else {
                return ResponseEntity
                .status(HttpStatus.OK)                 
                .body(new BaseDataRx<SongData>().bodyListCallback(false, data, "No Data Available"));
            } 
        }
        catch(Exception e) {
            e.printStackTrace();
            return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }

    @GetMapping(path = "/artist/search")
    public ResponseEntity<?> searchArtist(@RequestParam(required = false) String title) {
        try {
            List<ArtistData> data = artistRepository.findByNameIgnoreCaseContaining(title);
            if(data.size() != 0) {
                return ResponseEntity
                .status(HttpStatus.OK)                 
                .body(new BaseDataRx<ArtistData>().bodyListCallback(true, data, "true"));
            }
            else {
                return ResponseEntity
                .status(HttpStatus.OK)                 
                .body(new BaseDataRx<ArtistData>().bodyListCallback(false, data, "No Data Available"));
            } 
        }
        catch(Exception e) {
            e.printStackTrace();
            return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }


    @GetMapping(path = "/playlist/{id_user}/search")
    public ResponseEntity<?> searchPlaylist(@PathVariable("id_user") Integer userId,@RequestParam(required = false) String title) {
        try {
            List<PlaylistData> data = playlistRepository.findByUserAndTitle(userId,title);
            if(data.size() != 0) {
                return ResponseEntity
                .status(HttpStatus.OK)                 
                .body(new BaseDataRx<PlaylistData>().bodyListCallback(true, data, "true"));
            }
            else {
                return ResponseEntity
                .status(HttpStatus.OK)                 
                .body(new BaseDataRx<PlaylistData>().bodyListCallback(false, data, "No Data Available"));
            } 
        }
        catch(Exception e) {
            e.printStackTrace();
            return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new BaseRx().bodyCallback(false, e.toString()));
        }
    }
}