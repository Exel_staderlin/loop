package com.example.loop.repository;

import java.util.List;
import com.example.loop.model.SearchLogData;
import com.example.loop.model.SessionLogData;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SearchRepository extends JpaRepository<SearchLogData, Long> {

    @Query(value="SELECT * FROM search  WHERE id IN (SELECT MAX(id) AS id FROM search WHERE id_user= :id_user GROUP BY id_song,id_artist,id_playlist) ORDER BY id DESC LIMIT 10;", nativeQuery = true)
    List<SearchLogData> findByIdUser(@Param("id_user") int id_user);
    
}