package com.example.loop.repository;

import com.example.loop.model.PlaylistData;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface PlaylistRepository extends JpaRepository<PlaylistData, Long> {

    PlaylistData findById(int id);
    
    @Query(value="SELECT * FROM playlist WHERE id_user = :id_user", nativeQuery = true)
    List<PlaylistData> findByIdUser(@Param("id_user") int id_user);

    @Query(value="SELECT * FROM `playlist` WHERE id_user = :id_user AND title = :title", nativeQuery = true)
    PlaylistData findPlaylistTitle(@Param("id_user") int id_user, @Param("title") String title);

    @Transactional
    @Modifying
    @Query(value="DELETE FROM playlist WHERE id = :id", nativeQuery = true)
    void deleteById(@Param("id") int id);

    @Query(value="SELECT * FROM playlist WHERE id_user= :id_user AND title LIKE %:title% LIMIT 20", nativeQuery = true)
    List<PlaylistData> findByUserAndTitle(@Param("id_user") Integer id_user,@Param("title") String title);

    @Query(value="SELECT * FROM playlist WHERE id_user = :id_user AND title = 'Liked Songs'", nativeQuery = true)
    PlaylistData findLikedPlaylist(@Param("id_user") int id_user);

}