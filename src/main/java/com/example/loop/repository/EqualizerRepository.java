package com.example.loop.repository;

import com.example.loop.model.EqualizerData;
import com.example.loop.model.GenreData;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

/**
 * Created by Exel staderlin on 9/30/2019.
 */
public interface EqualizerRepository extends JpaRepository<EqualizerData, Long> {

    EqualizerData findById(int id);

    @Query(value="SELECT * FROM equalizer WHERE id_user = :id_user", nativeQuery = true)
    EqualizerData findByIdUser(@Param("id_user") int id_user);

    @Transactional
    @Modifying
    @Query(value="DELETE FROM equalizer WHERE id = :id", nativeQuery = true)
    void deleteById(@Param("id") int id);
}
