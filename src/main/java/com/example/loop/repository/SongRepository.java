package com.example.loop.repository;

import com.example.loop.model.SongData;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public interface SongRepository extends JpaRepository<SongData, Long>, JpaSpecificationExecutor<SongData> {

    @Query(value = "SELECT * FROM song LIMIT 100", nativeQuery = true)
    List<SongData> findTopDatas();

    @Query(value = "SELECT * FROM song ORDER BY RAND() LIMIT 50", nativeQuery = true)
    List<SongData> findRandom();

    @Query(value = "SELECT * FROM song WHERE id_artist = :id_artist", nativeQuery = true)
    List<SongData> findByIdArtist(@Param("id_artist") int id_artist);

    @Query(value = "SELECT * FROM song WHERE id_genre = :id_genre", nativeQuery = true)
    List<SongData> findByIdGenre(@Param("id_genre") int id_genre);

    @Query(value="SELECT * FROM song WHERE title LIKE %:title% LIMIT 50", nativeQuery = true)
    List<SongData> findByTitleIgnoreCaseContaining(@Param("title") String title);

    SongData findById(int id);

    @Transactional
    @Modifying
    @Query(value = "UPDATE song SET status = :status WHERE id_artist = :id_artist", nativeQuery = true)
    void blockByIdArtist(@Param("status") int status, @Param("id_artist") int id_artist);

    @Transactional
    @Modifying
    @Query(value = "UPDATE song SET id_genre = :id_genre WHERE id_artist = :id_artist", nativeQuery = true)
    void randomGenreOnSong(@Param("id_genre") int id_genre, @Param("id_artist") int id_artist);

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM song WHERE id = :id", nativeQuery = true)
    void deleteById(@Param("id") int id);

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM song WHERE id_artist = :id_artist", nativeQuery = true)
    void deleteByIdArtist(@Param("id_artist") int id);

}