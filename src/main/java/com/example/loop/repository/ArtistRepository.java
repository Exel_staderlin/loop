package com.example.loop.repository;

import com.example.loop.model.ArtistData;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ArtistRepository extends JpaRepository<ArtistData, Long> {

    @Query(value = "SELECT * FROM artist WHERE name LIKE %:name% LIMIT 20", nativeQuery = true)
    List<ArtistData> findByNameIgnoreCaseContaining(@Param("name") String name);

    ArtistData findById(int id);

    ArtistData findByName(String name);

    @Query(value = "SELECT * FROM artist LIMIT 100", nativeQuery = true)
    List<ArtistData> findAlls();

    @Query(value = "SELECT * FROM artist LIMIT 5000", nativeQuery = true)
    List<ArtistData> findArtistCustom();

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM artist WHERE id = :id", nativeQuery = true)
    void deleteById(@Param("id") int id);
}

