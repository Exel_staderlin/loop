package com.example.loop.repository;

import com.example.loop.model.GenreData;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

/**
 * Created by Exel staderlin on 9/30/2019.
 */
public interface GenreRepository extends JpaRepository<GenreData, Long> {

    GenreData findById(int id);

    GenreData findByName(String name);

    @Transactional
    @Modifying
    @Query(value="DELETE FROM genre WHERE id = :id", nativeQuery = true)
    void deleteById(@Param("id") int id);
}
