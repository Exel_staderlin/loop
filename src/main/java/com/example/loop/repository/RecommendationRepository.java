package com.example.loop.repository;

import java.util.List;

import com.example.loop.model.RecommendationData;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface RecommendationRepository extends JpaRepository<RecommendationData, Long> {

    @Query(value="SELECT * FROM recommendation WHERE id_user= :id_user", nativeQuery = true)
    List<RecommendationData> findByIdUser(@Param("id_user") int id_user);

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM recommendation WHERE id_user = :id_user", nativeQuery = true)
    void deleteById(@Param("id_user") int id_user);
    
}