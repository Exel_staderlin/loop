package com.example.loop.repository;

import com.example.loop.model.SessionLogData;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SessionLogRepository extends JpaRepository<SessionLogData, Long> {

    @Query(value="SELECT * FROM session_log WHERE id_song = :id_song", nativeQuery = true)
    List<SessionLogData> findByIdSong(@Param("id_song") int id_song);

    @Query(value="SELECT * FROM session_log WHERE id_session = :id_session", nativeQuery = true)
    List<SessionLogData> findByIdSession(@Param("id_session") int id_session);

    @Query(value="SELECT * FROM session_log WHERE id_session_user = :id_session_user", nativeQuery = true)
    List<SessionLogData> findByIdSessionUser(@Param("id_session_user") int id_session_user);

    @Query(value="SELECT *, COUNT(id_song) AS playcount FROM session_log group by id_song ORDER BY playcount DESC LIMIT 10", nativeQuery = true)
    List<Object> hitungByIdSong();

    @Query(value="SELECT * FROM session_log  WHERE id IN (SELECT MAX(id) AS id FROM session_log WHERE id_user= :id_user GROUP BY id_song) ORDER BY id DESC;", nativeQuery = true)
    List<SessionLogData> findLatestLog(@Param("id_user") int id_user);

    @Query(value="SELECT * FROM session_log WHERE id_user= :id_user", nativeQuery = true)
    List<SessionLogData> findByIdUser(@Param("id_user") int id_user);

    @Query(value="SELECT * FROM session_log ORDER BY created_date DESC LIMIT 100", nativeQuery = true)
    List<SessionLogData> findAlls();

    @Query(value="SELECT * FROM session_log WHERE id_user = :id_user ORDER BY id DESC LIMIT 1", nativeQuery = true)
    SessionLogData findLastItem(@Param("id_user") int id_user);

    @Query(value="SELECT * FROM session_log WHERE id_user = :id_user AND id_session = :id_session GROUP BY id_song", nativeQuery = true)
    List<SessionLogData> findLogByIdUserAndIdSession(@Param("id_user") int id_user, @Param("id_session") int id_session);

    @Query(value="SELECT * FROM session_log WHERE id_user = :id_user GROUP BY id_song", nativeQuery = true)
    List<SessionLogData> findLogByIdUser(@Param("id_user") int id_user);
}