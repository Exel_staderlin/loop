package com.example.loop.repository;

import java.util.List;

import com.example.loop.model.PlaylistSongData;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface PlaylistSongRepository extends JpaRepository<PlaylistSongData, Long> {

    @Query(value="SELECT * FROM playlist_song WHERE id_playlist = :id_playlist", nativeQuery = true)
    List<PlaylistSongData> findByIdPlaylist(@Param("id_playlist") int id_playlist);

    @Query(value="SELECT * FROM playlist_song WHERE id_playlist = :id_playlist AND id_song = :id_song", nativeQuery = true)
    PlaylistSongData findSongPlaylist(@Param("id_playlist") int id_playlist, @Param("id_song") int id_song);

    @Transactional
    @Modifying
    @Query(value="DELETE FROM playlist_song WHERE id_playlist = :id_playlist", nativeQuery = true)
    void deleteAllSongByIdPlaylist(@Param("id_playlist") int id_playlist);
    
    @Transactional
    @Modifying
    @Query(value="DELETE FROM playlist_song WHERE id_playlist = :id_playlist AND id_song = :id_song", nativeQuery = true)
    void deleteSongById(@Param("id_playlist") int id_playlist, @Param("id_song") int id_song);

    // PlaylistSongData findBy(String emailString);

}