package com.example.loop.repository;

import java.util.List;

import com.example.loop.model.UserData;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface UserRepository extends JpaRepository<UserData, Long> {

    UserData findByEmail(String emailString);
    
    UserData findById(int id);

    @Transactional
    @Modifying
    @Query(value="DELETE FROM user WHERE id = :id", nativeQuery = true)
    void deleteById(@Param("id") int id);

    List<UserData> findByNameIgnoreCaseContaining(String name);

}
