package com.example.loop.utilities;

import com.example.loop.model.SongData;
import com.example.loop.model.TemporalDiversityData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DummyData {

    public List<TemporalDiversityData> tempDataDummy() {
        List<TemporalDiversityData> temp = new ArrayList<TemporalDiversityData>();
        TemporalDiversityData s1 = new TemporalDiversityData();
        s1.id = 1;
        s1.hour = 8;
        s1.dayOfMonth = 2;
        s1.weekday = 3;
        s1.month = 5;
        s1.diversity = (float) 0.333;
        s1.songData = dummySongData().get(0);
        temp.add(s1);
        TemporalDiversityData s2 = new TemporalDiversityData();
        s2.id = 2;
        s2.hour = 11;
        s2.dayOfMonth = 2;
        s2.weekday = 2;
        s2.month = 6;
        s2.diversity = (float) 0.222;
        s2.songData = dummySongData().get(1);
        temp.add(s2);
        TemporalDiversityData s3 = new TemporalDiversityData();
        s3.id = 3;
        s3.hour = 9;
        s3.dayOfMonth = 3;
        s3.weekday = 4;
        s3.month = 7;
        s3.diversity = (float) 1;
        s3.songData = dummySongData().get(2);
        temp.add(s3);
        TemporalDiversityData s4 = new TemporalDiversityData();
        s4.id = 4;
        s4.hour = 11;
        s4.dayOfMonth = 3;
        s4.weekday = 2;
        s4.month = 8;
        s4.diversity = (float) 2;
        s4.songData = dummySongData().get(4);
        temp.add(s4);
        TemporalDiversityData s5 = new TemporalDiversityData();
        s5.id = 5;
        s5.hour = 18;
        s5.dayOfMonth = 4;
        s5.weekday = 3;
        s5.month = 9;
        s5.diversity = (float) 3;
        s5.songData = dummySongData().get(4);
        temp.add(s5);
        TemporalDiversityData s6 = new TemporalDiversityData();
        s6.id = 6;
        s6.hour = 20;
        s6.dayOfMonth = 4;
        s6.weekday = 3;
        s6.month = 10;
        s6.diversity = (float) 4;
        s6.songData = dummySongData().get(4);
        temp.add(s6);
        TemporalDiversityData s7 = new TemporalDiversityData();
        s7.id = 7;
        s7.hour = 1;
        s7.dayOfMonth = 5;
        s7.weekday = 9;
        s7.month = 11;
        s7.diversity = (float) 5;
        s7.songData = dummySongData().get(4);
        temp.add(s7);
        TemporalDiversityData s8 = new TemporalDiversityData();
        s8.id = 8;
        s8.hour = 7;
        s8.dayOfMonth = 5;
        s8.weekday = 3;
        s8.month = 12;
        s8.diversity = (float) 6;
        s8.songData = dummySongData().get(4);
        temp.add(s8);
        TemporalDiversityData s9 = new TemporalDiversityData();
        s9.id = 9;
        s9.hour = 9;
        s9.dayOfMonth = 7;
        s9.weekday = 3;
        s9.month = 12;
        s9.diversity = (float) 7;
        s9.songData = dummySongData().get(4);
        temp.add(s9);
        TemporalDiversityData s10 = new TemporalDiversityData();
        s10.id = 10;
        s10.hour = 8;
        s10.dayOfMonth = 1;
        s10.weekday = 6;
        s10.month = 12;
        s10.diversity = (float) 8;
        s10.songData = dummySongData().get(3);
        temp.add(s10);
        return temp;
    }

    public List<ArrayList<SongData>> dummySongData() {

        List<ArrayList<SongData>> listdummySongDatas = new ArrayList<>();
        ArrayList<SongData> s1 = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            SongData data = new SongData();
            List<Integer> playcount = new ArrayList<>(Arrays.asList(1, 1, 1, 1, 1, 1));
            List<String> title = new ArrayList<>(Arrays.asList("A", "B", "C", "D", "E", "F"));
            data.title = title.get(i);
            data.linkUrl = title.get(i);
            data.playcount = playcount.get(i);
            s1.add(data);
        }
        listdummySongDatas.add(s1);

        ArrayList<SongData> s2 = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            SongData data = new SongData();
            List<Integer> playcount = new ArrayList<>(Arrays.asList(0, 1, 1, 0, 2, 2));
            List<String> title = new ArrayList<>(Arrays.asList("A", "B", "C", "D", "E", "F"));
            data.title = title.get(i);
            data.linkUrl = title.get(i);
            data.playcount = playcount.get(i);
            s2.add(data);
        }
        listdummySongDatas.add(s2);

        ArrayList<SongData> s3 = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            SongData data = new SongData();
            List<Integer> playcount = new ArrayList<>(Arrays.asList(0, 1, 1, 0, 2, 0));
            List<String> title = new ArrayList<>(Arrays.asList("A", "B", "C", "D", "E", "F"));
            data.title = title.get(i);
            data.linkUrl = title.get(i);
            data.playcount = playcount.get(i);
            s3.add(data);
        }
        listdummySongDatas.add(s3);

        ArrayList<SongData> s10 = new ArrayList<SongData>();
        for (int i = 0; i < 6; i++) {
            SongData data = new SongData();
            List<Integer> playcount = new ArrayList<>(Arrays.asList(0, 1, 0, 0, 0, 2));
            List<String> title = new ArrayList<>(Arrays.asList("A", "B", "C", "D", "E", "F"));
            data.title = title.get(i);
            data.linkUrl = title.get(i);
            data.playcount = playcount.get(i);
            s10.add(data);
        }
        listdummySongDatas.add(s10);

        ArrayList<SongData> defaultData = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            SongData data = new SongData();
            List<Integer> playcount = new ArrayList<>(Arrays.asList(0, 0, 0, 0, 0, 0));
            List<String> title = new ArrayList<>(Arrays.asList("A", "B", "C", "D", "E", "F"));
            data.title = title.get(i);
            data.linkUrl = title.get(i);
            data.playcount = playcount.get(i);
            defaultData.add(data);
        }
        listdummySongDatas.add(defaultData);

        return listdummySongDatas;
    }

}