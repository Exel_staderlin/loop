/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.example.loop.utilities;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

/**
 * This Class Contains Theta Values which are first time random guess values
 * then the next iteration values these values are calculated based on EM 
 * algorithm
 * @author Naveen.Rokkam
 */
@XmlRootElement
@Getter
@Setter
public class EmParameterNew {
   
   public double weightBaru;

   public double meanTimeBaru;

   public double meanDomBaru;

   public double meanWeekDayBaru;

   public double meanMonthBaru;

   public double meanDiversityBaru;
   
   public double[][] varianceCovarianceBaru;
     
}
