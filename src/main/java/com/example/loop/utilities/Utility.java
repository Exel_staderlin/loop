package com.example.loop.utilities;

import org.apache.commons.lang3.RandomStringUtils;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Exel staderlin on 9/13/2019.
 */
public class Utility {

    public static String escapeSql(String str){
        String output = null;
        if (str != null && str.length() > 0) {
          str = str.replace("\\", "\\\\");
          str = str.replace("'", "\\'");
          str = str.replace("\0", "\\0");
          str = str.replace("\n", "\\n");
          str = str.replace("\r", "\\r");
          str = str.replace("\"", "\\\"");
          str = str.replace("\\x1a", "\\Z");
          str = str.replace(" ", "%20");
          output = str;
        }
      return output;
    }

    public static String encryptString(String message) {
        StandardPBEStringEncryptor standardPBEStringEncryptor = new StandardPBEStringEncryptor();
        standardPBEStringEncryptor.setPassword("jiet");
        return standardPBEStringEncryptor.encrypt(message);
    }

    public static String decryptString(String message) {
        StandardPBEStringEncryptor standardPBEStringEncryptor = new StandardPBEStringEncryptor();
        standardPBEStringEncryptor.setPassword("jiet");
        return standardPBEStringEncryptor.decrypt(message);
    }

    public static HashMap<Integer, Integer> countOccurrenceNumber(List<Integer> listAngka) {
        HashMap<Integer, Integer> intCounter = new HashMap<>();
        for (Integer angka : listAngka) {
            Integer valueWrapper = intCounter.get(angka);
            if (valueWrapper == null) {
                intCounter.put(angka, 1);
            } else {
                valueWrapper++;
                intCounter.put(angka, valueWrapper);
            }
        }

        return intCounter; // jumlah playcount
    }

    public static String randomPasswordString() {
        int length = 10;
        boolean useLetters = true;
        boolean useNumbers = true;
        String generatedString = RandomStringUtils.random(length, useLetters, useNumbers);
        return generatedString;
    }


    public static String formatZDateStr(String zDate) {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        Date date = null;
        try {
            date = inputFormat.parse(zDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String formattedDate = outputFormat.format(date);
//        System.out.println(formattedDate); // prints 10-04-2018
        return formattedDate;
    }

    public static Date formatZDate(String zDate) {
        Date date= null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").parse(zDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static void writeTextFile(String ...arg) {
//        List<DataSet> sortedUniqueName = new ArrayList<>(uniqueName);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("log_prediksi.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter printWriter = new PrintWriter(fileWriter);
        printWriter.println(arg[0]+". lagu = " + arg[1]);
        printWriter.println("rs = " + arg[2]);
        printWriter.println("total = " +arg[3] + " / " + arg[4]);
        printWriter.println("RAI = " + arg[5]);
        printWriter.println("----------------------------------------------");

        printWriter.close();
    }

    public static String getDurationMp3(String url)
    {
        try {
            String pathSubString = url.substring(36);
            String path = "D:\\Music\\" + pathSubString+".mp3";
            String decodedPath = URLDecoder.decode(path, "UTF-8");
            File file = new File(decodedPath);
            AudioFile audioFile = AudioFileIO.read(file);
            Integer totalSec = audioFile.getAudioHeader().getTrackLength();
            Integer minutes = (totalSec % 3600) / 60;
            Integer seconds = totalSec % 60;

            System.out.println("Minutes = " +minutes);
            System.out.println("seconds = " +seconds);
            return minutes.toString() + ":" + seconds.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "00.00";
        }
    }



}
