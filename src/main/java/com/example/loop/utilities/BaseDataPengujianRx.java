package com.example.loop.utilities;

import com.example.loop.callback.BaseCallback;
import com.example.loop.callback.BaseDataCallback;
import com.example.loop.callback.BaseListCallback;
import com.example.loop.callback.BaseListCallbackPengujian;

import java.util.List;

public final class BaseDataPengujianRx<T,E> {
 

    public BaseListCallbackPengujian<T,E> bodyListPengujianCallback(boolean status, List<T> response, E response2, String message) {
        BaseListCallbackPengujian<T,E> baseCallback = new BaseListCallbackPengujian<>();
        baseCallback.status = status;
        baseCallback.data = response;
        baseCallback.data_pengujian = response2;
        baseCallback.message = message;
		return baseCallback;
    }

}