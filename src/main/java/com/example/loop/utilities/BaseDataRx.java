package com.example.loop.utilities;

import com.example.loop.callback.BaseCallback;
import com.example.loop.callback.BaseDataCallback;
import com.example.loop.callback.BaseListCallback;
import com.example.loop.callback.BaseListCallbackPengujian;

import java.util.List;

public final class BaseDataRx<T> {


    public BaseDataCallback<T> bodyDataCallback(boolean status, T response, String message) {
        BaseDataCallback<T> baseCallback = new BaseDataCallback<>();
        baseCallback.status = status;
        baseCallback.data = response;
        baseCallback.message = message;
		return baseCallback;
    }

    public BaseListCallback<T> bodyListCallback(boolean status, List<T> response, String message) {
        BaseListCallback<T> baseCallback = new BaseListCallback<>();
        baseCallback.status = status;
        baseCallback.data = response;
        baseCallback.message = message;
		return baseCallback;
    }


    public BaseCallback bodyCallback(boolean status, String message) {
        BaseCallback baseCallback = new BaseCallback();
        baseCallback.status = status;
        baseCallback.message = message;
		return baseCallback;
    }
}