package com.example.loop.utilities;

import com.example.loop.model.KlDivergence;
import com.example.loop.model.PrediksiLaguDataV2;
import com.example.loop.model.ItemPlaycountData;
import com.example.loop.model.TemporalDiversityDataV2;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;


public class EmAlgoritmaV2 {

    private static double jumlahCluster = 2;
    private static List<TemporalDiversityDataV2> temporalDiversityData;

    private static List<Double> mWeight;
    private static List<Double> mMeanTime;
    private static List<Double> mMeanDom;
    private static List<Double> mMeanWeekDay;
    private static List<Double> mMeanMonth;
    private static List<Double> mMeanDiversity;
    private static List<double[][]> mVarianceCovariance;

    private static List<TemporalDiversityDataV2> mCluster;
    private static List<TemporalDiversityDataV2> mCluster2;
    private static ArrayList<Double> prob;
    private static List<ArrayList<Double>> finalProb;

    private static DummyData dummyData = new DummyData();

    private static PrintWriter printWriter = null;
    private static int topK = 1000;
    private static int limit = 4;
    private static double epsilon = 0.001;

    public static List<PrediksiLaguDataV2> build(Integer limit, Integer topK, Double epsilon, List<TemporalDiversityDataV2> temporalDiversityData) {
        EmAlgoritmaV2.limit = limit;
        EmAlgoritmaV2.topK = topK;
        EmAlgoritmaV2.epsilon = epsilon;
        EmAlgoritmaV2.temporalDiversityData = temporalDiversityData;
        initObject();
        doCluster();
        firstInitWMV();
        int iteration = 0;
        while (true) {
            iteration++;
            if (iteration == 85) EmAlgoritmaV2.epsilon = 0.01;
            if (iteration == 100) EmAlgoritmaV2.epsilon = 0.0;
            List<EmParameterNew> listParam = new ArrayList<>();
            finalProb = new ArrayList<>();
            boolean allClusterConvergence = true;
            for (int i = 0; i < jumlahCluster; i++) {
//                System.out.println("probability Iterasi" + iteration + "cluster ke:" + i + " = " + prob.toString());
                System.out.println("probability Iterasi" + iteration + "cluster ke:" + i);
                expectationStep(i);
                EmParameterNew param = maximizationStep(i);
                listParam.add(param);
                if (isConvergence(i, param)) {
                    if (allClusterConvergence) {
                        finalProb.add(prob);
//                        System.out.println("probability final cluster" + i + " = " + prob.toString());
                        allClusterConvergence = true;
                    }
                } else {
                    allClusterConvergence = false;
                }
            }
            if (allClusterConvergence) {
                System.out.println("-------Semua cluster Convergence-------");
                return prediksiLagu();
            }
            for (int i = 0; i < jumlahCluster; i++) {
                mWeight.set(i, ld(listParam.get(i).weightBaru));         // set nilai weight yg baru
                mMeanTime.set(i, ld(listParam.get(i).meanTimeBaru));         // set nilai meanTime yg baru
                mMeanDom.set(i, ld(listParam.get(i).meanDomBaru));         // set nilai meanDom yg baru
                mMeanWeekDay.set(i, ld(listParam.get(i).meanWeekDayBaru));         // set nilai meanDom yg baru
                mMeanMonth.set(i, ld(listParam.get(i).meanMonthBaru));         // set nilai meanDom yg baru
                mMeanDiversity.set(i, ld(listParam.get(i).meanDiversityBaru));         // set nilai meanDom yg baru
                mVarianceCovariance.set(i, listParam.get(i).varianceCovarianceBaru);     // set nilai varianceCovariance yg baru
            }
        }
    }

    public static void initObject() {
        mWeight = new ArrayList<>();
        mMeanTime = new ArrayList<>();
        mMeanDom = new ArrayList<>();
        mMeanWeekDay = new ArrayList<>();
        mMeanMonth = new ArrayList<>();
        mMeanDiversity = new ArrayList<>();
        mVarianceCovariance = new ArrayList<>();
        mCluster = new ArrayList<>();
        mCluster2 = new ArrayList<>();
        prob = new ArrayList<>();
        finalProb = new ArrayList<>();
    }

    public static void main(String[] args) {
//        EmAlgoritma.temporalDiversityData = dummyData.tempDataDummy();
//        build(temporalDiversityData, );
        System.out.println(Utility.formatZDateStr("2009-05-04T23:08:57Z"));
    }

    private static List<PrediksiLaguDataV2> prediksiLagu() {
        List<KlDivergence> klDivergence = klDivergence(); // 0, 0, 0.14
        List<TemporalDiversityDataV2> sessionTerpilih = getSessionTerpilih(klDivergence);
        List<ItemPlaycountData> laguTerpilih = getSemuaLaguTerpilih(sessionTerpilih);
        List<PrediksiLaguDataV2> listPrediksiLagu = prediksiLaguFormula(klDivergence, sessionTerpilih, laguTerpilih);
        Set<PrediksiLaguDataV2> listPrediksiBest = new HashSet<>(listPrediksiLagu); // Mencari prediksi terbaik di antara cluster
        List<PrediksiLaguDataV2> finalPrediksiLagu = new ArrayList<>(listPrediksiBest); //convert ke list
        Collections.sort(finalPrediksiLagu, Collections.reverseOrder());
        return finalPrediksiLagu;
    }

    private static List<PrediksiLaguDataV2> prediksiLaguFormula(
            List<KlDivergence> klDivergence,
            List<TemporalDiversityDataV2> sessionTerpilih,
            List<ItemPlaycountData> laguTerpilih
    ) {
        List<PrediksiLaguDataV2> listPrediksiLagu = new ArrayList<>();
        double rs = rataPlaycount(getListActivePlaycount()); // rata" playcount dlm active session
        for (int j = 0; j < laguTerpilih.size(); j++) {
            PrediksiLaguDataV2 prediksiLaguData = new PrediksiLaguDataV2();
            double jmlDklXPlaycount = 0d;
            double jumlahSimiliarDkl = 0d;

            int playcountSessionKe;
            for (int k = 0; k < klDivergence.size(); k++) { //ini k=3 jadi listDkl.size = 3
                TemporalDiversityDataV2 session = sessionTerpilih.get(k);   //session di dlm list session ke k
                List<ItemPlaycountData> listSong = session.songData;
                ItemPlaycountData song = laguTerpilih.get(j);        //lagu di dlm session ke j
                if (listSong.size() > 0) {  //list tidak boleh lebih kecil dari 0
                    playcountSessionKe = song.playcount;
                } else {
                    playcountSessionKe = 0;
                }
                List<Integer> listPlaycount = new ArrayList<>();
                //System.out.println("playcountSessionKe = " +j+ " = " + limitDigit(playcountSessionKe,3));
                for (ItemPlaycountData songData : listSong) {
                    int playcount = songData.playcount;
                    listPlaycount.add(playcount); // memasukan semua playcount lagu yg ada di session l
                }
                double rataRataPlaycount = ld(rataPlaycount(listPlaycount));
                double pccs = playcountSessionKe - rataRataPlaycount;
                jmlDklXPlaycount += ld(klDivergence.get(k).divergence) * ld(pccs);
                jumlahSimiliarDkl += ld(klDivergence.get(k).divergence);
            }
            double total = ld(jmlDklXPlaycount) / ld(jumlahSimiliarDkl);

            double rai = ld(rs) + ld(total);
            prediksiLaguData.ratingLagu = ld(rai);
            prediksiLaguData.songData = laguTerpilih.get(j);
            listPrediksiLagu.add(prediksiLaguData);
            System.out.println("RAI = " + j + " = " + ld(rai));
        }
        return listPrediksiLagu;
    }

    private static List<TemporalDiversityDataV2> getSessionTerpilih(List<KlDivergence> kl) { // ambil session yg similiar saja.
        List<TemporalDiversityDataV2> sessionTerpilih = new ArrayList<>();
        for (int i = 0; i < temporalDiversityData.size(); i++) {
            if (i == kl.size())
                break; // kita pilih session yg terpilih saja, kl Divergence isinya session terpilih dari top K
            int position = kl.get(i).position; // ambil posisi kl divergence yg telah di urutkan
            sessionTerpilih.add(temporalDiversityData.get(position)); // masukan nilai terbaik utk di rekomendasikan
            writeTextFile("session_terpilih.csv", "Session terpilih : ", kl.get(i).position.toString(), kl.get(i).divergence.toString());
            System.out.println("klDivergences = " + kl.get(i).divergence + " position = " + kl.get(i).position);
        }
        printWriter.close();
        return sessionTerpilih;
    }

    private static List<Integer> getListActivePlaycount() { //Mencari rata" playcount dlm active session
        List<Integer> listActivePlaycount = new ArrayList<>();
        TemporalDiversityDataV2 activeSession = temporalDiversityData.get(temporalDiversityData.size() - 1);
        List<ItemPlaycountData> songDataList = activeSession.songData;
        for (int j = 0; j < songDataList.size(); j++) {
            int playcount = songDataList.get(j).playcount;
            listActivePlaycount.add(playcount);
        }
        return listActivePlaycount;
    }

    private static void writeTextFile(String judul, String... arg) {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(judul);
        } catch (IOException e) {
            e.printStackTrace();
        }
        printWriter = new PrintWriter(fileWriter);
        printWriter.println(arg[0] + arg[1] + " = " + arg[2]);
        printWriter.println("----------------------------------------------\n");
    }

    private static List<ItemPlaycountData> getSemuaLaguTerpilih(List<TemporalDiversityDataV2> listSession) {
        System.out.println("Loading.. Get Semua Lagu terpilih");
        List<ItemPlaycountData> songDataList = new ArrayList<>();
        List<Integer> idSemuaLagu = new ArrayList<>();
        for (TemporalDiversityDataV2 session : listSession) {
            List<ItemPlaycountData> listSongData = session.songData;
            for (ItemPlaycountData songDataV2 : listSongData) {
                idSemuaLagu.add(songDataV2.id);
            }
        }
        HashMap<Integer, Integer> occurrenceId = Utility.countOccurrenceNumber(idSemuaLagu);
        Set<Integer> setUniqueData = new HashSet<>(idSemuaLagu);


        for (Integer idSong : setUniqueData) {
            ItemPlaycountData songData = new ItemPlaycountData();
            songData.id = idSong;
            songData.playcount = occurrenceId.get(idSong);
            songDataList.add(songData);
            writeTextFile("lagu_terpilih.csv", "Lagu terpilih : ", songData.id.toString(), songData.playcount.toString());
        }
        printWriter.close();
        System.out.println("songDataList size = " + songDataList.size());
        return new ArrayList<>(songDataList);
    }

    private static double rataPlaycount(List<Integer> playcount) {
        double rataPlaycount = 0d;
        for (int i = 0; i < playcount.size(); i++) {
            rataPlaycount += playcount.get(i);
        }
        return rataPlaycount / playcount.size();
    }

    private static List<KlDivergence> klDivergence() {  // kl divergence makin kecil makin bagus
        System.out.println("Loading.. klDivergence");
        List<ArrayList<Double>> listSessionSimilarity = klDivergenceFormula(finalProb);
        System.out.println("List Session Similarity Size = " + listSessionSimilarity.size());
        ArrayList<Double> similarityC1 = listSessionSimilarity.get(0);
        ArrayList<Double> similarityC2 = listSessionSimilarity.get(1);
        ArrayList<Double> sumSimilarity = new ArrayList<>();
        for (int j = 0; j < similarityC1.size(); j++) {
            Double sum = ld(similarityC1.get(j)) + ld(similarityC2.get(j)); //size c1 dan c2 harus sama
            sumSimilarity.add(ld(sum));
            writeTextFile("kl_divergence.csv", "sessionSimilarity position : ", String.valueOf(j), String.valueOf(ld(sum)));   // disini menjumlahkan klDiv prob cluster 1 dan 2
        }
        printWriter.close();
        return insertIntoKlDivergence(sumSimilarity);
    }

    private static List<ArrayList<Double>> klDivergenceFormula(List<ArrayList<Double>> finalProb) { // final prob cluster 1 dan cluster 2
        List<ArrayList<Double>> listSessionSimilarity = new ArrayList<>();
        for (int i = 0; i < finalProb.size(); i++) { // final prob cluster 1 dan cluster 2
            ArrayList<Double> sessionSimilarity = new ArrayList<>();
            ArrayList<Double> prob = finalProb.get(i);
            double activeProb = prob.get(prob.size() - 1);// probability active session ambil paling terakhir
            for (int j = 0; j < prob.size() - 1; j++) { //prob.size - 1 karena item terakhir active sesion gk ikutan di itung
                double P = ld(activeProb); // P adalah probability active session
                double Q = ld(prob.get(j)); // Q adalah probability ke j dari cluster i
                if (Q > 0d || P > 0) { // Q atau P harus lebih besar dari 0
                    double actBagiYik = ld(P / Q);                              // Rumus KL DIVERGENCE = sum of { P(i) * log(P(i)/Q(i)) }
                    double logAct = Math.log10(actBagiYik);
                    double klDivergence = ld(activeProb) * ld(logAct);
                    sessionSimilarity.add(ld(klDivergence));
                } else {
                    sessionSimilarity.add(0d); // klo P = 0 || Q == 0 maka hasil 0
                }
            }
            listSessionSimilarity.add(sessionSimilarity);
        }
        return listSessionSimilarity; //similiraty 0 berarti gk ada kecocokan, klo 1 berarti cocok
    }

    private static List<KlDivergence>  insertIntoKlDivergence(ArrayList<Double> sumSimilarity) {
        List<KlDivergence> klDivergences = new ArrayList<>();
        for (int i = 0; i < sumSimilarity.size(); i++) {
            if (sumSimilarity.get(i) > 0) {
                KlDivergence data = new KlDivergence();
                data.position = i;                                  // sini pindahkan nilai klDivergence ke Model agar gampang di susun
                data.divergence = ld(sumSimilarity.get(i));
                klDivergences.add(data);
            }
        }
        Collections.sort(klDivergences); // Di urutkan ke yg terkecil
        // ini pembatasnya top k, kita yg nentuin top k nya
        // ini kl divergence yg sudah terpotong
        // sublist gunanya untuk memotong list
        // sublist ambil dari index 0 sampai topK
        List<KlDivergence> finalKlDivergence = new ArrayList<>();   // ini kl divergence yg sudah terpotong
        for (int i = 0; i < klDivergences.size(); i++) {
            if (i > topK) break; // ini pembatasnya top k, kita yg nentuin top k nya
            finalKlDivergence.add(klDivergences.get(i));
        }
        return finalKlDivergence;
    }

    private static void doCluster() {
        Random randomGenerator = new Random();
//        int randomInt = randomGenerator.nextInt(temporalDiversityData.size()) + 1; // ini angka random
        int setengah = temporalDiversityData.size() / 2; // ini angka total bagi 2 untuk bagi Cluster
        for (int i = 0; i < temporalDiversityData.size(); i++) {
            if (i < setengah) {
                mCluster.add(temporalDiversityData.get(i));
            } else {
                mCluster2.add(temporalDiversityData.get(i));
            }
        }
    }

    private static void firstInitWMV() {
        mWeight.clear();
        mMeanTime.clear();
        mMeanDom.clear();
        mMeanWeekDay.clear();
        mMeanMonth.clear();
        mMeanDiversity.clear();
        mVarianceCovariance.clear();

        for (int i = 0; i < jumlahCluster; i++) {
            List<TemporalDiversityDataV2> cluster = new ArrayList<>();
            switch (i) {
                case 0: {
                    cluster = mCluster;
                    break;
                }
                case 1: {
                    cluster = mCluster2;
                    break;
                }
            }
            double[][] meanTemporal = itungMean(cluster);
            double weight = ld(1 / jumlahCluster);
            double meanTime = ld(meanTemporal[0][0]);
            double meanDom = ld(meanTemporal[1][0]);
            double meanWeekDay = ld(meanTemporal[2][0]);
            double meanMonth = ld(meanTemporal[3][0]);
            double meanDiversity = ld(meanTemporal[4][0]);

            mWeight.add(weight);   // init weight
            mMeanTime.add(meanTime); // init meanTime
            mMeanDom.add(meanDom); // init meanDom
            mMeanWeekDay.add(meanWeekDay); // init meanWeekDay
            mMeanMonth.add(meanMonth); // init meanMonth
            mMeanDiversity.add(meanDiversity); // init meanDiversity

            double[][] varianceCovariance = itungVarianceCovariance(cluster, meanTime, meanDom, meanWeekDay, meanMonth, meanDiversity);
//            MatrixOperations.printMatrix(varianceCovariance, 0);
            mVarianceCovariance.add(varianceCovariance); // init varianceCovariance
        }
    }

    private static void expectationStep(int clusterKe) {
        prob = probability(clusterKe);
    }

    private static EmParameterNew maximizationStep(int i) {
        EmParameterNew param = new EmParameterNew();
        double jmlProbability = 0d;
        double jmlProbXTime = 0d;
        double jmlProbXDom = 0d;
        double jmlProbXWeekDay = 0d;
        double jmlProbXMonth = 0d;
        double jmlProbXDiversity = 0d;

        for (int j = 0; j < prob.size(); j++) {
            double time = temporalDiversityData.get(j).hour;
            double dom = temporalDiversityData.get(j).dayOfMonth;
            double weekDay = temporalDiversityData.get(j).weekday;
            double month = temporalDiversityData.get(j).month;
            double diversity = temporalDiversityData.get(j).diversity;
            jmlProbXTime += ld(ld(prob.get(j)) * time);
            jmlProbXDom += ld(ld(prob.get(j)) * dom);
            jmlProbXWeekDay += ld(ld(prob.get(j)) * weekDay);
            jmlProbXMonth += ld(ld(prob.get(j)) * month);
            jmlProbXDiversity += ld(ld(prob.get(j)) * diversity);
            jmlProbability += ld(ld(prob.get(j)));
        }

        param.weightBaru = ld(ld(jmlProbability) / prob.size());
        param.meanTimeBaru = ld(ld(jmlProbXTime) / ld(jmlProbability));   //------Menghitung Mean Time baru (Maximization)
        param.meanDomBaru = ld(ld(jmlProbXDom) / ld(jmlProbability));   //------Menghitung Mean Dom baru (Maximization)
        param.meanWeekDayBaru = ld(ld(jmlProbXWeekDay) / ld(jmlProbability));   //------Menghitung Week Day baru (Maximization)
        param.meanMonthBaru = ld(ld(jmlProbXMonth) / ld(jmlProbability));   //------Menghitung Month baru (Maximization)
        param.meanDiversityBaru = ld(ld(jmlProbXDiversity) / ld(jmlProbability));   //------Menghitung Diversity baru (Maximization)
        param.varianceCovarianceBaru = varianceCovarianceMaximization(
                prob,
                ld(mMeanTime.get(i)),
                ld(mMeanDom.get(i)),
                ld(mMeanWeekDay.get(i)),
                ld(mMeanMonth.get(i)),
                ld(mMeanDiversity.get(i))
        );

        return param;
    }

    private static double[][] itungMean(List<TemporalDiversityDataV2> cluster) {
        float time = 0;
        float dom = 0;
        float weekDay = 0;
        float month = 0;
        float diversity = 0;
        float meanTime;
        float meanDom;
        float meanWeekDay;
        float meanMonth;
        float meanDiversity;

        for (int i = 0; i < cluster.size(); i++) {
            time += cluster.get(i).hour;
            dom += cluster.get(i).dayOfMonth;
            weekDay += cluster.get(i).weekday;
            month += cluster.get(i).month;
            diversity += cluster.get(i).diversity;
        }

        meanTime = time / cluster.size();
        meanDom = dom / cluster.size();
        meanWeekDay = weekDay / cluster.size();
        meanMonth = month / cluster.size();
        meanDiversity = diversity / cluster.size();

//        return new double[][]{{meanTime}, {meanDom}};
        return new double[][]{{meanTime}, {meanDom}, {meanWeekDay}, {meanMonth}, {meanDiversity}};
    }

    private static double[][] itungVarianceCovariance(
            List<TemporalDiversityDataV2> cluster,
            double rataMeanTime,
            double rataMeanDom,
            double rataMeanWeekDay,
            double rataMeanMonth,
            double rataMeanDiversity
    ) {
        double weight = 1d / (double) (cluster.size() - 1);
        double time = 0, dom = 0, weekDay = 0, month = 0, diversity = 0;
        double timeDom = 0, timeWeekDay = 0, timeMonth = 0, timeDiversity = 0;
        double domWeekDay = 0, domMonth = 0, domDiversity = 0;
        double weekDayMonth = 0, weekDayDiversity = 0;
        double monthDiversity = 0;

        for (int i = 0; i < cluster.size(); i++) {
            time += ld(Math.pow(cluster.get(i).hour - rataMeanTime, 2));
            dom += ld(Math.pow(cluster.get(i).dayOfMonth - rataMeanDom, 2));
            weekDay += ld(Math.pow(cluster.get(i).weekday - rataMeanWeekDay, 2));
            month += ld(Math.pow(cluster.get(i).month - rataMeanMonth, 2));
            diversity += ld(Math.pow(cluster.get(i).diversity - rataMeanDiversity, 2));
            timeDom += ld((cluster.get(i).hour - rataMeanTime) * (cluster.get(i).dayOfMonth - rataMeanDom));
            timeWeekDay += ld((cluster.get(i).hour - rataMeanTime) * (cluster.get(i).weekday - rataMeanWeekDay));
            timeMonth += ld((cluster.get(i).hour - rataMeanTime) * (cluster.get(i).month - rataMeanMonth));
            timeDiversity += ld((cluster.get(i).hour - rataMeanTime) * (cluster.get(i).diversity - rataMeanDiversity));
            domWeekDay += ld((cluster.get(i).dayOfMonth - rataMeanDom) * (cluster.get(i).weekday - rataMeanWeekDay));
            domMonth += ld((cluster.get(i).dayOfMonth - rataMeanDom) * (cluster.get(i).month - rataMeanMonth));
            domDiversity += ld((cluster.get(i).dayOfMonth - rataMeanDom) * (cluster.get(i).diversity - rataMeanDiversity));
            weekDayMonth += ld((cluster.get(i).weekday - rataMeanWeekDay) * (cluster.get(i).month - rataMeanMonth));
            weekDayDiversity += ld((cluster.get(i).weekday - rataMeanWeekDay) * (cluster.get(i).diversity - rataMeanDiversity));
            monthDiversity += ld((cluster.get(i).month - rataMeanMonth) * (cluster.get(i).diversity - rataMeanDiversity));
        }

        double varianceTime = ld(weight * time);
        double varianceDom = ld(weight * dom);
        double varianceWeekDay = ld(weight * weekDay);
        double varianceMonth = ld(weight * month);
        double varianceDiversity = ld(weight * diversity);
        double covarTimeDom = ld(weight * timeDom);
        double covarTimeWeekDay = ld(weight * timeWeekDay);
        double covarTimeMonth = ld(weight * timeMonth);
        double covarTimeDiversity = ld(weight * timeDiversity);
        double covarDomWeekDay = ld(weight * domWeekDay);
        double covarDomMonth = ld(weight * domMonth);
        double covarDomDiversity = ld(weight * domDiversity);
        double covarWeekDayMonth = ld(weight * weekDayMonth);
        double covarWeekDayDiversity = ld(weight * weekDayDiversity);
        double covarMonthDiversity = ld(weight * monthDiversity);


//        return new double[][]{ // Matrix 2x2
//                //Time, Dom
//                {varianceTime, covarTimeDom}, //Time
//                {covarTimeDom, varianceDom}, //Dom
//        };


        return new double[][]{ // Matrix 5x5
                //Time, Dom, WeekDay, Month, Diversity
                {varianceTime, covarTimeDom, covarTimeWeekDay, covarTimeMonth, covarTimeDiversity}, //Time
                {covarTimeDom, varianceDom, covarDomWeekDay, covarDomMonth, covarDomDiversity}, //Dom
                {covarTimeWeekDay, covarDomWeekDay, varianceWeekDay, covarWeekDayMonth, covarWeekDayDiversity}, //WeekDay
                {covarTimeMonth, covarDomMonth, covarWeekDayMonth, varianceMonth, covarMonthDiversity}, //Month
                {covarTimeDiversity, covarDomDiversity, covarWeekDayDiversity, covarMonthDiversity, varianceDiversity} //Diversity
        };
    }

    private static ArrayList<Double> nilaiN(int clusterKe) {

        ArrayList<Double> listNilaiN = new ArrayList<>();
        List<TemporalDiversityDataV2> semuaData = temporalDiversityData;

        double rataMeanTime = mMeanTime.get(clusterKe);
        double rataMeanDom = mMeanDom.get(clusterKe);
        double rataMeanWeekDay = mMeanWeekDay.get(clusterKe);
        double rataMeanMonth = mMeanMonth.get(clusterKe);
        double rataMeanDiversity = mMeanDiversity.get(clusterKe);
        double[][] matrixVarianceCovariance = mVarianceCovariance.get(clusterKe);
        double pi = 3.14;
        double[][] inverse = MatrixOperations.invertMatrix(matrixVarianceCovariance);
        double determinant = ld(MatrixOperations.matrixDeterminant(matrixVarianceCovariance));
//        System.out.println("determinant clusterKe: " +clusterKe+" ="+determinant);
        double xDeterminant = ld(Math.pow(2d * pi, jumlahCluster) * ld(determinant));
        double akar = Math.sqrt(xDeterminant);
        double nilai = ld(1d / akar);

        for (int i = 0; i < semuaData.size(); i++) {                                                   //ITUNG NILAI N
            double time = semuaData.get(i).hour - rataMeanTime;
            double dom = semuaData.get(i).dayOfMonth - rataMeanDom;
            double weekDay = semuaData.get(i).weekday - rataMeanWeekDay;
            double month = semuaData.get(i).month - rataMeanMonth;
            double diversity = semuaData.get(i).diversity - rataMeanDiversity;

//            double[][] matrixTimeDom = {{time}, {dom}};
//            double[][] transposeMatrixTimeDom = {{time, dom}};
            double[][] matrixTimeDom = {{time}, {dom}, {weekDay}, {month}, {diversity}};
            double[][] transposeMatrixTimeDom = {{time, dom, weekDay, month, diversity}};

            double[][] multipleMatrix = MatrixOperations.multiplyMatrices(transposeMatrixTimeDom, inverse);
            double[][] multipleMatrixFinal = MatrixOperations.multiplyMatrices(multipleMatrix, matrixTimeDom);

            double valueExp = -0.5d * multipleMatrixFinal[0][0];
            double exp = Math.exp(valueExp);

            double nilaiN = nilai * exp;

            listNilaiN.add(ld(nilaiN));
            // System.out.println("matrixTimeDom "+i+" == "+limitDigit(matrixTimeDom[0][0]));
            // System.out.println("matrixTimeDom "+i+" == "+limitDigit(matrixTimeDom[1][0]));
            // System.out.println("transposeMatrixTimeDom "+i+" == "+limitDigit(transposeMatrixTimeDom[0][0]));
            // System.out.println("transposeMatrixTimeDom "+i+" == "+limitDigit(transposeMatrixTimeDom[0][1]));
            // System.out.println("multipleMatrix "+i+" == "+limitDigit(multipleMatrix[0][0]));
            // System.out.println("multipleMatrix "+i+" == "+limitDigit(multipleMatrix[0][1]));
            // System.out.println("multipleMatrixFinal "+i+" == "+limitDigit(multipleMatrixFinal[0][0]));
            // System.out.println("exp "+i+" == "+limitDigit(exp));
//             System.out.println("nilai == "+ limitDigit(nilai));
            // System.out.println("Cluster "+clusterKe+ "nilaiN "+i+" == "+limitDigit(nilaiN));
        }
        return listNilaiN;
    }

    private static ArrayList<Double> probability(int clusterKe) {
        ArrayList<Double> probability = new ArrayList<>();
        ArrayList<Double> listNilaiNNow = nilaiN(clusterKe);
        ArrayList<Double> listNilaiNC1 = nilaiN(0);
        ArrayList<Double> listNilaiNC2 = nilaiN(1);

        for (int i = 0; i < listNilaiNNow.size(); i++) {
            // yik = probability item i dalam cluster k
            // jmw = weight * nilaiN cluster 1 +weight * nilaiN cluster 2 dan seterusnya tergantung jml cluster
            //knp weight.get(clusterKe -1) ?? karena weight utk cluster 1 = weight.get(0), dimulai dari 0 arraylist
            double weight = mWeight.get(clusterKe);
            double nilaiN = ld(listNilaiNNow.get(i));
            double jmwC1 = ld(weight) * ld(listNilaiNC1.get(i));
            double jmwC2 = ld(weight) * ld(listNilaiNC2.get(i));

            double jmw = ld(ld(jmwC1) + ld(jmwC2));  //ambil nilai weight cluster ke
            double weightXnilaiN = ld(mWeight.get(clusterKe) * nilaiN);
            double yik = ld(weightXnilaiN / jmw);
            probability.add(yik);
        }
        return probability;
    }

    private static double[][] varianceCovarianceMaximization(
            ArrayList<Double> prob,
            double meanTimePrev,
            double meanDomPrev,
            double meanWeekDayPrev,
            double meanMonthPrev,
            double meanDiversityPrev) { //-----Menghitung variance-covariance matrix(Maximization)

        double jumlahProb = 0d;

        List<TemporalDiversityDataV2> semuaData = temporalDiversityData;
//        double[][] multipleMatrixTimeTDomFinal = new double[2][2];
        double[][] multipleMatrixTimeTDomFinal = new double[5][5];

        for (int i = 0; i < semuaData.size(); i++) {
            jumlahProb += ld(prob.get(i));

            double timeKurangMean = ld(semuaData.get(i).hour - meanTimePrev);
            double domKurangMean = ld(semuaData.get(i).dayOfMonth - meanDomPrev);
            double weekDayKurangMean = ld(semuaData.get(i).weekday - meanWeekDayPrev);
            double monthKurangMean = ld(semuaData.get(i).month - meanMonthPrev);
            double diversityKurangMean = ld(semuaData.get(i).diversity - meanDiversityPrev);

//            double[][] matrixTimeDom = {{timeKurangMean}, {domKurangMean}};
//            double[][] transposeMatrixTimeDom = {{timeKurangMean, domKurangMean}};
            double[][] matrixTimeDom = {{timeKurangMean}, {domKurangMean}, {weekDayKurangMean}, {monthKurangMean}, {diversityKurangMean}};
            double[][] transposeMatrixTimeDom = {{timeKurangMean, domKurangMean, weekDayKurangMean, monthKurangMean, diversityKurangMean}};

            double[][] multipleMatrixTimeTDom = MatrixOperations.multiplyMatrices(matrixTimeDom, transposeMatrixTimeDom);

            for (int h = 0; h < multipleMatrixTimeTDom.length; h++) {
                for (int j = 0; j < multipleMatrixTimeTDom[h].length; j++) {
                    double hasil = ld(multipleMatrixTimeTDom[h][j]) * ld(prob.get(i));
                    multipleMatrixTimeTDomFinal[h][j] += ld(hasil);
                    // System.out.print(limitDigit(multipleMatrixTimeTDom[h][j] * prob.get(i),3)+"\t");
                    //Disini ngitung perkalian pertambahan matrix
                }
            }
        }

        for (int h = 0; h < multipleMatrixTimeTDomFinal.length; h++) {
            for (int j = 0; j < multipleMatrixTimeTDomFinal[h].length; j++) {
                double hasil = ld(multipleMatrixTimeTDomFinal[h][j]) / ld(jumlahProb);
                multipleMatrixTimeTDomFinal[h][j] = ld(hasil);
            }
        } // Pembagian jumlahMatrixFinaldengna jml prob
        return multipleMatrixTimeTDomFinal;
    }

    private static boolean isConvergence(int clusterKe, EmParameterNew param) {
        double weightLama = mWeight.get(clusterKe);//----- Weight Sebelumnya
        double meanTimePrev = mMeanTime.get(clusterKe);            //----- Mean Time Sebelumnya
        double meanDomPrev = mMeanDom.get(clusterKe);       //----- Mean Time Sebelumnya
        double meanWeekDayPrev = mMeanWeekDay.get(clusterKe);       //----- Mean Time Sebelumnya
        double meanMonthPrev = mMeanMonth.get(clusterKe);       //----- Mean Time Sebelumnya
        double meanDiversityPrev = mMeanDiversity.get(clusterKe);       //----- Mean Time Sebelumnya
        double[][] varianceCovarianceLama = mVarianceCovariance.get(clusterKe); // varCovar sebelumnya
        double[][] matrixMean = {{param.meanTimeBaru}, {param.meanDomBaru}, {param.meanWeekDayBaru}, {param.meanMonthBaru}, {param.meanDiversityBaru}};

        double selisiWeightBaruDanLama = weightLama - param.weightBaru; // Mencari selisih weight lama - weight baru
        double selisihMeanTimePrevDanBaru = meanTimePrev - matrixMean[0][0]; //Menghitung selisih mean lama - baru
        double selisihMeanDomPrevDanBaru = meanDomPrev - matrixMean[1][0];
        double selisihMeanWeekDayPrevDanBaru = meanWeekDayPrev - matrixMean[2][0];
        double selisihMeanMonthPrevDanBaru = meanMonthPrev - matrixMean[3][0];
        double selisihMeanDiversityPrevDanBaru = meanDiversityPrev - matrixMean[4][0];
        double[][] selisihVarCovar = new double[5][5];
//        double[][] selisihVarCovar = new double[2][2];
        for (int h = 0; h < param.varianceCovarianceBaru.length; h++) {
            for (int j = 0; j < param.varianceCovarianceBaru[h].length; j++) {
                selisihVarCovar[h][j] = ld(varianceCovarianceLama[h][j] - param.varianceCovarianceBaru[h][j]);
            }
        }


        // System.out.println("Selisih weight baru - weight lama "+i+" "+limitDigit(selisiWeightBaruDanLama,3));      // print selisih weight lama - baru
        // System.out.println("Selisih meanTime lama - mean baru "+i+" "+limitDigit(selisihMeanTimePrevDanBaru,3));      // print selisih weight lama - baru
        // System.out.println("Selisih meanDom lama - mean baru "+i+" "+limitDigit(selisihMeanDomPrevDanBaru,3));      // print selisih weight lama - baru
        // System.out.println("Selisih variance covariance baru dan lama ");
        // MatrixOperations.printMatrix(selisihVarCovar,0);
//        return Math.abs(selisiWeightBaruDanLama) < epsilon &&
//                Math.abs(selisihMeanTimePrevDanBaru) < epsilon &&
//                Math.abs(selisihMeanDomPrevDanBaru) < epsilon &&
//                Math.abs(selisihVarCovar[0][0]) < epsilon &&
//                Math.abs(selisihVarCovar[0][1]) < epsilon &&
//                Math.abs(selisihVarCovar[1][0]) < epsilon &&
//                Math.abs(selisihVarCovar[1][1]) < epsilon;

        boolean b1 = Math.abs(selisiWeightBaruDanLama) < epsilon;
        boolean b2 = Math.abs(selisihMeanTimePrevDanBaru) < epsilon;
        boolean b3 = Math.abs(selisihMeanDomPrevDanBaru) < epsilon;
        boolean b4 = Math.abs(selisihMeanWeekDayPrevDanBaru) < epsilon;
        boolean b5 = Math.abs(selisihMeanMonthPrevDanBaru) < epsilon;
        boolean b6 = Math.abs(selisihMeanDiversityPrevDanBaru) < epsilon;
        boolean b7 = Math.abs(selisihVarCovar[0][0]) < epsilon;
        boolean b8 = Math.abs(selisihVarCovar[0][1]) < epsilon;
        boolean b9 = Math.abs(selisihVarCovar[1][0]) < epsilon;
        boolean b10 = Math.abs(selisihVarCovar[1][1]) < epsilon;

        return b1 && b2 && b3 && b4 && b5 && b6 && b7 && b8 && b9 && b10;
    }

    private static double ld(double value) { //LD itu limit digit , persingkat aja
        return Math.round(value * Math.pow(10, limit)) / Math.pow(10, limit);
    }

}
