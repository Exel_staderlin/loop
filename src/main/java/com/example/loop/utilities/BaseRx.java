package com.example.loop.utilities;

import com.example.loop.callback.BaseCallback;

public final class BaseRx {

    public BaseCallback bodyCallback(boolean status, String message) {
        BaseCallback baseCallback = new BaseCallback();
        baseCallback.status = status;
        baseCallback.message = message;
		return baseCallback;
    }
}