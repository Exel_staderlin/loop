package com.example.loop.config;

/**
 * Created by Exel staderlin on 8/31/2019.
 */

import com.example.loop.model.DataSet;
import com.example.loop.model.ModelTesting;
import com.example.loop.model.SessionData;
import com.example.loop.model.SessionLogData;
import com.example.loop.utilities.Utility;
import com.univocity.parsers.common.processor.RowListProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class BatchConfig {

    public static void main(String[] args) {
        Timer timer = runTimer();
        ArrayList<List<String>> userLikedGenre = readCsvToList("C:/dataset/user_liked_genre.csv");
//        ArrayList<List<String>> uniqueSongGenre = readCsvToList("C:/dataset/unique_user_liked_genre_all.csv");
        ArrayList<List<String>> totalGenre = readCsvToList("C:/dataset/total_genre_song.csv");
        List<ModelTesting> dataSets = recordDataTotalGenre(userLikedGenre, totalGenre);
        writeTextUserLikedGenre(dataSets);
        timer.cancel();
    }

    private static List<SessionLogData> getSessionLog(List<DataSet> dataSet) {
        List<SessionLogData> listData = new ArrayList<>();
        int i = 0;
        for (DataSet list : dataSet) {
            i++;
            SessionLogData data = new SessionLogData();
            data.id = i;
            data.id_user = Integer.parseInt(list.user.substring(7));
            data.created_date = Utility.formatZDate(list.createDate);
            listData.add(data);
        }
        return listData;

    }

    //    private static void experiment(ArrayList<List<String>> dataSet) {
//        List<DataSet> listData = new ArrayList<>();
//        for (List<String> list : dataSet) {
//            DataSet data = new DataSet();
////            data.user = list.get(0);
//            data.song = list.get(5);
//            listData.add(data);
//        }
//        writeTextPlaycount(listData);
//
//    }

    private static List<ModelTesting> recordData(ArrayList<List<String>> userLikedGenre, ArrayList<List<String>> uniqueSongGenre) {
        List<ModelTesting> dataset = new ArrayList<>();
        for (int i = 0; i < userLikedGenre.size(); i++) {
            ModelTesting data = new ModelTesting();
            String idUser = userLikedGenre.get(i).get(0);
            String likedGenre = userLikedGenre.get(i).get(1);
            int playcount = Integer.valueOf(userLikedGenre.get(i).get(2));
            data.id_user = idUser;
            data.liked_genre = likedGenre;
            data.playcount = playcount;

            for (int k = 0; k < uniqueSongGenre.size(); k++) {
                if (idUser.equals(uniqueSongGenre.get(k).get(0))) { //jika user sama
                    if (likedGenre.equals(uniqueSongGenre.get(k).get(1))) { // jika genre sama
                        int jmlGenreSong = Integer.parseInt(uniqueSongGenre.get(k).get(2)); // masukan jml genre
                        data.jml_genre_song = jmlGenreSong;
                        break;
                    }
                }
            }
            dataset.add(data);
        }
        return dataset;
    }

    private static List<ModelTesting> recordDataTotalGenre(ArrayList<List<String>> userLikedGenre, ArrayList<List<String>> totalGenre) {
        List<ModelTesting> dataset = new ArrayList<>();
        for (int i = 0; i < userLikedGenre.size(); i++) {
            ModelTesting data = new ModelTesting();
            String idUser = userLikedGenre.get(i).get(0);
            String likedGenre = userLikedGenre.get(i).get(1);
            int playcount = Integer.valueOf(userLikedGenre.get(i).get(2));
            data.id_user = idUser;
            data.liked_genre = likedGenre;
            data.playcount = playcount;

            for (int k = 0; k < totalGenre.size(); k++) {
                if (likedGenre.equals(totalGenre.get(k).get(0))) { // jika genre sama
                    int jmlGenreSong = Integer.parseInt(totalGenre.get(k).get(1)); // masukan jml genre
                    data.jml_genre_song = jmlGenreSong;
                    break;
                }
            }
            dataset.add(data);
        }
        return dataset;
    }


    private static void experiment(ArrayList<List<String>> dataSet) {
        List<String> listData = new ArrayList<>();
        for (List<String> list : dataSet) {
            listData.add(list.get(5));
        }
        writeTextSongPlaycount(listData);

    }

    private static List<SessionData> generateSession(List<SessionLogData> response, Integer threshold) {
        List<SessionData> allSession = new ArrayList<>();
        List<SessionLogData> listLog = new ArrayList<>();
        SessionData sessionData = new SessionData();
        Date lastDate = null;
        int sessionIdUser = 1;
        for (SessionLogData log : response) {
            Date currentDate = log.created_date;
            if (lastDate != null && lastDate.before(currentDate)) {
                long dif = getDateDiffMinutes(lastDate, currentDate, TimeUnit.MINUTES);
//                System.out.println("difference time " + "=" + dif);
                if (dif >= threshold) {
                    sessionIdUser++;
                    sessionData.sessionLogData = listLog;
                    allSession.add(sessionData);

                    sessionData = new SessionData(); //Create New Session
                    listLog = new ArrayList<>();
                    sessionData.id = sessionIdUser;
                    sessionData.created_date = currentDate;
//                    System.out.println("Buat session baru ==" + sessionIdUser);//"Di Session Berikutnya";
                }
                listLog.add(log);
                lastDate = currentDate;
            } else {
                sessionData.id = sessionIdUser;
                sessionData.created_date = currentDate;
                listLog.add(log);
                lastDate = currentDate;
            }
        }
//        System.out.println("Total Session " + "=" + sessionIdUser);
        return allSession;
    }

    private static long getDateDiffMinutes(Date lastDate, Date currentDate, TimeUnit timeUnit) {
        long diffTime = currentDate.getTime() - lastDate.getTime();
        long diffMinutes = diffTime / (60 * 1000);
        return timeUnit.convert(diffMinutes, TimeUnit.MINUTES);
    }


    private static void writeText(ArrayList<List<String>> dataSet) {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("log_training.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter printWriter = new PrintWriter(fileWriter);

        for (List<String> data : dataSet) {
            printWriter.println(data.get(0) + "," + data.get(1) + "," + data.get(2) + "," + data.get(3) + "," + data.get(4) + "," + data.get(5));
        }
        printWriter.close();
    }

    private static void writeTextSongPlaycount(List<String> dataSet) {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("log_training.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter printWriter = new PrintWriter(fileWriter);
        Set<String> set = new HashSet<>(dataSet);
        System.out.println(set.size());
        for (String s : set) {
            printWriter.println(s + "," + Collections.frequency(dataSet, s));
        }
        printWriter.close();
    }

    private static void writeTextUserLikedGenre(List<ModelTesting> dataSet) {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("C://dataset/user_liked_genre(2).csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter printWriter = new PrintWriter(fileWriter);
        printWriter.println("id_user,liked_genre,jml_playcount,jml_genre_song");

        for (ModelTesting data : dataSet) {
            printWriter.println(data.id_user + "," + data.liked_genre+ "," + data.playcount+ "," + data.jml_genre_song);
        }
        printWriter.close();
    }


    private static void writeTextPlaycount(List<DataSet> dataSet) {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("log_training.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter printWriter = new PrintWriter(fileWriter);
        Set<DataSet> set = new HashSet<>(dataSet);
        List<DataSet> list = new ArrayList<>(set);
//        List<Integer> totalPlaycount = new ArrayList<>();

        Collections.sort(list);
        System.out.println(list.size());
        for (DataSet data : list) {
            printWriter.println(data.id + "," + Collections.frequency(dataSet, data));
        }
        printWriter.close();
    }

    private static void writeTextListDataset(ArrayList<List<DataSet>> dataSet) {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("log_training.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter printWriter = new PrintWriter(fileWriter);
        for (int i = 0; i < dataSet.size(); i++) {
            List<DataSet> listData = dataSet.get(i);
            Collections.sort(listData);
            for (DataSet data : listData) {
                printWriter.println(data.user + "," + data.createDate + "," + data.idArtist + "," + data.artist + "," + data.idSong + "," + data.song);
            }
        }
        printWriter.close();
    }

    private static void writeTextSession(ArrayList<List<DataSet>> dataSet) {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("log_training.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter printWriter = new PrintWriter(fileWriter);
        List<Integer> count = new ArrayList<>();
        for (int i = 0; i < dataSet.size(); i++) {
            List<DataSet> listData = dataSet.get(i);
            List<SessionLogData> getSessionLog = getSessionLog(listData);
            List<SessionData> sessionData = generateSession(getSessionLog, 30);
            count.add(sessionData.size());
//            printWriter.println(Integer.valueOf(listData.get(0).user.substring(7))+ "," +sessionData.size());
        }
        Collections.sort(count);
        Set<Integer> set = new HashSet<>(count);
        for (Integer angka : set) {
            printWriter.println(Collections.frequency(count, angka) + "," + angka);
        }
        printWriter.close();
    }


    private static ArrayList<List<String>> readCsvToList(String csvFileLocation) {
        ArrayList<List<String>> records = new ArrayList<>();
        CsvParser parser = new CsvParser(settingsCsvParser());
        parser.beginParsing(new File(csvFileLocation));
        try {
            String[] row;
            int loop = 1;
            int threshold = 5000000;
            while ((row = parser.parseNext()) != null) {
                records.add(Arrays.asList(row));
                if (loop >= threshold) {
                    System.out.println("load record :" + loop);
                }
                loop++;
            }
            System.out.println("record size :" + records.size());
            return records;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    private static CsvParserSettings settingsCsvParser() {
        CsvParserSettings settings = new CsvParserSettings(); // you'll find many options here, check the tutorial.
        RowListProcessor rowProcessor = new RowListProcessor();
        settings.setNullValue("<NULL>");
        settings.setEmptyValue("<EMPTY>");
        settings.setIgnoreLeadingWhitespaces(false);
        settings.setIgnoreTrailingWhitespaces(false);
        settings.setNumberOfRecordsToRead(10000000);
        settings.setSkipEmptyLines(false);
        settings.setMaxCharsPerColumn(10000000);
        settings.setInputBufferSize(1000);
        settings.setReadInputOnSeparateThread(false);
        settings.setLineSeparatorDetectionEnabled(true);
        settings.setRowProcessor(rowProcessor);
        settings.setHeaderExtractionEnabled(true);
        return settings;
    }

    private static Timer runTimer() {
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            private int i = 0;

            public void run() {
                System.out.println("time running :" + i++); /*difference time*/
            }
        };
        timer.scheduleAtFixedRate(task, 0, 1000); //1000ms = 1sec
        return timer;
    }


}