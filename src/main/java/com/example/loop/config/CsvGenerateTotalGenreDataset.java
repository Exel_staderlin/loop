package com.example.loop.config;

import com.example.loop.model.DataSet;
import com.example.loop.utilities.Utility;
import com.univocity.parsers.common.processor.RowListProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

public class CsvGenerateTotalGenreDataset {

    public static void main(String[] args) {
        Timer timer = runTimer();

        ArrayList<List<String>> songRecord = readCsvToList("C:/dataset/song_id_idgenre_idartist_doang.csv");
        List<DataSet> listDataset = recordGenrePerSong(songRecord);
        writeTotalGenreInDataset(listDataset);
        timer.cancel();
    }

    private static List<DataSet> recordGenrePerSong(ArrayList<List<String>> datasetV1) {
        List<DataSet> dataset = new ArrayList<>();
        for (int i = 0; i < datasetV1.size(); i++) {
            DataSet data = new DataSet();
            data.idSong = datasetV1.get(i).get(0);
            data.idArtist = datasetV1.get(i).get(1);
            data.idGenre = datasetV1.get(i).get(2);
            dataset.add(data);
        }
        return dataset;
    }

    private static void writeTotalGenreInDataset(List<DataSet> listDataset) {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("C://dataset/total_genre_song.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(listDataset.size());
        PrintWriter printWriter = new PrintWriter(fileWriter);

        List<Integer> listIdGenre = new ArrayList<>();
        for (DataSet data : listDataset) {
            listIdGenre.add(Integer.valueOf(data.idGenre));     // add semua id genre
        }
        HashMap<Integer, Integer> occurrenceId = Utility.countOccurrenceNumber(listIdGenre);  // Hashmap semua id genre

        Set<Integer> uniqueGenre = new HashSet<>(listIdGenre);
        printWriter.println("id_genre,total_genre");
        for (Integer idGenre : uniqueGenre) {
            printWriter.println(idGenre +","+ occurrenceId.get(idGenre));
        }
        printWriter.close();
    }

    private static ArrayList<List<String>> readCsvToList(String csvFileLocation) {
        ArrayList<List<String>> records = new ArrayList<>();
        CsvParser parser = new CsvParser(settingsCsvParser());
        parser.beginParsing(new File(csvFileLocation));
        try {
            String[] row;
            int loop = 1;
            int threshold = 5000000;
            while ((row = parser.parseNext()) != null) {
                records.add(Arrays.asList(row));
                if (loop >= threshold) {
                    System.out.println("load record :" + loop);
                }
                loop++;
            }
            System.out.println("record size :" + records.size());
            return records;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    private static CsvParserSettings settingsCsvParser() {
        CsvParserSettings settings = new CsvParserSettings(); // you'll find many options here, check the tutorial.
        RowListProcessor rowProcessor = new RowListProcessor();
        settings.setNullValue("<NULL>");
        settings.setEmptyValue("<EMPTY>");
        settings.setIgnoreLeadingWhitespaces(false);
        settings.setIgnoreTrailingWhitespaces(false);
        settings.setNumberOfRecordsToRead(10000000);
        settings.setSkipEmptyLines(false);
        settings.setMaxCharsPerColumn(10000000);
        settings.setInputBufferSize(1000);
        settings.setReadInputOnSeparateThread(false);
        settings.setLineSeparatorDetectionEnabled(true);
        settings.setRowProcessor(rowProcessor);
        settings.setHeaderExtractionEnabled(true);
        return settings;
    }

    private static Timer runTimer() {
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            private int i = 0;

            public void run() {
                System.out.println("time running :" + i++); /*difference time*/
            }
        };
        timer.scheduleAtFixedRate(task, 0, 1000); //1000ms = 1sec
        return timer;
    }
}
