package com.example.loop.config;

import com.example.loop.model.DataSet;
import com.example.loop.model.ModelTesting;
import com.example.loop.model.SongData;
import com.example.loop.repository.SongRepository;
import com.example.loop.utilities.Utility;
import com.univocity.parsers.common.processor.RowListProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Exel staderlin on 10/19/2019.
 */

public class CsvGenerateDataset {
    private static final int USER = 0;
    private static final int TIMESPAMP = 1;
    private static final int ARTIST = 3;
    private static final int SONG = 5;


    @Autowired
    private static SongRepository songRepository;

    public static void main(String[] args) {
        Timer timer = runTimer();
        ArrayList<List<String>> datasetV1 = readCsvToList("C:/dataset/unique_song_session_log_plus_genre.csv");
//        ArrayList<List<String>> datasetV1 = readCsvToList("C:/unique_song_session_log_plus_genre.csv");
//        List<DataSet> dataset = recordRekomendasi(datasetV1);
        ArrayList<List<DataSet>> listDataset = recordGenrePerUser(datasetV1);
//         ArrayList<List<String>> songRecord = readCsvToList("C:/song_id_idgenre_idartist_doang.csv");
//         ArrayList<List<String>> sessionLogRecord = readCsvToList("C:/data_testing.csv");
//         List<DataSet> data = recordMixSongLog(datasetV1, songRecord);
//        ArrayList<List<DataSet>> listData = recordUniqueSongPerUser(datasetV1);
//        writeTextSessionLogUniqueSong(listData);
//        writeTextSessionLog(listData);
         writeGenreYgDisukaiUser(listDataset);
        timer.cancel();
    }

    private static ArrayList<List<DataSet>> recordGenrePerUser(ArrayList<List<String>> datasetV1) {
        ArrayList<List<DataSet>> listDataset = new ArrayList<>();
        List<DataSet> dataset = new ArrayList<>();
        String userCurrent = "6";
        for (int i = 0; i < datasetV1.size(); i++) {
            if (!datasetV1.get(i).get(1).equals(userCurrent)) {
                listDataset.add(dataset);
                dataset = new ArrayList<>();
            }
            userCurrent = datasetV1.get(i).get(1);
            DataSet data = new DataSet();
            data.id = datasetV1.get(i).get(0);
            data.user = datasetV1.get(i).get(1);
            data.idSong = datasetV1.get(i).get(2);
            data.createDate = datasetV1.get(i).get(3);
            data.idGenre = datasetV1.get(i).get(4);
            dataset.add(data);
        }
        return listDataset;
    }

    private static List<DataSet> recordRekomendasi(ArrayList<List<String>> datasetV1) {
        List<DataSet> dataset = new ArrayList<>();
        for (int i = 0; i < datasetV1.size(); i++) {
            DataSet data = new DataSet();
            data.user = datasetV1.get(i).get(0);
            data.rating = datasetV1.get(i).get(1);
            data.idSong = datasetV1.get(i).get(2);
            data.playcount = datasetV1.get(i).get(3);
            dataset.add(data);
        }
        return dataset;
    }

    private static List<DataSet> recordDefaultLog(ArrayList<List<String>> datasetV1) {
        List<DataSet> dataset = new ArrayList<>();
        for (int i = 0; i < datasetV1.size(); i++) {
            DataSet data = new DataSet();
            data.id = datasetV1.get(i).get(0);
            data.user = datasetV1.get(i).get(1);
            data.idSong = datasetV1.get(i).get(2);
            data.createDate = datasetV1.get(i).get(3);
            data.idGenre = datasetV1.get(i).get(4);
            dataset.add(data);
        }
        return dataset;
    }

    private static ArrayList<List<DataSet>> recordUniqueSongPerUser(ArrayList<List<String>> datasetV1) {
        ArrayList<List<DataSet>> listDataset = new ArrayList<>();
        String currentUser = datasetV1.get(0).get(1);
        List<DataSet> dataset = new ArrayList<>();
        for (int i = 0; i < datasetV1.size(); i++) {
            if(!currentUser.equals(datasetV1.get(i).get(1))) {
                listDataset.add(dataset);
                dataset = new ArrayList<>();
            } else if (i+1 == datasetV1.size()) {
                listDataset.add(dataset);
            }
            DataSet data = new DataSet();
            data.id = datasetV1.get(i).get(0);
            data.user = datasetV1.get(i).get(1);
            data.idSong = datasetV1.get(i).get(2);
            data.createDate = datasetV1.get(i).get(3);
            data.idGenre = datasetV1.get(i).get(4);
            dataset.add(data);
        }
        return listDataset;
    }

    private static List<DataSet> recordMixSongLog(ArrayList<List<String>> logRecord, ArrayList<List<String>> songRecord) {
        List<DataSet> listTestData = new ArrayList<>();
        System.out.println("loading...");
        final int logSize = logRecord.size();
        for (int i = 0; i < logSize; i++) {
            String id = logRecord.get(i).get(0);
            String user = logRecord.get(i).get(1);
            String idSong = logRecord.get(i).get(2);
            String timespamp = logRecord.get(i).get(3);
            String idGenre = "null";
            final int songSize = songRecord.size();
            for (int j = 0; j < songSize; j++) {
                if (idSong.equals(songRecord.get(j).get(0))) { // jika song sama maka
                    idGenre = songRecord.get(j).get(2); // ambil genre
                    break;
                }
            }
            System.out.println(id + " to " + logRecord.size() + " = " + idGenre);
            DataSet testData = new DataSet();
            testData.user = user;
            testData.idSong = idSong;
            testData.createDate = timespamp;
            testData.idGenre = idGenre;
            listTestData.add(testData);
        }
        System.out.println("finish");
        return listTestData;
    }

    private static List<DataSet> recordMixUserSong(ArrayList<List<String>> artistRecord, ArrayList<List<String>> songRecord) {
        List<DataSet> listTestData = new ArrayList<>();
        System.out.println("loading...");

        for (int i = 0; i < songRecord.size(); i++) {
            String idArtist = "null"; // Artist
            String song = songRecord.get(i).get(1);
            for (int j = 0; j < artistRecord.size(); j++) {
                if (songRecord.get(i).get(0).equals(artistRecord.get(j).get(2))) { // jika title sama maka
                    idArtist = artistRecord.get(j).get(0); // ambil idnya
                    break;
                }
            }
            System.out.println("idArtist  :" + idArtist + " - " + "Song :" + song);
            DataSet testData = new DataSet();
            testData.idArtist = idArtist;
            testData.song = song;
            listTestData.add(testData);
        }
        System.out.println("finish");

        return listTestData;
    }

    private static void writeGenreYgDisukaiUser(ArrayList<List<DataSet>> listDataset) {
        // Collections.sort(uniqueName);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("unique_user_liked_genre.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(listDataset.size());
        PrintWriter printWriter = new PrintWriter(fileWriter);

        printWriter.println("id_user,liked_genre,jml_genre_song");
        for (List<DataSet> dataset : listDataset) {
            List<Integer> listIdGenre = new ArrayList<>();
            Set<DataSet> uniqueGenre = new HashSet<>(dataset);
            List<ModelTesting> listModelTesting = new ArrayList<>();
            String user = "";
            for (DataSet data : dataset) {
                listIdGenre.add(Integer.valueOf(data.idGenre));     // add semua id genre
            }
            HashMap<Integer, Integer> occurrenceId = Utility.countOccurrenceNumber(listIdGenre);  // Hashmap semua id genre
            for (DataSet data : uniqueGenre) {
                ModelTesting model = new ModelTesting();
                user = data.user;
                model.id = data.idGenre;
                model.playcount = occurrenceId.get(Integer.valueOf(data.idGenre));  // add playcount
                listModelTesting.add(model);
            }
            Collections.sort(listModelTesting, Collections.reverseOrder());
            int i = 0;

            for (ModelTesting model : listModelTesting) {
//                if(i==2) break;
                printWriter.println(user + "," + model.id + "," + model.playcount);
                i++;
            }


        }
        printWriter.close();
    }

    private static void writeTextSessionLogUniqueSong(ArrayList<List<DataSet>> listDataset) {
//        Collections.sort(uniqueName);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("unique_song_session_log.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter printWriter = new PrintWriter(fileWriter);
        int id = 1;
        printWriter.println("id,id_user,id_song,timespamp,id_genre");



        for(List<DataSet> list : listDataset) {
            Set<DataSet> uniqueData = new HashSet<>(list);
            List<DataSet> listData = new ArrayList<>(uniqueData);
            for (DataSet testData : listData) {
//            String id = testData.id;
                String user = testData.user;
                String idSong = testData.idSong;
                String timespamp = testData.createDate;
                String idGenre = testData.idGenre;
                printWriter.println(id + "," + user + "," + idSong + "," + timespamp + "," + idGenre);
                id++;
            }

        }

        printWriter.close();
    }



    private static void writeTextSessionLog(List<DataSet> uniqueName) {
        Collections.sort(uniqueName);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("unique_song_session_log.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter printWriter = new PrintWriter(fileWriter);
        int id = 1;
        printWriter.println("id,id_user,id_song,timespamp,id_genre");
        for (DataSet testData : uniqueName) {
//            String id = testData.id;
            String user = testData.user;
            String idSong = testData.idSong;
            String timespamp = testData.createDate;
            String idGenre = testData.idGenre;
            printWriter.println(id + "," + user + "," + idSong + "," + timespamp + "," + idGenre);
            id++;
        }
        printWriter.close();
    }

    private static void writeTextUser(List<DataSet> uniqueName) {
        Collections.sort(uniqueName);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("user.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter printWriter = new PrintWriter(fileWriter);
        int id = 5;
        for (DataSet testData : uniqueName) {
            String email = testData.user + "@gmail.com";
            String image = "https://www.logolynx.com/images/logolynx/b4/b4ef8b89b08d503b37f526bca624c19a.jpeg";
            String role = "0";
            String lastLoginDate = "2019-05-04T12:43:14Z";
            String name = testData.user;
            String password = "$2a$10$T43uOOwHjk7ovcGUo0yzk.1gEXIpPGQeJSYwgiSjUOOGXVzmNlsX6";
            String registeredDate = "2019-05-04T12:43:14Z";
            String status = "0";
            printWriter.println(id + "," + email + "," + image + "," + role + "," + lastLoginDate + "," + name + "," + password + "," + registeredDate + "," + status);
            id++;
        }
        printWriter.close();
    }

    private static void writeTextArtist(List<DataSet> uniqueName) {
        Collections.sort(uniqueName);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("artist.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter printWriter = new PrintWriter(fileWriter);
        int id = 7;
        String image = "https://www.logolynx.com/images/logolynx/b4/b4ef8b89b08d503b37f526bca624c19a.jpeg";
        for (DataSet testData : uniqueName) {
            printWriter.println(id + "," + image + "," + testData.artist + "," + "0");
//            if (testData.artist.length() > 3 && testData.song.length() > 3) {
//                printWriter.println(testData.artist + "," + testData.song);
//            }
            id++;
        }

        printWriter.close();
    }

    private static void writeTextSongFromDB(List<SongData> songData) {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("song_id_idgenre_idartist_doang.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter printWriter = new PrintWriter(fileWriter);
        for (SongData testData : songData) {
            Integer id_song = testData.id;
            int id_artist = testData.id_artist;
            int genre = testData.id_genre;
            printWriter.println(id_song + "," + id_artist + "," + genre);
        }

        printWriter.close();
    }


    private static void writeTextSong(List<DataSet> uniqueName) {
//        Collections.sort(uniqueName);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("song_id_idgenre_idartist_doang.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter printWriter = new PrintWriter(fileWriter);
        int id = 13;
        String url = "no available";
        for (DataSet testData : uniqueName) {
            String idSong = testData.idSong;
            String idArtist = testData.idArtist;
            String song = testData.song;
            String playcount = "0";
            String status = "0";
            String duration = "00:00";
            String id_genre = testData.idGenre;
            printWriter.println(idSong + "," + idArtist + "," + id_genre);
//            printWriter.println(id + "," + idArtist + "," + url + "," + song + "," + playcount + "," + status+ "," + duration+ "," + id_genre);
            id++;
        }

        printWriter.close();
    }

    private static ArrayList<List<String>> readCsvToList(String csvFileLocation) {
        ArrayList<List<String>> records = new ArrayList<>();
        CsvParser parser = new CsvParser(settingsCsvParser());
        parser.beginParsing(new File(csvFileLocation));
        try {
            String[] row;
            int loop = 1;
            int threshold = 5000000;
            while ((row = parser.parseNext()) != null) {
                records.add(Arrays.asList(row));
                if (loop >= threshold) {
                    System.out.println("load record :" + loop);
                }
                loop++;
            }
            System.out.println("record size :" + records.size());
            return records;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    private static CsvParserSettings settingsCsvParser() {
        CsvParserSettings settings = new CsvParserSettings(); // you'll find many options here, check the tutorial.
        RowListProcessor rowProcessor = new RowListProcessor();
        settings.setNullValue("<NULL>");
        settings.setEmptyValue("<EMPTY>");
        settings.setIgnoreLeadingWhitespaces(false);
        settings.setIgnoreTrailingWhitespaces(false);
        settings.setNumberOfRecordsToRead(10000000);
        settings.setSkipEmptyLines(false);
        settings.setMaxCharsPerColumn(10000000);
        settings.setInputBufferSize(1000);
        settings.setReadInputOnSeparateThread(false);
        settings.setLineSeparatorDetectionEnabled(true);
        settings.setRowProcessor(rowProcessor);
        settings.setHeaderExtractionEnabled(true);
        return settings;
    }

    private static Timer runTimer() {
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            private int i = 0;

            public void run() {
                System.out.println("time running :" + i++); /*difference time*/
            }
        };
        timer.scheduleAtFixedRate(task, 0, 1000); //1000ms = 1sec
        return timer;
    }

}
