package com.example.loop.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    // public static final String SQL_LOGIN
    // ="select username, password, active as enabled \n" +
    // "from s_users where username = ?";

    // public static final String SQL_PERMISSION
    //     = "select u.username, r.nama as authority\n" +
    //     "from s_users u join s_user_role ur on u.id = ur.id_user\n" +
    //     "join s_roles r on ur.id_role = r.id\n" +
    //     "where u.username = ?";

    public static final String SQL_LOGIN
    ="select email, password \n" +
    "from users where email = ?";

    public static final String SQL_PERMISSION
        = "select u.email, r.role as authority\n" +
        "from users when r.role = adm where u.email = ?";
        
    @Autowired
    private DataSource dataSource;

    @Autowired
    public void configurGlobal(AuthenticationManagerBuilder auth) throws Exception {
    auth
            .jdbcAuthentication()
            .dataSource(dataSource)
            .usersByUsernameQuery(SQL_LOGIN)
            .authoritiesByUsernameQuery(SQL_PERMISSION)
            .passwordEncoder(byPasswordEncoder()); // bcrypt

    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedMethods("*")
                        .allowedOrigins("*");
            }
        };
    }
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.authorizeRequests()
                .antMatchers("/bootstrap/**", "/dist/**", "/plugins/**").permitAll()
                .antMatchers("/js/**").permitAll()
                .antMatchers("/css/**").permitAll()
                .antMatchers("/fonts/**").permitAll()
                // .anyRequest().authenticated()
                .and()
            .formLogin()
                .loginPage("/login").permitAll()
                .failureUrl("/login?error")
                .defaultSuccessUrl("/", true)
                .and()
            .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/login")
                .permitAll();
    }


    @Bean
    public BCryptPasswordEncoder byPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }


}
