package com.example.loop.callback;

import java.util.List;

public class BaseListCallback<T> {

    public boolean status;

    public List<T> data;

    public String message;
}