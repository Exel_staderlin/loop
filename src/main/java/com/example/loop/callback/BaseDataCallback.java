package com.example.loop.callback;


public class BaseDataCallback<T> {

    public boolean status;

    public T data;

    public String message;
}