//package com.example.loop.service;
//
//import com.example.loop.model.SessionLogData;
//import com.example.loop.repository.SessionLogRepository;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.scheduling.annotation.Async;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//import java.util.concurrent.CompletableFuture;
//
//@Service
//public class SessionService {
//    private static final Logger LOGGER = LoggerFactory.getLogger(SessionService.class);
//    @Autowired
//    private SessionLogRepository sessionLogRepository;
//
////    @Async
////    public CompletableFuture<List<Car>> saveCars(final MultipartFile file) throws Exception {
////        final long start = System.currentTimeMillis();
////        List<Car> cars = parseCSVFile(file);
////        LOGGER.info("Saving a list of cars of size {} records", cars.size());
////        cars = sessionLogRepository.saveAll(cars);
////        LOGGER.info("Elapsed time: {}", (System.currentTimeMillis() - start));
////        return CompletableFuture.completedFuture(cars);
////    }
////    private List<Car> parseCSVFile(final MultipartFile file) throws Exception {
////        final List<Car> cars=new ArrayList<>();
////        try {
////            try (final BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
////                String line;
////                while ((line=br.readLine()) != null) {
////                    final String[] data=line.split(";");
////                    final Car car=new Car();
////                    car.setManufacturer(data[0]);
////                    car.setModel(data[1]);
////                    car.setType(data[2]);
////                    cars.add(car);
////                }
////                return cars;
////            }
////        } catch(final IOException e) {
////            LOGGER.error("Failed to parse CSV file {}", e);
////            throw new Exception("Failed to parse CSV file {}", e);
////        }
////    }
//
//    @Async
//    public CompletableFuture<List<SessionLogData>> findByIdUser(int userId) {
//        LOGGER.info("Request to get a list of session by user");
//        final List<SessionLogData> sessionLogData = sessionLogRepository.findByIdUser(userId);
//        return CompletableFuture.completedFuture(sessionLogData);
//    }
//
//    @Async
//    public CompletableFuture<List<SessionLogData>> findAll() {
//        LOGGER.info("Request to get a list of all session");
//        final List<SessionLogData> sessionLogData = sessionLogRepository.findAll();
//        return CompletableFuture.completedFuture(sessionLogData);
//    }
//
//
//}
